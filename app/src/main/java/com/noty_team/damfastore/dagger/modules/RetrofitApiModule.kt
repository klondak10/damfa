package com.noty_team.damfastore.dagger.modules

import android.app.Application
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import com.noty_team.damfastore.BuildConfig
import com.noty_team.damfastore.base.api.interfaces.DamfaStoreAdministratorApi
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.POST
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class RetrofitApiModule {

    companion object {
        private const val CONNECTION_TIMEOUT_SEC = 60L
        private const val CACHE_SIZE = 10_000_000L
        private const val CACHE_FILE_NAME = "http_cache"
    }

    @Provides
    @Singleton
    fun provideHttpCache(application: Application): Cache {
        val cacheSize = 10L * 1024 * 1024
        return Cache(application.cacheDir, cacheSize)
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        return gsonBuilder.create()
    }


    @Provides
    @Singleton
    fun provideOkhttpClient(cache: Cache): OkHttpClient {
        return OkHttpClient.Builder().apply {
            cache(cache)
/*
            val cookieManager = CookieManager()
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL)
            cookieJar(JavaNetCookieJar(cookieManager))*/



            if (BuildConfig.DEBUG) {
                val httpLoggingInterceptor = HttpLoggingInterceptor()
                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                addInterceptor(httpLoggingInterceptor)
            }

            connectTimeout(CONNECTION_TIMEOUT_SEC, TimeUnit.SECONDS)
            readTimeout(CONNECTION_TIMEOUT_SEC, TimeUnit.SECONDS)
            writeTimeout(CONNECTION_TIMEOUT_SEC, TimeUnit.SECONDS)
            retryOnConnectionFailure(true)

            addInterceptor { chain ->
                var request = chain.request()
//                val token = PaperIO.getBearerToken()
//
//                if (token.isNotEmpty()) {
//                    var requestBody = request.body()
//
//                    val customReq = bodyToString(requestBody)
//                    try {
//                        val obj = JSONObject(customReq)
//
//                        if (obj.has("data")) {
//
//                            val data = obj.getJSONObject("data")
//                            if (data != null) {
//                                data.put("token", token)
//                                obj.put("data", data)
//
//                                requestBody = RequestBody.create(requestBody?.contentType(), obj.toString());
//                                if (requestBody != null) {
//                                    val requestBuilder = request.newBuilder()
//                                    request = requestBuilder
//                                            .post(requestBody)
//                                            .build()
//                                }
//                            }
//                        }
//
//                    } catch (e: JSONException) {
//                        e.printStackTrace()
//                    }
//                }

                val change = request.newBuilder()
                    .header("Content-Type", "application/json")
                    .header("Content-type", "application/x-www-form-urlencoded")
                    .header("Connection", "close")


                val response = chain.proceed(change.build())
//                val allHeaders = response.headers()

//            response.body()?.let { responseBody ->
//                try {
//                    val bodyJson = JSONObject(responseBody.string())
//                    if (bodyJson.has("data") && bodyJson.has("response")){
//                        if (!bodyJson.getBoolean("response")){
//                            bodyJson.remove("data")
//                        }
//                        else {
//                            allHeaders["X-Items-Total"]?.let { total ->
//                                allHeaders["X-Items-Count"]?.let { count ->
//                                    bodyJson.put("total_items", total)
//                                    bodyJson.put("items_count", count)
//                                }
//                            }
//                        }
//                    }
//                    val newBody = ResponseBody.create(responseBody.contentType(), bodyJson.toString())
//                    response = response.newBuilder()
//                            .headers(allHeaders)
//                            .body(newBody)
//                            .build()
//                } catch (e: Exception) {
//                }
//            }

                response
            }
        }.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_END_POINT)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }


    @Provides
    @Singleton
    fun provideLoginApiImplementation(retrofit: Retrofit): DamfaStoreAdministratorApi {
        return retrofit.create(DamfaStoreAdministratorApi::class.java)
    }
}
