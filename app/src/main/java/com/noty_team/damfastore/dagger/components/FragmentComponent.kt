package com.noty_team.damfastore.dagger.components

import com.noty_team.damfastore.base.activity.BaseActivity
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.base.fragment.BasePresenter
import dagger.Subcomponent
import com.noty_team.damfastore.dagger.modules.FragmentModule
import com.noty_team.damfastore.dagger.scopes.FragmentScope

@FragmentScope
@Subcomponent(modules = [FragmentModule::class])
interface FragmentComponent {
	fun inject(target: BaseActivity)
	fun inject(target: BaseFragment<BasePresenter>)
}