package com.noty_team.damfastore.dagger.components

import android.app.Application
import com.noty_team.damfastore.base.api.retrofit.DamfastoreAdministrator
import com.noty_team.damfastore.base.fragment.BasePresenter
import dagger.Component
import com.noty_team.damfastore.dagger.modules.AppModule
import com.noty_team.damfastore.dagger.modules.FragmentModule
import com.noty_team.damfastore.dagger.modules.RetrofitApiModule
import javax.inject.Singleton

@Singleton
@Component(
		modules = [
			AppModule::class,
			RetrofitApiModule::class
		]
)
interface AppComponent {
	fun plus(fragmentModule: FragmentModule): FragmentComponent

	fun inject(app: Application)
	fun inject(target:DamfastoreAdministrator)
	fun inject(target: BasePresenter)


}