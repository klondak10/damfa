package com.noty_team.damfastore.dagger.modules

import com.noty_team.damfastore.base.activity.BaseActivity
import dagger.Module
import dagger.Provides
import com.noty_team.damfastore.dagger.scopes.FragmentScope


@Module
class FragmentModule(private val baseActivity: BaseActivity) {

    @Provides
    @FragmentScope
    fun provideBaseActivity(): BaseActivity = baseActivity

}