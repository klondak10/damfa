package com.noty_team.damfastore.dagger.scopes

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class RetrofitScope