package com.noty_team.damfastore.dagger.modules

import android.app.Application
import android.content.Context
import com.noty_team.damfastore.base.api.retrofit.DamfastoreAdministrator
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class AppModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideContext(): Context = app.applicationContext

    @Provides
    fun provideRequestsDisposable() = CompositeDisposable()

    @Provides
    @Singleton
    fun provideDamfastoreApi(context: Context, retrofit: Retrofit):
            DamfastoreAdministrator = DamfastoreAdministrator(context, retrofit)


}