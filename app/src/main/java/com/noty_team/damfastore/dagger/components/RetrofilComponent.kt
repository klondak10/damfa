package com.noty_team.damfastore.dagger.components

import com.noty_team.damfastore.dagger.modules.RetrofitApiModule
import dagger.Subcomponent
import com.noty_team.damfastore.dagger.scopes.RetrofitScope

@RetrofitScope
@Subcomponent(modules = [RetrofitApiModule::class])
interface RetrofilComponent{

}
