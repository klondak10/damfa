package noty_team.com.urger.utils.cicerone

import android.support.v4.app.Fragment
import android.view.View
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.base.fragment.BasePresenter


import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {
	/*enum class MainScreens {
		VACANCY,
		CHAT,
		MAP,
		NOTIFICATION,
		SERVICE_DELIVERY,
		PROFILE,
		BOTTOM_VACANCY_FRAGMENT
	}

	enum class NestedScreens {
		PROFILE_KOLBASA,
		PROFILE_PAGER
	}

	class TabScreen(val tabScreen: MainScreens) : SupportAppScreen() {

		override fun getFragment(): Fragment {
			when (tabScreen) {
				MainScreens.VACANCY -> return VacancyFragmentContainer.getNewInstance(tabScreen.name)
				MainScreens.CHAT -> return ChatFragmentContainer.getNewInstance(tabScreen.name)
				MainScreens.MAP -> return MapFragmentContainer.getNewInstance(tabScreen.name)
				MainScreens.NOTIFICATION -> return NotificationFragmentContainer.getNewInstance(tabScreen.name)
				MainScreens.SERVICE_DELIVERY -> return ServiceDeliveryFragmentContainer.getNewInstance(tabScreen.name)
				MainScreens.PROFILE -> return ProfileFragmentContainer.getNewInstance(tabScreen.name)
				Screens.MainScreens.BOTTOM_VACANCY_FRAGMENT -> return BottomVacancyFragmentContainer.getNewInstance(tabScreen.name)
				else -> return VacancyFragmentContainer.getNewInstance(tabScreen.name)
			}
		}
	}

	class NestedTabScreen(val tabScreen: NestedScreens, val onFragmentCreated: (rootView: View) -> Unit = {}) : SupportAppScreen() {
		override fun getFragment(): Fragment {
			when (tabScreen) {
				NestedScreens.PROFILE_KOLBASA -> return KolbasaProfileFragmentContainer.getNewInstance(tabScreen.name)
				NestedScreens.PROFILE_PAGER -> return PagerProfileFragmentContainer.getNewInstance(tabScreen.name, onFragmentCreated)
				else -> return VacancyFragmentContainer.getNewInstance(tabScreen.name)
			}
		}
	}*/

	class FragmentScreen<P : BasePresenter, T : BaseFragment<P>>(var fragment: T) : SupportAppScreen() {
		override fun getFragment(): Fragment {
			return fragment
		}
	}
}