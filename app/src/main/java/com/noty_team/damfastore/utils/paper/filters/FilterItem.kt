package com.noty_team.damfastore.utils.paper.filters

data class FilterItem(
    val filterName: String,
    val filterId: String,
    var isSelected: Boolean
)