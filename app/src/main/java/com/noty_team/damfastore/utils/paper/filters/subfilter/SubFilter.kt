package com.noty_team.damfastore.utils.paper.filters.subfilter

data class SubFilter(
    var subfilterId: String,
    var subfilterName: String,
    var isSelected: Boolean
)