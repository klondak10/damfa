package com.noty_team.damfastore.utils.paper

import android.util.Log
import com.noty_team.damfastore.base.api.response.filter_subfilter.FilterSubFilterResponse
import com.noty_team.damfastore.base.api.response.filters.FilterResponse
import com.noty_team.damfastore.ui.recycler.item_adapter.CategoriesManufactureItem
import com.noty_team.damfastore.ui.recycler.item_adapter.ProductRecyclerItem
import com.noty_team.damfastore.utils.paper.filters.FilterItem
import com.noty_team.damfastore.utils.paper.filters.subfilter.SubFilter
import com.noty_team.damfastore.utils.paper.products.ProductItem
import io.paperdb.Paper
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlin.collections.ArrayList

class PaperIO {
    companion object {
        private const val LIST_CATEGORIES_MANUFACTURE_FILTER = "list_categories_manufacture_filter"
        private const val LIST_CATEGORIES_FILTER = "list_categories_filter"
        private const val LIST_MANUFACTURE_FILTER = "list_manufacture_filter"
        private const val IS_NEED_UPATE_FILTER_LIST = "IS_NEED_UPATE_FILTER_LIST"
        private const val SUBFILTER_DB_LIST = "SUBFILTER_DB_LIST"

        private const val PRODUCTS = "PRODUCTS"
        const val SELLER_ID = "seller_id"
        private const val MANUFACTURE = "MANUFACTURE"
        private const val CATEGORIES = "CATEGORIES"
        private const val FRAGMENT_CATEGORY_PRODUCT = "FRAGMENT_CATEGORY_PRODUCT"
        private const val FRAGMENT_CUSTOMER_PRODUCT = "FRAGMENT_CUSTOMER_PRODUCT"
        private const val RECYCLE = "RECYCLE"
        private const val DELETED_LIST = "DELETED_LIST"
        private const val COUNTER_APP = "COUNTER_APP"
        private const val COUNTER_APPPPP = "COUNTER_APPPP"
        private const val FILTERS = "FILTERS"


        private const val LOGIN = "LOGIN"
        private const val PASSWORD = "PASSWORD"

        private const val SUBFILTERS = "SUBFILTERS"
        private const val SUBFILTERS_CLEAN = "SUBFILTERS_CLEAN"

        private const val FILTERS_SUBFILTER = "FILTER_SUBFILTER"
        private const val FILTERS_SUBFILTER_CLEAN = "FILTER_SUBFILTER_CLEAN"

        private const val FILTERS_SUBFILTER_LIST = "FILTERS_SUBFILTER_LIST"
        private const val FILTERS_SUBFILTER_LIST_CLEAN = "FILTERS_SUBFILTER_LIST_CLEAN"

        private const val FILTER_COUNTER = "FILTER_COUNTER"
        private const val FILTER_COUNTER_CLEAN = "FILTER_COUNTER_CLEAN"

        private const val COUNTER_SELECTED = "COUNTER_SELECTED"


        fun setSellerID(sellerId: Int) {
            Paper.book().write(SELLER_ID, sellerId)
        }

        fun getSellerID(): Int {
            return Paper.book().read(SELLER_ID)
        }

        fun resetAllFilters() {
            subfiltersHashList = subfiltersHashListClean
            filterBDItem = filterBDItemClean
            filterSubfilter = filterSubfilterClean
            filterCounter = filterCounterClean
        }


        var products: ArrayList<ProductItem>
            get() {

                return if (Paper.book().exist(PRODUCTS))
                    Paper.book().read(PRODUCTS)
                else arrayListOf()
            }
            set(value) {

                Paper.book().write(PRODUCTS, value)
            }

        var USER_LOGIN: String
            get() {

                return if (Paper.book().exist(LOGIN))
                    Paper.book().read(LOGIN)
                else ""
            }
            set(value) {

                Paper.book().write(LOGIN, value)
            }

        var USER_PASSWORD: String
            get() {

                return if (Paper.book().exist(PASSWORD))
                    Paper.book().read(PASSWORD)
                else ""
            }
            set(value) {

                Paper.book().write(PASSWORD, value)
            }

        fun setListProducts(list: ArrayList<ProductRecyclerItem>, onSuccess: () -> Unit) {
            getSingleProduct {

                for (i in list.indices) {
                    var counter = false
                    for (j in it.indices) {
                        if (list[i].id == it[j].productsId) {
                            counter = true
                            if (list[i].isSelected != it[j].isSelected) {
                                it[j].isSelected = list[i].isSelected
                                if (it[j].isSelected) {
                                    ++counterSelected
                                } else {
                                    --counterSelected
                                }
                                break
                            }
                        } else {
                            if (counter) {
                                break
                            }
                        }
                    }
                }

                products = it

                onSuccess()
            }
        }

        fun setListCategoryFiter(list: ArrayList<CategoriesManufactureItem>) {
            categoriesFilter = list
        }

        fun clearListCategoryFilter() {
            categoriesFilter = arrayListOf()
        }

        fun setListManufactureFiter(list: ArrayList<CategoriesManufactureItem>) {
            manufactureFilter = list
        }

        fun clearListManufactureFilter() {
            manufactureFilter = arrayListOf()
        }


        var categoriesFilter: ArrayList<CategoriesManufactureItem>
            get() {
                return if (Paper.book().exist(LIST_CATEGORIES_FILTER))
                    Paper.book().read(LIST_CATEGORIES_FILTER)
                else arrayListOf()
            }
            set(value) {
                Paper.book().write(LIST_CATEGORIES_FILTER, value)
            }


        var manufactureFilter: ArrayList<CategoriesManufactureItem>
            get() {
                return if (Paper.book().exist(LIST_MANUFACTURE_FILTER))
                    Paper.book().read(LIST_MANUFACTURE_FILTER)
                else arrayListOf()
            }
            set(value) {
                Paper.book().write(LIST_MANUFACTURE_FILTER, value)
            }

        fun setListCategory(list: ArrayList<CategoriesManufactureItem>, onSuccess: () -> Unit) {
            getSingleCategory {
                for (i in list.indices) {
                    var counter = false
                    for (j in it.indices) {
                        if (list[i].id == it[j].id) {
                            counter = true
                            if (list[i].isSelected != it[j].isSelected) {
                                it[j].isSelected = list[i].isSelected
                                if (it[j].isSelected) {
                                    ++counterSelected
                                } else {
                                    --counterSelected
                                }
                                break
                            }
                        } else {
                            if (counter) {
                                break
                            }
                        }
                    }
                }

                categories = it

                onSuccess()
            }
        }

        fun clearAllFilters(onSuccess: () -> Unit) {

            getSingleSubfiltersHashList {
                it.values.forEach {
                    it.forEach {
                        it.isSelected = false
                    }
                }

                subfiltersHashList = it

                getSingleFilterBDItem {
                    it.forEach {
                        it.isSelected = false
                    }

                    filterBDItem = it


                    var filterCounterData = filterCounter

                    var newList = HashMap<String, Int>()

                    filterCounterData.forEach {
                        newList[it.key] = 0
                    }

                    filterCounter = newList

                    onSuccess()
                }
            }
        }


        // set selected product by category or manufacture
        fun setSelectedUnselectedByListProduct(
            isSelect: Boolean,
            listIdProducts: List<String>,
            onSuccess: () -> Unit
        ) {
            getSingleProduct {
                for (i in listIdProducts.indices) {
                    for (j in it.indices) {
                        if (listIdProducts[i] == it[j].productsId) {
                            if (isSelect) {
                                it[j].isSelected = true
                                ++counterSelected
                            } else {
                                it[j].isSelected = false
                                --counterSelected
                            }
                            break
                        }
                    }
                }

                products = it

                onSuccess()

            }


            /* getSingleProduct { productList ->



                     onSuccess()
             }*/
        }

        fun setSelectedUnselectedFilter(
            isSelected: Boolean,
            filterID: String,
            onSuccess: (listSize: Int) -> Unit
        ) {

            /* getSingleSubfiltersHashList {

             }*/

            val subfiltersById = getSublistByFilterId(filterID)

            if (isSelected) {
                subfiltersById.map {
                    it.isSelected = true
                }
            } else {
                subfiltersById.map {
                    it.isSelected = false
                }
            }

            setSubfilteryByFilterId(filterID, subfiltersById)
            onSuccess(subfiltersById.size)
        }

        fun setListManufacture(list: ArrayList<CategoriesManufactureItem>, onSuccess: () -> Unit) {
            getSingleManufacture {
                for (i in list.indices) {
                    var counter = false
                    for (j in it.indices) {
                        if (list[i].id == it[j].id) {
                            counter = true
                            if (list[i].isSelected != it[j].isSelected) {
                                it[j].isSelected = list[i].isSelected
                                if (it[j].isSelected) {
                                    ++counterSelected
                                } else {
                                    --counterSelected
                                }
                                break
                            }
                        } else {
                            if (counter) {
                                break
                            }
                        }
                    }
                }

                manufactures = it
                onSuccess()
            }
        }


        fun getSelectedCategory(onDataLoad: (list: ArrayList<CategoriesManufactureItem>) -> Unit) {
            getSingleCategory {
                val listSelected = ArrayList<CategoriesManufactureItem>()
                for (i in it.indices) {
                    if (it[i].isSelected) {
                        listSelected.add(it[i])
                    }
                }

                onDataLoad(listSelected)
            }
        }

        fun getSelectedManufacture(onDataLoad: (list: ArrayList<CategoriesManufactureItem>) -> Unit) {
            getSingleManufacture {
                val listSelected = ArrayList<CategoriesManufactureItem>()
                for (i in it.indices) {
                    if (it[i].isSelected) {
                        listSelected.add(it[i])
                    }
                }

                onDataLoad(listSelected)
            }
        }

        private fun getSingleProduct(onSuccessData: (t: ArrayList<ProductItem>) -> Unit)/*: Single<ArrayList<ProductItem>>*/ {
            return Single.fromCallable {
                products
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<ArrayList<ProductItem>> {
                    override fun onSuccess(t: ArrayList<ProductItem>) {
                        onSuccessData(t)
                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onError(e: Throwable) {}
                })
        }


        fun getSingleSubfiltersHashList(onSuccessData: (list: HashMap<String, ArrayList<SubFilter>>) -> Unit) {
            return Single.fromCallable {
                subfiltersHashList
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<HashMap<String, ArrayList<SubFilter>>> {
                    override fun onSuccess(t: HashMap<String, ArrayList<SubFilter>>) {
                        onSuccessData(t)
                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onError(e: Throwable) {}
                })
        }

        private fun getSingleManufacture(onDataLoad: (list: ArrayList<CategoriesManufactureItem>) -> Unit) {
            return Single.fromCallable {
                manufactures
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<ArrayList<CategoriesManufactureItem>> {
                    override fun onSuccess(t: ArrayList<CategoriesManufactureItem>) {
                        onDataLoad(t)
                    }

                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onError(e: Throwable) {

                    }
                })
        }

        private fun getSingleCategory(onDataLoad: (list: ArrayList<CategoriesManufactureItem>) -> Unit) {

            return Single.fromCallable {
                categories
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<ArrayList<CategoriesManufactureItem>> {
                    override fun onSuccess(t: ArrayList<CategoriesManufactureItem>) {
                        onDataLoad(t)
                    }

                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onError(e: Throwable) {

                    }
                })

        }

        fun getSingleRecycler(onSuccessData: (t: ArrayList<ProductRecyclerItem>) -> Unit) {

            return Single.fromCallable { recycle }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<ArrayList<ProductRecyclerItem>> {
                    override fun onSuccess(t: ArrayList<ProductRecyclerItem>) {
                        onSuccessData(t)
                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onError(e: Throwable) {}
                }
                )

        }

        fun getProductById(
            productId: String,
            onSuccess: (listProductItems: ProductItem) -> Unit
        ) {

            getSingleProduct {
                loop@ for (productItem in it) {
                    if (productItem.productsId == productId) {
                        onSuccess(productItem)
                        break@loop
                    }


                }
            }

        }

        fun getProductsByIdsCategoryByFilters(
            listProduct: List<ProductItem>,
            onSuccessData: (t: ArrayList<ProductItem>) -> Unit
        ) {

            val listIdFilters = ArrayList<String>()


            val filters = subfiltersHashList

            filters.forEach { hashList ->
                hashList.value.map { item ->
                    if (item.isSelected) {
                        listIdFilters.add(item.subfilterId)
                    }
                }
            }

            val hashSet = HashSet<ProductItem>()

            for (i in listProduct.indices) {
                for (j in 0 until listIdFilters.size) {
                    val productFilters = listProduct[i].filterId ?: arrayListOf()
                    for (element in productFilters) {
                        if (element == listIdFilters[j]) {
                            hashSet.add(listProduct[i])
                            break
                        }
                    }

                }
            }

            val list = ArrayList(hashSet)

            onSuccessData(list)
        }

        fun getProductsByFilters(onSuccessData: (t: ArrayList<ProductItem>) -> Unit) {
            getSingleProduct {

                val listIdFilters = ArrayList<String>()


                val filters = subfiltersHashList

                filters.forEach { hashList ->
                    hashList.value.map { item ->
                        if (item.isSelected) {
                            listIdFilters.add(item.subfilterId)
                        }
                    }
                }

                val hashSet = HashSet<ProductItem>()

                for (i in 0 until it.size) {
                    for (j in 0 until listIdFilters.size) {
                        val productFilters = it[i].filterId ?: arrayListOf()
                        for (element in productFilters) {
                            if (element == listIdFilters[j]) {
                                hashSet.add(it[i])
                                break
                            }
                        }

                    }
                }

                val list = ArrayList(hashSet)

                onSuccessData(list)

            }
        }

        fun getProductByIdIfSelected(
            listProductId: List<String>,
            onSuccess: (listProductItems: ArrayList<ProductItem>) -> Unit
        ) {

            getSingleProduct { productList ->
                val returnList = ArrayList<ProductItem>()

                if (!listProductId.isNullOrEmpty()) {
                    for (i in productList.indices) {
                        for (j in listProductId.indices) {
                            if (productList[i].productsId == listProductId[j] && productList[i].isSelected) {
                                returnList.add(productList[i])
                                break
                            }
                        }
                    }


                }
                onSuccess(returnList)
            }

        }


        fun setSelectedProduct(id: String, isSelected: Boolean) {
            getSingleProduct {
                for (i in it.indices) {
                    if (it[i].productsId == id) {
                        it[i].isSelected = isSelected
                        break
                    }
                }
                products = it
            }
        }

        fun getRecycleToSelectedProducts(
            listProductItems: ArrayList<ProductItem>,
            onSuccessData: (t: ArrayList<ProductItem>) -> Unit
        ) {

            getSingleRecycler {

                for (i in listProductItems.indices) {
                    listProductItems[i].isSelected = false
                }


                for (i in it.indices) {
                    for (j in listProductItems.indices) {
                        if (it[i].id == listProductItems[j].productsId) {
                            if (it[i].isSelected) {
                                listProductItems[j].isSelected = true
                                break
                            }
                        }
                    }
                }

                onSuccessData(listProductItems)
            }


        }


        fun getProductsByIdCustomer(
            listProductId: List<String>,
            onSuccess: (listProductItems: ArrayList<ProductItem>) -> Unit
        ) {

            getSingleProduct { productList ->
                val returnList = ArrayList<ProductItem>()

                if (!listProductId.isNullOrEmpty())
                    productList.forEachIndexed { _, productItem ->
                        listProductId.forEachIndexed { _, idProduct ->
                            if (productItem.productsId == idProduct)
                                if(productItem.isSelected)
                                returnList.add(productItem)

                        }
                    }
                onSuccess(returnList)
            }

        }

        fun getProductsById(
            listProductId: List<String>,
            onSuccess: (listProductItems: ArrayList<ProductItem>) -> Unit
        ) {

            getSingleProduct { productList ->
                val returnList = ArrayList<ProductItem>()

                if (!listProductId.isNullOrEmpty())
                    productList.forEachIndexed { _, productItem ->
                        listProductId.forEachIndexed { _, idProduct ->
                            if (productItem.productsId == idProduct)
                                returnList.add(productItem)

                            Log.d("myLogs","---"+productItem.productsId )
                            Log.d("myLogs","+++"+idProduct)
                        }
                    }
                onSuccess(returnList)
            }

        }
        //TODO

        var fragmentCategoryProduct: ArrayList<ProductRecyclerItem>
            get() {
                return if (Paper.book().exist(FRAGMENT_CATEGORY_PRODUCT))
                    Paper.book().read(FRAGMENT_CATEGORY_PRODUCT)
                else arrayListOf()
            }
            set(value) {
                Paper.book().write(FRAGMENT_CATEGORY_PRODUCT, value)
            }


        fun searchProductByNameSync(
            listProduct: List<ProductRecyclerItem>,
            name: String,
            onAllDataFind: (searchList: ArrayList<ProductRecyclerItem>) -> Unit,
            onError: () -> Unit
        ) {

            Single.fromCallable {
                listProduct
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<List<ProductRecyclerItem>> {
                    override fun onSuccess(t: List<ProductRecyclerItem>) {

                        val returnList = ArrayList<ProductRecyclerItem>()

                        t.forEach {

                                val productName = it.productName.contains(name, true)
                                val filter = it.fillterName.contains(name)
                                val des = it.desName.contains(name, true)
                                 if (productName||filter||des) {
                                    returnList.add(it)

                            }
                        }

                        onAllDataFind(returnList)
                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onError(e: Throwable) {
                        onError()
                    }
                })

            /*  listProduct.forEach
              {

                  if (!it.productName.isNullOrEmpty()) {
                      val upperProductName = it.productName*//*.toUpperCase()*//**//*.substring(
                        0,
                        if (it.productName.length >= name.length) name.length else 0
                    )*//*

                            val upperName = name*//*.toUpperCase()*//*

                            *//*  val pattern = Pattern.compile(upperProductName)
                              val matcher = pattern.matcher(upperName)
                               val found = matcher.find()*//*

                            val d = upperProductName.contains(upperName, true)
                            if (d) {
                                returnList.add(it)
                            }
                            *//* if (found)
                                 returnList.add(it)*//*
                            //println("Найдено")
                            *//*  else
                                  println("Не найдено")*//*
                            *//*  if (productName.toUpperCase() == name.toUpperCase())
                                  returnList.add(it)*//*
                        }

                    }

                    return returnList*/
        }

        fun getProductByName(
            listProduct: List<ProductRecyclerItem>,
            name: String
        ): ArrayList<ProductRecyclerItem> {

            val returnList = ArrayList<ProductRecyclerItem>()

            listProduct.forEach {

                if (!it.productName.isNullOrEmpty()) {
                    val upperProductName = it.productName/*.toUpperCase()*//*.substring(
                        0,
                        if (it.productName.length >= name.length) name.length else 0
                    )*/

                    val upperName = name/*.toUpperCase()*/

                    /*  val pattern = Pattern.compile(upperProductName)
                      val matcher = pattern.matcher(upperName)
                       val found = matcher.find()*/

                    val d = upperProductName.contains(upperName, true)
                    if (d) {
                        returnList.add(it)
                    }
                    /* if (found)
                         returnList.add(it)*/
                    //println("Найдено")
                    /*  else
                          println("Не найдено")*/
                    /*  if (productName.toUpperCase() == name.toUpperCase())
                          returnList.add(it)*/
                }

            }

            return returnList
        }


        fun getProductByNameSync(
            name: String,
            onAllDataFind: (list: ArrayList<ProductRecyclerItem>) -> Unit
        ) {
            getSingleProduct {
                val returnList = ArrayList<ProductRecyclerItem>()

                it.forEach {

                    if (!it.productsAppName.isNullOrEmpty()) {
                        val productName = it.productsAppName/*.substring(
                        0,
                        if (it.productName.length >= name.length) name.length else 0
                    )*/

                        val find = productName.contains(name, true)

                        if (find) {

                            val productRecyclerItem = ProductRecyclerItem(
                                id = it.productsId ?: "",
                                isSelected = it.isSelected,
                                productImage = it.productsImage ?: "",
                                productName = it.productsAppName
                            )
                            returnList.add(productRecyclerItem)
                        }
                        /*if (productName.toUpperCase() == name.toUpperCase())
                            returnList.add(it)*/
                    }

                }

                onAllDataFind(returnList)
            }

        }

        fun getProductByNameDataBase(
            name: String
        ): ArrayList<ProductRecyclerItem> {

            val returnList = ArrayList<ProductRecyclerItem>()

            /* products {listProduct ->*/
            products.forEach {

                if (!it.productsAppName.isNullOrEmpty()) {
                    val productName = it.productsAppName/*.substring(
                        0,
                        if (it.productName.length >= name.length) name.length else 0
                    )*/

                    val find = productName.contains(name, true)

                    if (find) {

                        val productRecyclerItem = ProductRecyclerItem(
                            id = it.productsId ?: "",
                            isSelected = it.isSelected,
                            productImage = it.productsImage ?: "",
                            productName = it.productsAppName
                        )
                        returnList.add(productRecyclerItem)
                    }
                    /*if (productName.toUpperCase() == name.toUpperCase())
                        returnList.add(it)*/
                }

            }
            /*}*/



            return returnList
        }

        fun getCategoryByNameSync(
            name: String,
            onAllDataFind: (list: ArrayList<CategoriesManufactureItem>) -> Unit
        ) {

            val returnList = ArrayList<CategoriesManufactureItem>()

            categories.forEach {

                val productName = it.manufactureName

                val find = productName.contains(name, true)

                if (find) {
                    returnList.add(it)
                }
            }

            onAllDataFind(returnList)
        }

        fun getCategoryByName(name: String): ArrayList<CategoriesManufactureItem> {

            val returnList = ArrayList<CategoriesManufactureItem>()

            categories.forEach {

                val productName = it.manufactureName/*.substring(
                    0,
                    if (it.manufactureName.length >= name.length) name.length else 0
                )*/


                val find = productName.contains(name, true)

                if (find) {
                    returnList.add(it)
                }
            }

            return returnList
        }

        fun getManufactureByNameSync(
            name: String,
            onAllDataLoad: (list: ArrayList<CategoriesManufactureItem>) -> Unit
        ) {
            val returnList = ArrayList<CategoriesManufactureItem>()

            manufactures.forEach {
                val productName = it.manufactureName/*.substring(
                    0,
                    if (it.manufactureName.length >= name.length) name.length else 0
                )

                if (productName.toUpperCase() == name.toUpperCase())
                    returnList.add(it)*/

                val find = productName.contains(name, true)

                if (find) {
                    returnList.add(it)
                }
            }

            onAllDataLoad(returnList)
        }

        fun getManufactureByName(name: String): ArrayList<CategoriesManufactureItem> {

            val returnList = ArrayList<CategoriesManufactureItem>()

            manufactures.forEach {
                val productName = it.manufactureName/*.substring(
                    0,
                    if (it.manufactureName.length >= name.length) name.length else 0
                )

                if (productName.toUpperCase() == name.toUpperCase())
                    returnList.add(it)*/

                val find = productName.contains(name, true)

                if (find) {
                    returnList.add(it)
                }
            }

            return returnList
        }


        var categories: ArrayList<CategoriesManufactureItem>
            get() {
                return if (Paper.book().exist(CATEGORIES))
                    Paper.book().read(CATEGORIES)
                else arrayListOf()
            }
            set(value) {
                Paper.book().write(CATEGORIES, value)
            }

        var recycle: ArrayList<ProductRecyclerItem>
            get() {
                return if (Paper.book().exist(RECYCLE))
                    Paper.book().read(RECYCLE)
                else arrayListOf()
            }
            set(value) {
                Paper.book().write(RECYCLE, value)
            }


        var deletedList: ArrayList<ProductRecyclerItem>
            get() {
                return if (Paper.book().exist(DELETED_LIST))
                    Paper.book().read(DELETED_LIST)
                else arrayListOf()
            }
            set(value) {
                Paper.book().write(DELETED_LIST, value)
            }


        var manufactures: ArrayList<CategoriesManufactureItem>
            get() {
                return if (Paper.book().exist(MANUFACTURE))
                    Paper.book().read(MANUFACTURE)
                else arrayListOf()
            }
            set(value) {
                Paper.book().write(MANUFACTURE, value)
            }

        var filters: FilterResponse
            get() {
                return if (Paper.book().exist(FILTERS))
                    Paper.book().read(FILTERS)
                else FilterResponse()
            }
            set(value) {
                Paper.book().write(FILTERS, value)
            }

        var filterSubfilter: FilterSubFilterResponse
            get() {
                return if (Paper.book().exist(FILTERS_SUBFILTER))
                    Paper.book().read(FILTERS_SUBFILTER)
                else FilterSubFilterResponse()
            }
            set(value) {
                Paper.book().write(FILTERS_SUBFILTER, value)
            }
        var filterSubfilterClean: FilterSubFilterResponse
            get() {
                return if (Paper.book().exist(FILTERS_SUBFILTER_CLEAN))
                    Paper.book().read(FILTERS_SUBFILTER_CLEAN)
                else FilterSubFilterResponse()
            }
            set(value) {
                Paper.book().write(FILTERS_SUBFILTER_CLEAN, value)
            }


        fun setFilterDbItemIsSelected(
            isSelect: Boolean, filterID: String,
            onSuccess: () -> Unit
        ) {

            getSingleFilterBDItem {
                for (i in it.indices) {
                    if (filterID == it[i].filterId) {
                        it[i].isSelected = isSelect
                        break
                    }
                }
                filterBDItem = it
                onSuccess()
            }
        }

        fun getSingleFilterBDItem(onSuccess: (listFilters: ArrayList<FilterItem>) -> Unit) {
            return Single.fromCallable {
                filterBDItem
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<ArrayList<FilterItem>> {
                    override fun onSuccess(t: ArrayList<FilterItem>) {
                        onSuccess(t)
                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onError(e: Throwable) {
                    }
                })
        }

        var filterBDItem: ArrayList<FilterItem>
            get() {
                return if (Paper.book().exist(FILTERS_SUBFILTER_LIST))
                    Paper.book().read(FILTERS_SUBFILTER_LIST)
                else arrayListOf()
            }
            set(value) {
                Paper.book().write(FILTERS_SUBFILTER_LIST, value)
            }

        var filterBDItemClean: ArrayList<FilterItem>
            get() {
                return if (Paper.book().exist(FILTERS_SUBFILTER_LIST_CLEAN))
                    Paper.book().read(FILTERS_SUBFILTER_LIST_CLEAN)
                else arrayListOf()
            }
            set(value) {
                Paper.book().write(FILTERS_SUBFILTER_LIST_CLEAN, value)
            }

        var couterApp: Int
            get() {
                return if (Paper.book().exist(COUNTER_APP))
                    Paper.book().read(COUNTER_APP)
                else 0
            }
            set(value) {
                Paper.book().write(COUNTER_APP, value)
            }


        var couterAppPPP: Int
            get() {
                return if (Paper.book().exist(COUNTER_APPPPP))
                    Paper.book().read(COUNTER_APPPPP)
                else 0
            }
            set(value) {
                Paper.book().write(COUNTER_APPPPP, value)
            }


        fun getCounterSelected(counterSelected: (counter: Int) -> Unit) {
            getSingleProduct {
                var counter = 0
                for (i in it.indices)
                    if (it[i].isSelected)
                        ++counter

                counterSelected(counter)
            }
        }

        var counterSelected: Int
            get() {
                return if (Paper.book().exist(COUNTER_SELECTED))
                    Paper.book().read(COUNTER_SELECTED)
                else 0
            }
            set(value) {
                Paper.book().write(COUNTER_SELECTED, value)
            }

        var fragmentCustomerProducts: ArrayList<ProductRecyclerItem>
            get() {
                return if (Paper.book().exist(FRAGMENT_CUSTOMER_PRODUCT))
                    Paper.book().read(FRAGMENT_CUSTOMER_PRODUCT)
                else arrayListOf()
            }
            set(value) {
                Paper.book().write(FRAGMENT_CUSTOMER_PRODUCT, value)
            }

        var filterCounter: HashMap<String, Int>
            get() {
                return if (Paper.book().exist(FILTER_COUNTER))
                    Paper.book().read(FILTER_COUNTER)
                else hashMapOf()
            }
            set(value) {
                Paper.book().write(FILTER_COUNTER, value)
            }

        var filterCounterClean: HashMap<String, Int>
            get() {
                return if (Paper.book().exist(FILTER_COUNTER_CLEAN))
                    Paper.book().read(FILTER_COUNTER_CLEAN)
                else hashMapOf()
            }
            set(value) {
                Paper.book().write(FILTER_COUNTER_CLEAN, value)
            }


        fun getFilterCounter(): Int {

            var counter = 0

            filterCounter.forEach {
                counter += it.value

            }


            return counter
        }


        var subfiltersHashList: HashMap<String, ArrayList<SubFilter>>
            get() {
                return if (Paper.book().exist(SUBFILTERS))
                    Paper.book().read(SUBFILTERS)
                else hashMapOf()
            }
            set(value) {
                Paper.book().write(SUBFILTERS, value)
            }

        //TODO Rework sucks
        var subfiltersHashListClean: HashMap<String, ArrayList<SubFilter>>
            get() {
                return if (Paper.book().exist(SUBFILTERS_CLEAN))
                    Paper.book().read(SUBFILTERS_CLEAN)
                else hashMapOf()
            }
            set(value) {
                Paper.book().write(SUBFILTERS_CLEAN, value)
            }


        fun getSublistByFilterId(id: String): ArrayList<SubFilter> {

            subfiltersHashList.forEach {
                if (id == it.key) return it.value

            }

            return arrayListOf()
        }

        fun setSubfilteryByFilterId(
            id: String,
            newSubfilterList: ArrayList<SubFilter>
        ) {

            val list = subfiltersHashList

            list[id] = newSubfilterList

            subfiltersHashList = list

        }

        fun getAllSelectedFilterNames(osSuccess: (allSelectedFilterNames: String) -> Unit) {
            getSingleFilterBDItem {

                var selectedItems = ""

                for (i in it.indices) {
                    if (it[i].isSelected) {

                        selectedItems += it[i].filterName + ", "
                    }
                }

                if (!selectedItems.isNullOrEmpty())
                    selectedItems =
                        selectedItems.substring(0, selectedItems.length - 2)

                osSuccess(selectedItems)
            }
        }


    }
}