package com.noty_team.damfastore.utils.paper.products

import android.net.Uri
import com.google.gson.annotations.SerializedName

data class ProductItem(

    val productsImage: String? = null,

    val manufacturersName: String? = null,

    val productsId: String? = null,

    val manufacturersId: String? = null,

    val productsModel: String? = null,

    val productsAppId: String? = null,

    val filterId: List<String>? = null,

    val filter_name: List<String>? = null,

    val productsAppName: String? = null,

    val productsDescription: String? = null,

    var isSelected: Boolean = false,

    var image: String = ""
)