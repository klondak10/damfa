package com.noty_team.damfastore.utils.base_adapters

import com.noty_team.damfastore.utils.Constants

open class BaseUiPaginationItem(var itemPaginationType: Constants.PaginationItemType = Constants.PaginationItemType.MAIN_ITEM) :
    Comparable<BaseUiPaginationItem> {
    override fun compareTo(other: BaseUiPaginationItem): Int {
        return itemPaginationType.compareTo(other.itemPaginationType)
    }
}