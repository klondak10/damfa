package com.noty_team.damfastore.utils.base_adapters

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.extensions.LayoutContainer
import java.util.*
import kotlin.collections.ArrayList


abstract class BaseAdapterTreeSet<T, H : BaseAdapter.BaseViewHolder>
    (protected var list: TreeSet<T>) : RecyclerView.Adapter<H>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): H {
        return createViewHolder(inflate(parent, viewType), viewType)
    }

    internal abstract fun createViewHolder(view: View, @LayoutRes layoutId: Int): H

    @LayoutRes
    abstract override fun getItemViewType(position: Int): Int

    override fun onBindViewHolder(holder: H, position: Int) {
        holder.bind(position)
    }

    fun removeAll(removeIterator: Iterator<T>) {
        while (removeIterator.hasNext()) {
            val item = removeIterator.next()
            if (list.contains(item))
                remove(item)
        }
    }

    open fun addAll(addIterator: Iterator<T>) {
        while (addIterator.hasNext()) {
            val item = addIterator.next()
            if (!list.contains(item))
                add(item)
        }
    }

    fun addAll(elements: ArrayList<T>) {
        val startPosition = list.size
        list.addAll(elements)
        notifyItemRangeInserted(startPosition, elements.size)
    }


    fun add(t: T) {
//        for (t1 in list) {
//            if (t1 >= t) {
//                add(t, list.indexOf(t1))
//                return
//            }
//        }
        add(t, list.size)
    }

    fun add(t: T, pos: Int) {
        list.add(t)
        notifyItemInserted(pos)
    }

    fun notifyByItem(t: T) {
        val index = list.indexOf(t)
        if (index >= 0)
            notifyItemInserted(index)
    }


    fun remove(t: T) {
        val index = list.indexOf(t)
        list.remove(t)
        if (index >= 0)
            notifyItemRemoved(index)
    }

    fun clear() {
        list.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return list.size
    }


    abstract class BaseViewHolder constructor(view: View) : RecyclerView.ViewHolder(view),
        LayoutContainer {
        override val containerView: View?
            get() = itemView

        abstract fun bind(pos: Int)
    }

    companion object {

        fun inflate(parent: ViewGroup, @LayoutRes res: Int): View {
            return LayoutInflater.from(parent.context).inflate(res, parent, false)
        }
    }
}