package com.noty_team.damfastore.utils

object Constants {

    enum class PaginationItemType {
        MAIN_ITEM,
        LOADING_ITEM,
        ERROR_ITEM;
    }

    const val SellerLimit = 15

    const val SERVICE_IDENT = "IGHFJ"

    const val appCounter = 30
}