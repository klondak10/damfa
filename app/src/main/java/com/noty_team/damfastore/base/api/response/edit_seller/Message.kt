package com.noty_team.damfastore.base.api.response.edit_seller

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Message(

	@field:SerializedName("customers_seller_name")
	val customersSellerName: String? = null,

	@field:SerializedName("customers_seller_status")
	val customersSellerStatus: Boolean? = null,

	@field:SerializedName("customers_seller_id")
	val customersSellerId: String? = null,

	@field:SerializedName("customers_seller_company")
	val customersSellerCompany: String? = null,

	@field:SerializedName("customers_seller_email")
	val customersSellerEmail: String? = null,

	@field:SerializedName("customers_seller_phone")
	val customersSellerPhone: String? = null
)