package com.noty_team.damfastore.base.fragment

import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.noty_team.damfastore.base.activity.BaseActivity
import com.noty_team.damfastore.utils.Constants.appCounter
import com.noty_team.damfastore.utils.paper.PaperIO
import io.reactivex.disposables.CompositeDisposable
import noty_team.com.urger.utils.cicerone.BackButtonListener
import noty_team.com.urger.utils.cicerone.RouterProvider
import noty_team.com.urger.utils.cicerone.Screens
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit
import javax.inject.Inject

abstract class BaseFragment<P : BasePresenter> : Fragment(), BackButtonListener {

    @Inject
    lateinit var baseContext: Context

    @Inject
    lateinit var baseActivity: BaseActivity

    @Inject
    lateinit var subscriptions: CompositeDisposable

    protected var rootView: View? = null

    open var presenter: P? = null

    @LayoutRes
    protected abstract fun layout(): Int

    protected abstract fun initialization(view: View, isFirstInit: Boolean)

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            (context as BaseActivity).fragmentComponent.inject(this as BaseFragment<BasePresenter>)
        } catch (e: UninitializedPropertyAccessException) {
            (context as BaseActivity).initFragmentComponent()
            context.fragmentComponent.inject(this as BaseFragment<BasePresenter>)
        }

        baseActivity = (context as BaseActivity)

    }


    // fun startDownload() = baseActivity.startDownloadImage()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = if (layout() != 0)
            inflater.inflate(layout(), container, false)
        else
            super.onCreateView(inflater, container, savedInstanceState)
        return if (rootView == null) view else rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        initialization(view, rootView == null)
        rootView = view
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroy() {
        baseActivity.toggleKeyboard(false)
        subscriptions.clear()
        presenter?.clearDisposable()
        super.onDestroy()
    }

    fun View.getClick(durationMillis: Long = 600, onClick: (view: View) -> Unit) {
        RxView.clicks(this)
            .throttleFirst(durationMillis, TimeUnit.MILLISECONDS)
            .subscribe({ _ ->
                onClick(this)
            }, {

            })
            .also { subscriptions.add(it) }
    }

    fun getScreenRouter(): Router {
        return (parentFragment as RouterProvider).getRouter()
    }


    override fun onBackPressed(): Boolean {
        if (parentFragment != null && parentFragment is RouterProvider) getScreenRouter().exit()
        else baseActivity.exit()
        return true
    }

    fun <P : BasePresenter> replaceFragment(fragment: BaseFragment<P>) {
        baseActivity.toggleKeyboard(false)
        if (parentFragment != null && parentFragment is RouterProvider) getScreenRouter().replaceScreen(
            Screens.FragmentScreen(
                fragment
            )
        )
        else baseActivity.replaceFragment(fragment)
    }


    /* fun myOnBack(doo:()->Unit) = {
         doo

     }*/

    fun <P : BasePresenter> addFragment(fragment: BaseFragment<P>) {
        baseActivity.toggleKeyboard(false)
        val fm = baseActivity.supportFragmentManager
        val fragments = fm.fragments
        var isRouterFound = false
        fragments.forEach {
            if (it is RouterProvider) {
                isRouterFound = true
                it.getRouter().navigateTo(Screens.FragmentScreen(fragment))
                return
            }
        }
        if (!isRouterFound) baseActivity.addFragment(fragment)
    }

    fun <P : BasePresenter> newRootFragment(fragment: BaseFragment<P>) {
        if (parentFragment != null && parentFragment is RouterProvider) getScreenRouter().newRootScreen(
            Screens.FragmentScreen(
                fragment
            )
        )
        else baseActivity.newRootFragment(fragment)
    }

    //цицерон бля
    fun navigateTo(clazz: Class<*>) {
        val fm = baseActivity.supportFragmentManager
        val fragments = fm.fragments
        var isRouterFound = false
        fragments.forEach {


            val d = it.fragmentManager?.fragments

            d?.forEach {
                if (it.javaClass == clazz && it is RouterProvider) {
                    isRouterFound = true
                    it.getRouter()
                        .navigateTo(Screens.FragmentScreen(it as BaseFragment<BasePresenter>))
                    return
                }
            }
        }
    }

    fun isShowLoadingDialog(isShow: Boolean) = baseActivity.isShowLoadingDialog(isShow)

    fun showShortToast(toasMassage: String) = baseActivity.showShortToast(toasMassage)

    fun showLongToast(toasMassage: String) = baseActivity.showLongToast(toasMassage)
}