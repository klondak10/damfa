package com.noty_team.damfastore.base.api.response.create_new_seller

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import com.noty_team.damfastore.ui.recycler.item_adapter.SellerItem

@Generated("com.robohorse.robopojogenerator")
data class CreateNewSellerResponse(

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("action")
	val action: String? = null,

	@field:SerializedName("message")
	val message: Message? = null,

	@field:SerializedName("error")
	val error: String? = null
){
	fun mapToSeller(): SellerItem {


		return SellerItem(
			message?.customersSellerName,
			message?.customersSellerStatus,
			"123456",
			message?.customersSellerId,
			message?.customersSellerCompany,
			message?.customersSellerEmail,
			message?.customersSellerPhone
		)
	}
}