package com.noty_team.damfastore.base.api.response

import com.google.gson.annotations.SerializedName

open class BaseResponse {
    @field:SerializedName("error")
    var error: String = ""
}