package com.noty_team.damfastore.base.api.response.test

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Response(

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("action")
	val action: String? = null,

	@field:SerializedName("message")
	val message: List<MessageItem?>? = null
)