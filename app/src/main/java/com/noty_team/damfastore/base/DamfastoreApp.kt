package com.noty_team.damfastore.base

import android.app.Activity
import android.app.Application
import android.content.Context
import com.noty_team.damfastore.dagger.components.AppComponent
import com.noty_team.damfastore.dagger.components.DaggerAppComponent
import com.noty_team.damfastore.dagger.modules.AppModule
import com.noty_team.damfastore.dagger.modules.RetrofitApiModule
import io.paperdb.Paper
import noty_team.com.urger.utils.cicerone.LocalCiceroneHolder

class DamfastoreApp : Application() {

    companion object {

        val localCiceroneHolder: LocalCiceroneHolder by lazy {
            LocalCiceroneHolder()
        }

        lateinit var instance: DamfastoreApp
            private set

        operator fun get(activity: Activity): DamfastoreApp {
            return activity.application as DamfastoreApp
        }

        operator fun get(context: Context): DamfastoreApp {
            return context as DamfastoreApp
        }
    }

    val appComponent: AppComponent by lazy {
        initDagger()
    }

    private fun initDagger(): AppComponent =
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .retrofitApiModule(RetrofitApiModule())
            .build()

    override fun onCreate() {
        super.onCreate()
        instance = this
        Paper.init(applicationContext)
    }

}