package com.noty_team.damfastore.base.fragment

import android.content.Context
import android.widget.Toast
import com.noty_team.damfastore.base.DamfastoreApp
import com.noty_team.damfastore.base.api.retrofit.DamfastoreAdministrator
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

open class BasePresenter(val context: Context) : BaseContract.Presenter {

    @Inject
    lateinit var apiAuthorization: DamfastoreAdministrator

    @Inject
    lateinit var subscriptions: CompositeDisposable

    init {
        DamfastoreApp[context].appComponent.inject(this)
    }

    fun <T> Single<T>.callRequest() {
        this.subscribe({}, {

        }).also { subscriptions.add(it) }
    }

    override fun clearDisposable() {
        subscriptions.clear()
    }

    fun showLongToast(massage: String) {
        Toast.makeText(context, massage, Toast.LENGTH_LONG).show()
    }

    fun showShortToast(massage: String) {
        Toast.makeText(context, massage, Toast.LENGTH_SHORT).show()
    }
}