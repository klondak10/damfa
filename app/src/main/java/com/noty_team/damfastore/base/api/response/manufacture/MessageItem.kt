package com.noty_team.damfastore.base.api.response.manufacture

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import com.noty_team.damfastore.base.api.response.test.MessageItem

@Generated("com.robohorse.robopojogenerator")
data class MessageItem(

	@field:SerializedName("manufacturers_name")
	val manufacturersName: String? = null,

	@field:SerializedName("manufacturers_id")
	val manufacturersId: String? = null,

    @field:SerializedName("products")
    val products: List<String>? = null
)