package com.noty_team.damfastore.base.api.response.manufacture

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import com.noty_team.damfastore.ui.recycler.item_adapter.CategoriesManufactureItem

@Generated("com.robohorse.robopojogenerator")
data class GetManufactureResponse(

    @field:SerializedName("success")
    val success: Boolean? = null,

    @field:SerializedName("action")
    val action: String? = null,

    @field:SerializedName("message")
    val message: List<MessageItem?>? = null
) {
    fun mapToManufactureRecycler(): ArrayList<CategoriesManufactureItem> {

        return message?.flatMap {
            arrayListOf(
                CategoriesManufactureItem(
                    id = it?.manufacturersId ?: "",
                    isSelected = false,
                    manufactureName = it?.manufacturersName ?: "",
                    listProducts = it?.products ?: arrayListOf()
                )
            )
        } as ArrayList<CategoriesManufactureItem>

    }
}