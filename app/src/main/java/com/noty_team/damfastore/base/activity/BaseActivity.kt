package com.noty_team.damfastore.base.activity

import android.app.Dialog
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.RelativeLayout
import android.widget.Toast
import com.jakewharton.rxbinding2.view.RxView
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.DamfastoreApp
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.base.fragment.BasePresenter
import com.noty_team.damfastore.dagger.components.FragmentComponent
import com.noty_team.damfastore.dagger.modules.FragmentModule
import com.noty_team.damfastore.utils.paper.PaperIO
import com.noty_team.damfastore.utils.paper.products.ProductItem
import io.reactivex.disposables.CompositeDisposable
import noty_team.com.urger.utils.cicerone.Screens
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.*
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity() {

    lateinit var fragmentComponent: FragmentComponent

    @Inject
    lateinit var context: Context

    @Inject
    lateinit var subscriptions: CompositeDisposable

    lateinit var mainRouter: Router

    //private var presenter = PresenterForDownload()

    private lateinit var loadingDialog: Dialog

    lateinit var navigatorHolder: NavigatorHolder

    val ciceroneNavigator = object : SupportAppNavigator(this, R.id.main_fragment_container) {
        override fun applyCommands(commands: Array<Command>) {
            super.applyCommands(commands)
            supportFragmentManager.executePendingTransactions()
        }
    }

    abstract fun layout(): Int


    abstract fun initialization()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initCicerone()

        initFragmentComponent()

      //  addCounterApp()

        intiLoadingDialog()

        if (layout() != 0) {
            setContentView(layout())
            initialization()

        }
    }


    fun initFragmentComponent() {
        if (!::fragmentComponent.isInitialized) {
            fragmentComponent = DamfastoreApp[this].appComponent.plus(FragmentModule(this))
            fragmentComponent.inject(this)
        }
    }

    private fun intiLoadingDialog() {
        loadingDialog = Dialog(this)

        loadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        loadingDialog.setContentView(R.layout.base_loading_dialog)


        loadingDialog.setOnCancelListener {
            onBackPressed()
            isShowLoadingDialog(false)
        }
        loadingDialog.setCanceledOnTouchOutside(false)
    }


    fun toggleKeyboard(show: Boolean) {
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (!show)
            inputMethodManager.hideSoftInputFromWindow(window.decorView.windowToken, 0)
        else
            inputMethodManager.toggleSoftInputFromWindow(
                window.decorView.windowToken,
                InputMethodManager.SHOW_FORCED,
                0
            )
    }

    override fun onBackPressed() {

        val b = findViewById<DrawerLayout>(R.id.drawer)

        if (b != null) {
            if (b.isDrawerOpen(GravityCompat.END)) {
                b.closeDrawer(GravityCompat.END)
            } else {
                // exit()
                super.onBackPressed()
            }
        } else {

            super.onBackPressed()
        }
    }

    private fun initCicerone() {
        val cicerone = Cicerone.create()
        mainRouter = cicerone.router
        navigatorHolder = cicerone.navigatorHolder
    }

    fun <P : BasePresenter> replaceFragment(fragment: BaseFragment<P>) {
        ciceroneNavigator.applyCommands(arrayOf(Replace(Screens.FragmentScreen(fragment))))
    }

    fun <P : BasePresenter> addFragment(fragment: BaseFragment<P>) {
        ciceroneNavigator.applyCommands(arrayOf(Forward(Screens.FragmentScreen(fragment))))
    }

    fun <P : BasePresenter> newRootFragment(fragment: BaseFragment<P>) {
        ciceroneNavigator.applyCommands(
            arrayOf(
                BackTo(null),
                Replace(Screens.FragmentScreen(fragment))
            )
        )
    }

    fun exit() {
        ciceroneNavigator.applyCommands(arrayOf(Back()))
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(ciceroneNavigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    fun View.getClick(durationMillis: Long = 1000, onClick: (view: View) -> Unit) {
        RxView.clicks(this)
            .throttleFirst(durationMillis, TimeUnit.MILLISECONDS)
            .subscribe({ _ ->
                onClick(this)
            }, {

            })
            .also { subscriptions.add(it) }
    }

    fun isShowLoadingDialog(isShow: Boolean) {
        if (isShow) {
            loadingDialog.show()
        } else {
            loadingDialog.dismiss()
        }

        val window = loadingDialog.getWindow()
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
        loadingDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent)
    }

    fun showShortToast(massage: String) {
        Toast.makeText(this, massage, Toast.LENGTH_SHORT).show()
    }

    fun showLongToast(massage: String) {
        Toast.makeText(this, massage, Toast.LENGTH_LONG).show()
    }


    private val onDownloadComplete = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            //Fetching the download id received with the broadcast
            val id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
            //Checking if the received broadcast is for our enqueued download by matching download id
            if (id == downloadID) {
                //TODO return
                PaperIO.products = response
            }
        }
    }

    private var downloadID: Long = 0

    val response = arrayListOf<ProductItem>()


    /*fun startDownloadImage() {


        this.registerReceiver(
            onDownloadComplete,
            IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        )


        var b = Single.fromCallable {
            presenter.loadAllImages(PaperIO.products, onItemComplete = { productItem ->
                val imageParse = beginDownload(
                    productItem.productsAppName ?: "",
                    productItem.productsImage ?: ""
                )
                productItem.image = imageParse

                response.add(productItem)
            }, onComplete = {
                PaperIO.products = response

            })
        }.subscribeOn(Schedulers.io())
            .observeOn(Schedulers.newThread())

        b.subscribe()

    }*/

    private fun beginDownload(imageName: String, addressLink: String): String {

        if (addressLink.isNotEmpty()) {


            val nameDir =
                /*if (imageName.isNotEmpty()) {
                    imageName
                } else {*/
                UUID.randomUUID().toString()
            //}

            val file =
                File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES), nameDir)

            val request = DownloadManager.Request(Uri.parse(addressLink))
                .setTitle(nameDir)
                .setDescription("Downloading")
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                .setDestinationUri(Uri.fromFile(file))// Uri of the destination file
                .setAllowedOverMetered(true)
                .setAllowedOverRoaming(true)


            val downloadManager =
                context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager


            downloadID = downloadManager.enqueue(request)

            return file.absolutePath
        } else return ""
    }

}