package com.noty_team.damfastore.base.api.response.login_seller

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Message(

    @field:SerializedName("admin_exist")
    val adminExist: Boolean? = null,
    @field:SerializedName("customers_seller_id")
    val customers_seller_id: String? = null
)