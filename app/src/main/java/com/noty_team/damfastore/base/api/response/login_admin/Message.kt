package com.noty_team.damfastore.base.api.response.login_admin

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Message(

	@field:SerializedName("admin_exist")
	val adminExist: Boolean? = null
)