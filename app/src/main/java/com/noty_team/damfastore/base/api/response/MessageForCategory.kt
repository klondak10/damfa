package com.noty_team.damfastore.base.api.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
class MessageForCategory(
    @field:SerializedName("categories_app_id")
    val categoriesAppId: String? = null,
    @field:SerializedName("categories_app_seller_id")
    val categoriesAppSellerId: String? = null,
    @field:SerializedName("categories_app_category_id")
    val categoriesAppCategoryId: String? = null,
    @field:SerializedName("categories_app_status")
    val categoriesAppStatus: String? = null
) {
    override fun toString(): String {
        return "MessageForCategory(categoriesAppId=$categoriesAppId, categoriesAppSellerId=$categoriesAppSellerId, categoriesAppCategoryId=$categoriesAppCategoryId, categoriesAppStatus=$categoriesAppStatus)"
    }
}
