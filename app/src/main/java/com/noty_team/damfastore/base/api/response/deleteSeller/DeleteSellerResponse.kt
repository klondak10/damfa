package com.noty_team.damfastore.base.api.response.deleteSeller

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class DeleteSellerResponse(

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("action")
	val action: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("error")
	val error: String? = null
)