package com.noty_team.damfastore.base.api.response.products

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class MessageItem(

	@field:SerializedName("products_image")
	val productsImage: String? = null,

	@field:SerializedName("manufacturers_name")
	val manufacturersName: String? = null,

	@field:SerializedName("products_id")
	val productsId: String? = null,

	@field:SerializedName("manufacturers_id")
	val manufacturersId: String? = null,

	@field:SerializedName("products_model")
	val productsModel: String? = null,

	@field:SerializedName("products_app_id")
	val productsAppId: String? = null,

	@field:SerializedName("products_app_name")
	val productsAppName: String? = null,

    @field:SerializedName("filter_id")
    val filterId: List<String>? = null,

	@field:SerializedName("filter_name")
    val filterName: List<String>? = null,

	@field:SerializedName("products_description")
	val productsDescription: String? = null
)