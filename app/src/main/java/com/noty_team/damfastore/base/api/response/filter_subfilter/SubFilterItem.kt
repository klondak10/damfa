package com.noty_team.damfastore.base.api.response.filter_subfilter

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class SubFilterItem(

	@field:SerializedName("filter_name")
	val filterName: String? = null,

	@field:SerializedName("filter_id")
	val filterId: String? = null,

	@field:SerializedName("filter_parent")
	val filterParent: String? = null
)