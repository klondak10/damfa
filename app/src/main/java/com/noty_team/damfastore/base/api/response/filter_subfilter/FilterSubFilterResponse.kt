package com.noty_team.damfastore.base.api.response.filter_subfilter

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import com.noty_team.damfastore.utils.paper.filters.FilterItem
import com.noty_team.damfastore.utils.paper.filters.subfilter.SubFilter
import kotlin.Comparator
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@Generated("com.robohorse.robopojogenerator")
data class FilterSubFilterResponse(

    @field:SerializedName("success")
    val success: Boolean? = null,

    @field:SerializedName("action")
    val action: String? = null,

    @field:SerializedName("message")
    val message: List<MessageItem?>? = null,

    @field:SerializedName("error")
    val error: String? = null
) {
    fun mapToFilterList(): ArrayList<FilterItem> {


        val list = ArrayList<FilterItem>()
        message?.forEach {
            if (it?.filterId != null && it.filterName != null) {
                list.add(FilterItem(it.filterName, it.filterId, isSelected = false))
            }
        }

        list.sortWith(Comparator { o1, o2 ->
            o1.filterName.compareTo(o2.filterName, true)
        })

        return list
    }

    fun mapToSubfilter(): HashMap<String, ArrayList<SubFilter>> {
        val mapList = HashMap<String, ArrayList<SubFilter>>()

        if (!message.isNullOrEmpty())
            for (i in message.indices) {
                mapList[message[i]?.filterId ?: ""] = message[i]?.subFilter?.flatMap {
                    arrayListOf(
                        SubFilter(it?.filterId ?: "", it?.filterName ?: "", false)
                    )
                } as ArrayList<SubFilter>
            }
        return mapList

/*

        message?.flatMap {
           hashMapOf<String, List<SubFilter>>([it?.filterId ?: ""] = it?.subFilter?.flatMap {
               listOf(
                   SubFilter(it?.filterId ?: "", it?.filterName ?: "", false)
               )
           } as List<SubFilter>)
       } as HashMap<String, List<SubFilter>>
*/


    }

    fun mapToCounter(): HashMap<String, Int> {
        val mapList = HashMap<String, Int>()

        if (!message.isNullOrEmpty())
            for (i in message.indices) {
                mapList[message[i]?.filterId ?: ""] = 0
            }
        return mapList
    }
}