package com.noty_team.damfastore.base.api.response.filters

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import com.noty_team.damfastore.ui.recycler.item_adapter.FilterItem

@Generated("com.robohorse.robopojogenerator")
data class FilterResponse(

    @field:SerializedName("success")
    val success: Boolean? = null,

    @field:SerializedName("action")
    val action: String? = null,

    @field:SerializedName("message")
    val message: List<MessageItem?>? = null
) {
    fun mapToFilterItem(): ArrayList<FilterItem> {
        return message?.flatMap {
            arrayListOf(
                FilterItem(
                    filterId = it?.filterId ?: "",
                    isSelected = false,
                    filterName = it?.filterName ?: ""
                )
            )
        } as ArrayList<FilterItem>
    }
}