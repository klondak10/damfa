package com.noty_team.damfastore.base.api.response.base64

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class MessageItem(

	@field:SerializedName("product_id")
	val productId: String? = null,

	@field:SerializedName("product_img")
	val productImg: String? = null
)