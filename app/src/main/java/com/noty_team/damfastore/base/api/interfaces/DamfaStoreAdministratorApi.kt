package com.noty_team.damfastore.base.api.interfaces

import com.noty_team.damfastore.base.api.response.CreateCategoriesIdsResponse
import com.noty_team.damfastore.base.api.response.GetCategoriesIdsResponse
import com.noty_team.damfastore.base.api.response.base64.Base64Response
import com.noty_team.damfastore.base.api.response.category.CategoryResponse
import com.noty_team.damfastore.base.api.response.create_new_seller.CreateNewSellerResponse
import com.noty_team.damfastore.base.api.response.deleteSeller.DeleteSellerResponse
import com.noty_team.damfastore.base.api.response.edit_seller.EditSellerResponse
import com.noty_team.damfastore.base.api.response.filter_subfilter.FilterSubFilterResponse
import com.noty_team.damfastore.base.api.response.filters.FilterResponse
import com.noty_team.damfastore.base.api.response.login_seller.LoginSellerResponse
import com.noty_team.damfastore.base.api.response.manufacture.GetManufactureResponse
import com.noty_team.damfastore.base.api.response.new_login_admin.NewLoginAdminResponse
import com.noty_team.damfastore.base.api.response.products.ProductsResponse
import com.noty_team.damfastore.base.api.response.sellers_pagination.GetAllSellersResponse
import io.reactivex.Single
import retrofit2.http.*

@JvmSuppressWildcards
interface DamfaStoreAdministratorApi {

    //@FormUrlEncoded
    @POST("/shop.php?do=RestApi/LoginAdmin")
    fun loginAdmin(
        @Query("email") email: String,
        @Query("password") password: String
    ): Single<NewLoginAdminResponse>


    // @FormUrlEncoded
    @POST("/shop.php?do=RestApi/LoginSeller")
    fun loginSeller(
        @Query("email") email: String,
        @Query("password") password: String
    ): Single<LoginSellerResponse>


    @FormUrlEncoded
    @POST("/shop.php?do=RestApi/InsertSeller")
    fun createNewSeller(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("name") name: String,
        @Field("company") company: String,
        @Field("phone") phone: String
    ): Single<CreateNewSellerResponse>


    @GET("/shop.php?do=RestApi/GetAllProductsBase64")
    fun getBase64(): Single<Base64Response>

    // @FormUrlEncoded
    @DELETE("/shop.php?do=RestApi/DeleteSeller")
    fun deleteSeller(
        @Query("seller_id") sellerId: String
    ): Single<DeleteSellerResponse>


    @GET("/shop.php?do=RestApi/GetSellers")
    fun getSellersPagination(
        /*   @Field("limit") limit: Int,
           @Field("offset") offset: Int*/
    ): Single<GetAllSellersResponse>


    // @FormUrlEncoded
    @PUT("/shop.php?do=RestApi/EditSeller")
    fun editSeller(
        @Query("seller_id") id: String,
        @Query("email") email: String,
        @Query("password") password: String,
        @Query("name") name: String,
        @Query("company") company: String,
        @Query("phone") phone: String
    ): Single<EditSellerResponse>


    @GET("/shop.php?do=RestApi/GetManufacture")
    fun getManufacture(): Single<GetManufactureResponse>

    @GET("/shop.php?do=RestApi/GetCategory")
    fun getCategory(): Single<CategoryResponse>

    @GET("/shop.php?do=RestApi/GetProducts")
    fun getProducts(): Single<ProductsResponse>


    @GET("/shop.php?do=RestApi/GetFilters")
    fun getFilters(): Single<FilterResponse>

    @GET("/shop.php?do=RestApi/GetFilterToSubfilter")
    fun getFiltersSubfilters(): Single<FilterSubFilterResponse>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("/shop.php?do=RestApi/CreateGategoriesIdsBySeller")
    fun сreateCategoriesIdsBySeller(
        @Field("seller_id") seller_id: Int,
        @FieldMap categories_ids: HashMap<String, Any>
    ): Single<CreateCategoriesIdsResponse>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("/shop.php?do=RestApi/GetGategoriesIdsBySeller")
    fun getCategoriesIdsBySeller(
        @Field("seller_id") seller_id: Int
    ): Single<GetCategoriesIdsResponse>
}