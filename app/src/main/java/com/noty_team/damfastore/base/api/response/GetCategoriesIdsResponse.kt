package com.noty_team.damfastore.base.api.response

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
class GetCategoriesIdsResponse(
    @field:SerializedName("action")
    val action: String? = null,

    @field:SerializedName("success")
    val success: Boolean? = null,

    @field:SerializedName("message")
    val message: List<MessageForCategory>? = null,

    @field:SerializedName("error")
    val error: String? = null
) {

}
