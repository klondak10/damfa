package com.noty_team.damfastore.base.api.response.category

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class MessageItem(

    @field:SerializedName("categories_name")
    val categoriesName: String? = null,

    @field:SerializedName("categories_id")
    val categoriesId: String? = null,

    @field:SerializedName("products")
    val products: List<String>? = null
)