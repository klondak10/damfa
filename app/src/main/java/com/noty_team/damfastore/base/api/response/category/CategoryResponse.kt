package com.noty_team.damfastore.base.api.response.category

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import com.noty_team.damfastore.ui.recycler.item_adapter.CategoriesManufactureItem

@Generated("com.robohorse.robopojogenerator")
data class CategoryResponse(

    @field:SerializedName("success")
    val success: Boolean? = null,

    @field:SerializedName("action")
    val action: String? = null,

    @field:SerializedName("message")
    val message: List<MessageItem?>? = null,

    @field:SerializedName("error")
    val error: String? = null
) {
    fun mapToCategory(): ArrayList<CategoriesManufactureItem> {

        return message?.flatMap {
            arrayListOf(
                CategoriesManufactureItem(
                    id = it?.categoriesId ?: "",
                    manufactureName = it?.categoriesName ?: "",
                    isSelected = false,
                    listProducts = it?.products ?: arrayListOf()
                )
            )

        } as ArrayList<CategoriesManufactureItem>

    }
}