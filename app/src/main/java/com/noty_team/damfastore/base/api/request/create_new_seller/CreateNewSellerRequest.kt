package com.noty_team.damfastore.base.api.request.create_new_seller

data class CreateNewSellerRequest(
    val email: String = "test@email.com",
    val password: String = "123456",
    val name: String = "test",
    val company: String = "test",
    val phone: String = "06654682"
)