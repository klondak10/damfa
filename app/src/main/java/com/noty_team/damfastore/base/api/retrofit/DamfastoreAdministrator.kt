package com.noty_team.damfastore.base.api.retrofit

import android.content.Context
import com.noty_team.damfastore.base.DamfastoreApp
import com.noty_team.damfastore.base.api.interfaces.DamfaStoreAdministratorApi
import com.noty_team.damfastore.base.api.request.create_new_seller.CreateNewSellerRequest
import com.noty_team.damfastore.base.api.request.login_admin.LoginAdminRequest
import com.noty_team.damfastore.base.api.request.login_seller.LoginSellerRequest
import com.noty_team.damfastore.base.api.response.CreateCategoriesIdsResponse
import com.noty_team.damfastore.base.api.response.GetCategoriesIdsResponse
import com.noty_team.damfastore.base.api.response.base64.Base64Response
import com.noty_team.damfastore.base.api.response.category.CategoryResponse
import com.noty_team.damfastore.base.api.response.create_new_seller.CreateNewSellerResponse
import com.noty_team.damfastore.base.api.response.deleteSeller.DeleteSellerResponse
import com.noty_team.damfastore.base.api.response.edit_seller.EditSellerResponse
import com.noty_team.damfastore.base.api.response.filter_subfilter.FilterSubFilterResponse
import com.noty_team.damfastore.base.api.response.filters.FilterResponse
import com.noty_team.damfastore.base.api.response.login_admin.LoginAdminResponse
import com.noty_team.damfastore.base.api.response.login_seller.LoginSellerResponse
import com.noty_team.damfastore.base.api.response.manufacture.GetManufactureResponse
import com.noty_team.damfastore.base.api.response.new_login_admin.NewLoginAdminResponse
import com.noty_team.damfastore.base.api.response.products.ProductsResponse
import com.noty_team.damfastore.base.api.response.sellers_pagination.GetAllSellersResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import java.lang.Exception
import java.lang.ref.WeakReference
import javax.inject.Inject

class DamfastoreAdministrator @Inject constructor(
    val context: Context,
    val retrofit: Retrofit
) {
    @Inject
    lateinit var damfastoreAdministratorApi: DamfaStoreAdministratorApi

    init {
        DamfastoreApp[context].appComponent.inject(this)
    }


    fun getBase64(
        onSuccess: (response: Base64Response) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit

    ): Single<Base64Response> {
        return initRequestSingleForResult(
            damfastoreAdministratorApi.getBase64(),
            onSuccess,
            onError
        )
    }

    fun loginAdmin(
        loginAdminRequest: LoginAdminRequest,
        onSuccess: (response: NewLoginAdminResponse) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit
    ): Single<NewLoginAdminResponse> {

        return initRequestSingleForResult(
            damfastoreAdministratorApi.loginAdmin(
                loginAdminRequest.email,
                loginAdminRequest.password
            ), onSuccess, onError
        )
    }

    fun loginSeller(
        loginSellerRequest: LoginSellerRequest,
        onSuccess: (response: LoginSellerResponse) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit

    ): Single<LoginSellerResponse> {
        return initRequestSingleForResult(
            damfastoreAdministratorApi.loginSeller(
                loginSellerRequest.email,
                loginSellerRequest.password
            ), onSuccess, onError
        )
    }

    fun createNewSeller(
        createNewSellerRequest: CreateNewSellerRequest,
        onSuccess: (response: CreateNewSellerResponse) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit
    ): Single<CreateNewSellerResponse> {

        return initRequestSingleForResult(
            damfastoreAdministratorApi.createNewSeller(
                createNewSellerRequest.email,
                createNewSellerRequest.password,
                createNewSellerRequest.name,
                createNewSellerRequest.company,
                createNewSellerRequest.phone
            ), onSuccess, onError
        )

    }

    fun deleteSeller(
        sellerId: String,
        onSuccess: (response: DeleteSellerResponse) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit
    ): Single<DeleteSellerResponse> {


        return initRequestSingleForResult(
            damfastoreAdministratorApi.deleteSeller(sellerId), onSuccess, onError
        )
    }

    fun editSeller(
        id: String,
        email: String,
        password: String,
        name: String,
        company: String,
        phone: String,
        onSuccess: (response: EditSellerResponse) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit
    ): Single<EditSellerResponse> {
        return initRequestSingleForResult(
            damfastoreAdministratorApi.editSeller(
                id,
                email,
                password,
                name,
                company,
                phone
            ), onSuccess, onError
        )
    }

    fun getSellerPagination(
        pageSize: Int,
        page: Int,
        onSuccess: (response: GetAllSellersResponse) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit
    ): Single<GetAllSellersResponse> {
        return initRequestSingleForResult(
            damfastoreAdministratorApi.getSellersPagination(/*pageSize, page*/),
            onSuccess, onError
        )
    }

    fun getManufacture(
        onSuccess: (response: GetManufactureResponse) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit
    ): Single<GetManufactureResponse> {

        return initRequestSingleForResult(
            damfastoreAdministratorApi.getManufacture(),
            onSuccess,
            onError
        )
    }

    fun getCategory(
        onSuccess: (response: CategoryResponse) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit
    ): Single<CategoryResponse> {
        return initRequestSingleForResult(
            damfastoreAdministratorApi.getCategory(),
            onSuccess,
            onError
        )
    }

    fun getProducts(
        onSuccess: (response: ProductsResponse) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit
    ): Single<ProductsResponse> {

        return initRequestSingleForResult(
            damfastoreAdministratorApi.getProducts(),
            onSuccess,
            onError
        )
    }

    fun getFilter(
        onSuccess: (response: FilterResponse) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit
    ): Single<FilterResponse> {
        return initRequestSingleForResult(
            damfastoreAdministratorApi.getFilters(),
            onSuccess,
            onError
        )
    }

    fun getFilterSubfilter(
        onSuccess: (response: FilterSubFilterResponse) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit
    ): Single<FilterSubFilterResponse> {
        return initRequestSingleForResult(
            damfastoreAdministratorApi.getFiltersSubfilters(),
            onSuccess, onError
        )
    }

    fun сreateCategoriesIdsBySeller(
        seller_id: Int,
        categories_ids: HashMap<String, Any>,
        onSuccess: (response: CreateCategoriesIdsResponse) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit
    ): Single<CreateCategoriesIdsResponse> {
        return initRequestSingleForResult(
            damfastoreAdministratorApi.сreateCategoriesIdsBySeller(seller_id, categories_ids),
            onSuccess, onError
        )
    }

    fun getCategoriesIdsBySeller(
        seller_id: Int,
        onSuccess: (response: GetCategoriesIdsResponse) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit
    ): Single<GetCategoriesIdsResponse> {
        return initRequestSingleForResult(
            damfastoreAdministratorApi.getCategoriesIdsBySeller(seller_id),
            onSuccess, onError
        )
    }

    private fun <T, V> initRequestSingleForResult(
        request: Single<T>,
        onSuccess: (response: V) -> Unit,
        onError: (errorMessage: String, errorCode: Int) -> Unit
    ): Single<T> {
        val callback =
            CallbackWrapper<T, V>(WeakReference(context), retrofit, onSuccess, onError)
        val requestSingle = request.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess {
                callback.onSuccess(it)
            }
            .doOnError {
                callback.onError(it)
            }
        callback.request = requestSingle
        return requestSingle


    }


    fun zip(requests: ArrayList<Single<*>>, onLoadingEnded: () -> Unit): Disposable {
        return Single.zip(requests) { result -> }.subscribe({ onLoadingEnded() }, {})
    }
}
