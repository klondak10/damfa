package com.noty_team.damfastore.base.fragment

interface BaseContract {

	interface Presenter {
		fun clearDisposable()
	}
}