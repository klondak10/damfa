package com.noty_team.damfastore.base.api.response.sellers_pagination

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import com.noty_team.damfastore.ui.recycler.item_adapter.SellerItem

@Generated("com.robohorse.robopojogenerator")
data class GetAllSellersResponse(

    @field:SerializedName("success")
    val success: Boolean? = null,

    @field:SerializedName("action")
    val action: String? = null,

    @field:SerializedName("message")
    val message: List<MessageItem?>? = null,

    @field:SerializedName("error")
    val error: String? = null

) {
    fun mapResponse(): ArrayList<SellerItem> {

        return message?.flatMap {
            arrayListOf(
                SellerItem(
                    customersSellerName = it?.customersSellerName ?: "",
                    customersSellerStatus = it?.customersSellerStatus ?: false,
                    customersSellerPassword = it?.customersSellerPassword ?: "",
                    customersSellerId = it?.customersSellerId ?: "",
                    customersSellerCompany = it?.customersSellerCompany ?: "",
                    customersSellerEmail = it?.customersSellerEmail ?: "",
                    customersSellerPhone = it?.customersSellerPhone ?: ""
                )
            )
        } as ArrayList<SellerItem>
    }
}