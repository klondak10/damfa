package com.noty_team.damfastore.base.activity

import com.noty_team.damfastore.utils.paper.products.ProductItem
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class PresenterForDownload {

    var subscriptions = CompositeDisposable()

    fun loadAllImages(
        list: ArrayList<ProductItem>,
        onItemComplete: (productItem: ProductItem) -> Unit,
        onComplete: () -> Unit
    ) {

        val listt = list


        Observable.fromIterable(listt)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.newThread())
            .subscribe(object : Observer<ProductItem> {
                override fun onComplete() {
                    onComplete()
                }

                override fun onSubscribe(d: Disposable) {
                    subscriptions.add(d)
                }

                override fun onNext(t: ProductItem) {
                    onItemComplete(t)

                }

                override fun onError(e: Throwable) {
                }
            })

    }
}