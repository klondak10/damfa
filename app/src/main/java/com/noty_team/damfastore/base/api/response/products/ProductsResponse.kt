package com.noty_team.damfastore.base.api.response.products

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import com.noty_team.damfastore.R
import com.noty_team.damfastore.utils.paper.products.ProductItem
import java.text.Normalizer
import java.util.regex.Pattern

@Generated("com.robohorse.robopojogenerator")
data class ProductsResponse(

    @field:SerializedName("success")
    val success: String? = null,

    @field:SerializedName("action")
    val action: String? = null,

    @field:SerializedName("message")
    val message: List<MessageItem?>? = null
) {
    fun mapToProductsList(): ArrayList<ProductItem> {

        return message?.flatMap {
            arrayListOf(
                ProductItem(
                    productsImage = "https://www.damfastore.de/${it?.productsImage}",
                    manufacturersName = it?.manufacturersName,
                    productsId = it?.productsId,
                    manufacturersId = it?.manufacturersId,
                    productsModel = it?.productsModel,
                    productsAppId = it?.productsAppId,
                    filterId = it?.filterId,
                    filter_name = it?.filterName,
                    productsAppName = it?.productsAppName,
                    productsDescription = normalize(it?.productsDescription ?: "")
                )
            )

        } as ArrayList<ProductItem>

    }

    fun loadImages(imageView: ImageView, context : Context, list: ArrayList<ProductItem>){
        /*list.forEach {
            if (it.productsImage != "")
                Glide.with(imageView.context)
                    .load()
                    .apply(
                        RequestOptions()
                            .encodeQuality(80)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .fitCenter()
                    )
            .into(imageView)
        }*/
    }


    private fun normalize(input: String): String {
        val d  = input.replace("\n","&lt;br&gt;")
       return d
    }
}