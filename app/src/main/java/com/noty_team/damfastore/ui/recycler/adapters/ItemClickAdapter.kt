package com.noty_team.damfastore.ui.recycler.adapters

interface ItemClickAdapter {
    fun onItemCLick(id:String, isSelected:Boolean)
}