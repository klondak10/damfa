package com.noty_team.damfastore.ui.fragment.login.seller.product_management.product_filters.subfilter

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.ui.fragment.login.customer.category.FragmentCustomerSearch
import com.noty_team.damfastore.ui.recycler.adapters.SubFilterAdapter
import com.noty_team.damfastore.utils.paper.PaperIO
import com.noty_team.damfastore.utils.paper.filters.subfilter.SubFilter
import kotlinx.android.synthetic.main.fragment_product_subfilter.*
import kotlinx.android.synthetic.main.fragment_product_subfilter.product_content_search

class FragmentSubFilterProduct : BaseFragment<SubFilterProductPresenter>() {

    companion object {
        fun newInstance() = FragmentSubFilterProduct()
    }

    var filterID: String = "1453"
    var filterName: String = "Geschmack"

    lateinit var adapterSubfilter: SubFilterAdapter

    var callbackSelectedButton: (isSelected: Boolean, filterID: String) -> Unit =
        { _, _ -> }

    override fun layout() = R.layout.fragment_product_subfilter


    override fun initialization(view: View, isFirstInit: Boolean) {


        initView()
        getData()

    }

    private fun initView() {

        product_content_search.setOnClickListener({ product_content_search.setIconified(false) })

        product_content_search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {

                 adapterSubfilter.filter(newText,ArrayList( PaperIO.getSublistByFilterId(filterID).sortedWith(compareBy({ it.subfilterName.toLowerCase() }))))

                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {


                return false
            }
        })

        subfilter_name.text = filterName
    }

    override fun onResume() {
        initAction()
        super.onResume()
    }

    private fun getData() {
        initRecycler(PaperIO.getSublistByFilterId(filterID))
    }

    private fun initRecycler(list: ArrayList<SubFilter>) {
        subfilter_recycler.layoutManager = LinearLayoutManager(baseContext)

        adapterSubfilter = SubFilterAdapter(ArrayList(list.sortedWith(compareBy({ it.subfilterName.toLowerCase()}))))

        subfilter_recycler.adapter = adapterSubfilter
    }

    private fun initAction() {
        back_product_subfilter.getClick {
            onBackPressed()
        }
    }

    override fun onBackPressed(): Boolean {
        saveFilters()
        return super.onBackPressed()
    }

    override fun onDetach() {
        saveFilters()
        super.onDetach()
    }

    private fun saveFilters() {
        if (adapterSubfilter.isNeedUpdateBDFilter) {
            PaperIO.setSubfilteryByFilterId(
                filterID,
                adapterSubfilter.List()
            )

            val lisrCounter = PaperIO.filterCounter

            lisrCounter[filterID] = adapterSubfilter.getBaseCount()

            if (adapterSubfilter.getBaseCount() > 0) {
                callbackSelectedButton(true, filterID)

                PaperIO.setFilterDbItemIsSelected(true, filterID) {

                }
            } else {
                callbackSelectedButton(false, filterID)

                PaperIO.setFilterDbItemIsSelected(false, filterID) {

                }
            }

            PaperIO.filterCounter = lisrCounter
        }
    }

}