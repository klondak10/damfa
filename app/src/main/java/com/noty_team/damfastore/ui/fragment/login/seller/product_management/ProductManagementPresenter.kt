package com.noty_team.damfastore.ui.fragment.login.seller.product_management

import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Base64OutputStream
import android.widget.ImageView
import com.noty_team.damfastore.base.api.response.CreateCategoriesIdsResponse
import com.noty_team.damfastore.base.api.response.GetCategoriesIdsResponse
import com.noty_team.damfastore.base.api.response.base64.Base64Response
import com.noty_team.damfastore.base.api.response.filter_subfilter.FilterSubFilterResponse
import com.noty_team.damfastore.base.api.response.filters.FilterResponse
import com.noty_team.damfastore.base.fragment.BasePresenter
import com.noty_team.damfastore.ui.recycler.item_adapter.CategoriesManufactureItem
import com.noty_team.damfastore.utils.paper.PaperIO
import com.noty_team.damfastore.utils.paper.products.ProductItem
import io.paperdb.Paper
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ProductManagementPresenter(context: Context) : BasePresenter(context) {

    fun getBaseData(
        onAllRequestsLoaded: () -> Unit,
        onCategorySuccess: (response: ArrayList<CategoriesManufactureItem>) -> Unit,
        onManufactureSuccess: (response: ArrayList<CategoriesManufactureItem>) -> Unit,
        onProductSuccess: (response: ArrayList<ProductItem>) -> Unit,
        onFilterSuccess: (response: FilterResponse) -> Unit,
        onFilterSubfilterSuccess: (response: FilterSubFilterResponse) -> Unit/*,
        onBase64Success: (responce: Base64Response) -> Unit = {},*/,
        onError: () -> Unit
    ) {
        apiAuthorization.zip(
            arrayListOf(
                getCategory(onCategorySuccess, onError),
                getManufacture(onManufactureSuccess, onError),
                getProducts(onProductSuccess, onError),
                getFilters(onFilterSuccess, onError),
                getFilterSubfilter(onFilterSubfilterSuccess, onError)/*,
                getBase64(onBase64Success, onError)*/
            ),
            onAllRequestsLoaded
        )
    }

    fun сreateCategoriesIdsBySeller(
        categories_ids: HashMap<String, Any>,
        onSuccess: (response: CreateCategoriesIdsResponse) -> Unit,
        onError: () -> Unit
    ) {
        val id = PaperIO.getSellerID()
        apiAuthorization.сreateCategoriesIdsBySeller(
            id,
            categories_ids,
            onSuccess = {
                onSuccess(it)
            },
            onError = { errorMessage, errorCode ->

                onError()
            }).callRequest()
    }

    fun getCategoriesIdsBySeller(
        onSuccess: (response: GetCategoriesIdsResponse) -> Unit,
        onError: () -> Unit
    ) {
        if (Paper.book().exist(PaperIO.SELLER_ID)) {
            val id = PaperIO.getSellerID()
            apiAuthorization.getCategoriesIdsBySeller(
                id,
                onSuccess = {
                    onSuccess(it)
                },
                onError = { errorMessage, errorCode ->

                    onError()
                }).callRequest()
        }
    }

    private fun getBase64(
        onSuccess: (response: Base64Response) -> Unit,
        onError: () -> Unit
    ): Single<*> {
        return apiAuthorization.getBase64({
            onSuccess(it)
        }, onError = { _, _ ->
            onError()
        })
    }

    fun getFilterSubfilter(
        onSuccess: (response: FilterSubFilterResponse) -> Unit,
        onError: () -> Unit
    ): Single<*> {
        return apiAuthorization.getFilterSubfilter({
            onSuccess(it)
        }, onError = { errorMessage, errorCode ->
            onError()
        })

    }

    private fun getFilters(
        onSuccess: (response: FilterResponse) -> Unit,
        onError: () -> Unit
    ): Single<*> {
        return apiAuthorization.getFilter({
            onSuccess(it)
        }, onError = { errorMessage, errorCode ->
            onError()
        })
    }

    private fun getProducts(
        // imageView: ImageView,
        onSuccess: (response: ArrayList<ProductItem>) -> Unit,
        onError: () -> Unit
    ): Single<*> {
        return apiAuthorization.getProducts(onSuccess = {

            //   it.loadImages(imageView, context, listProducts)
            onSuccess(it.mapToProductsList())

        }, onError = { errorMessage, errorCode ->

            onError()
        })
    }

    private fun getCategory(
        onSuccess: (response: ArrayList<CategoriesManufactureItem>) -> Unit,
        onError: () -> Unit
    ): Single<*> {
        return apiAuthorization.getCategory({
            onSuccess(it.mapToCategory())
        }, { errorMessage, errorCode ->
            onError()
        })
    }

    private fun getManufacture(
        onSuccess: (response: ArrayList<CategoriesManufactureItem>) -> Unit,
        onError: () -> Unit
    ): Single<*> {
        return apiAuthorization.getManufacture(onSuccess = {
            onSuccess(it.mapToManufactureRecycler())
        }, onError = { errorMessage, errorCode ->

            onError()
        })
    }

    private fun getCategoryZip(response: (data: ArrayList<CategoriesManufactureItem>) -> Unit) {
        return Single.fromCallable { PaperIO.categories }
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<ArrayList<CategoriesManufactureItem>> {
                override fun onSuccess(t: ArrayList<CategoriesManufactureItem>) {
                    response(t)
                }

                override fun onSubscribe(d: Disposable) {
                    subscriptions.add(d)
                }

                override fun onError(e: Throwable) {

                }
            })
    }

    private fun getManufactureZip(
        onManufactureSuccess: (response: ArrayList<CategoriesManufactureItem>) -> Unit
    ) {
        return Single.fromCallable { PaperIO.manufactures }
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                (object : SingleObserver<ArrayList<CategoriesManufactureItem>> {
                    override fun onSuccess(t: ArrayList<CategoriesManufactureItem>) {

                        onManufactureSuccess(t)

                    }

                    override fun onSubscribe(d: Disposable) {
                        subscriptions.add(d)
                    }

                    override fun onError(e: Throwable) {}
                })
            )
    }


    fun getDataFromDB(
        onCategorySuccess: (responseCategory: ArrayList<CategoriesManufactureItem>) -> Unit,
        onManufactureSuccess: (response: ArrayList<CategoriesManufactureItem>) -> Unit,
        onProductSuccess: (response: ArrayList<ProductItem>) -> Unit
    ) {
        getCategoryZip {
            onCategorySuccess(it)
        }

        getManufactureZip {
            onManufactureSuccess(it)
        }

        getFiftyProductZip {
            onProductSuccess(it)
        }
    }

    fun getFiftyProductZip(listFifty: (list: ArrayList<ProductItem>) -> Unit) {
        Single.fromCallable { PaperIO.products }
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<ArrayList<ProductItem>> {
                override fun onSuccess(t: ArrayList<ProductItem>) {

                    val list = ArrayList<ProductItem>()

                    if (t.size < 50) {
                        listFifty(arrayListOf())
                    } else {
                        for (i in 0..49) {
                            list.add(
                                t[i]
                            )
                        }

                        listFifty(list)
                    }
                }

                override fun onSubscribe(d: Disposable) {
                    subscriptions.add(d)
                }

                override fun onError(e: Throwable) {}
            })

    }


    fun loadAllImages(
        list: ArrayList<ProductItem>,
        onItemComplete: (productItem: ProductItem) -> Unit,
        onComplete: () -> Unit
    ) {

        /*val listt = list*/

        Observable.fromIterable(list)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe(object : Observer<ProductItem> {
                override fun onComplete() {
                    onComplete()
                }

                override fun onSubscribe(d: Disposable) {
                    subscriptions.add(d)
                }

                override fun onNext(t: ProductItem) {
                    onItemComplete(t)

                }

                override fun onError(e: Throwable) {
                }
            })

    }

    private fun bitmapToFile(bitmap: Bitmap): Uri {
        // Get the context wrapper
        val wrapper = ContextWrapper(context)

        var file = wrapper.getDir("Images", Context.MODE_PRIVATE)
        file = File(file, "${UUID.randomUUID()}.jpg")

        try {
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        // Return the saved bitmap uri
        return Uri.parse(file.absolutePath)
    }

    private fun imageFileToBase64(imageFile: File): String {

        return FileInputStream(imageFile).use { inputStream ->
            ByteArrayOutputStream().use { outputStream ->
                Base64OutputStream(outputStream, DEFAULT_BUFFER_SIZE).use { base64FilterStream ->
                    inputStream.copyTo(base64FilterStream)
                    base64FilterStream.flush()
                    outputStream.toString()
                }
            }
        }
    }
}