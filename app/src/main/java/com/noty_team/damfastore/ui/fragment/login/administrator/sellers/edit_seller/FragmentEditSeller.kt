package com.noty_team.damfastore.ui.fragment.login.administrator.sellers.edit_seller

import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.ui.recycler.item_adapter.SellerItem
import kotlinx.android.synthetic.main.fragment_add_seller.*

class FragmentEditSeller : BaseFragment<EditSellerPresenter>() {

    companion object {
        fun newInstance() = FragmentEditSeller()
    }

    override fun layout() = R.layout.fragment_add_seller


    lateinit var sellerItem: SellerItem

    var callbackEditSeller: (item: SellerItem) -> Unit = {}

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.toggleKeyboard(false)
        presenter = EditSellerPresenter(baseContext)

        initSeller()

        initView()
        initAction()
    }

    private fun initView() {
        submit_btn.text = getString(R.string.edit)
    }

    private fun initSeller() {
        name_surname_seller.setText(sellerItem.customersSellerName)
        company_name_seller.setText(sellerItem.customersSellerCompany)
        phone_seller.setText(sellerItem.customersSellerPhone)
        email_seller.setText(sellerItem.customersSellerEmail)
        password_seller.setText(sellerItem.customersSellerPassword)
    }

    private fun initAction() {
        create_seller_btn.getClick {

            presenter?.editSeller(
                SellerItem(
                    name_surname_seller.text.toString(),
                    sellerItem.customersSellerStatus,
                    password_seller.text.toString(),
                    sellerItem.customersSellerId,
                    company_name_seller.text.toString(),
                    email_seller.text.toString(),
                    phone_seller.text.toString()
                )
            ) {
                callbackEditSeller(it)
                baseActivity.onBackPressed()
            }

            /* presenter?.createNewSeller(CreateNewSellerRequest(
                 name = name_surname_seller.text.toString(),
                 company = company_name_seller.text.toString(),
                 phone = phone_seller.text.toString(),
                 email = email_seller.text.toString(),
                 password = password_seller.text.toString()
             ), isSuccess = { isCreated, sellerItem ->
                 if (isCreated) {
                     callbackAddSeller(sellerItem)
                     baseActivity.onBackPressed()

                 } else {
                     showShortToast("user isn't created")
                 }
             })*/
        }
    }
}