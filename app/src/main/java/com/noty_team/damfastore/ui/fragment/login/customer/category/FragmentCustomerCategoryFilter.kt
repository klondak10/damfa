package com.noty_team.damfastore.ui.fragment.login.customer.category

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.ui.fragment.login.seller.product_management.product_filters.subfilter.FragmentSubFilterProduct
import com.noty_team.damfastore.ui.recycler.adapters.CategoryCustomerAdapter
import com.noty_team.damfastore.ui.recycler.adapters.FilterCustomerAdapte
import com.noty_team.damfastore.ui.recycler.adapters.FilterCustomerAdapter1
import com.noty_team.damfastore.ui.recycler.item_adapter.CategoriesManufactureItem
import com.noty_team.damfastore.utils.paper.PaperIO
import com.noty_team.damfastore.utils.paper.PaperIO.Companion.categoriesFilter
import com.noty_team.damfastore.utils.paper.PaperIO.Companion.manufactureFilter
import com.noty_team.damfastore.utils.paper.PaperIO.Companion.setListCategoryFiter
import com.noty_team.damfastore.utils.paper.PaperIO.Companion.setListManufactureFiter
import kotlinx.android.synthetic.main.fragment_customer_category.*
import kotlinx.android.synthetic.main.fragment_customer_category_filter.*
import kotlinx.android.synthetic.main.item_customer_category_filter.*

class FragmentCustomerCategoryFilter : BaseFragment<CustomerCategoryPresenter>() {
    override fun layout(): Int = R.layout.fragment_customer_category_filter
    private lateinit var categoryCustomerAdapter: FilterCustomerAdapte
    private lateinit var categoryCustomerAdapter1: FilterCustomerAdapter1
    var manufactureList = manufactureFilter
    var categoriesList =  categoriesFilter
    override fun initialization(view: View, isFirstInit: Boolean) {

        presenter = CustomerCategoryPresenter(baseContext)
        isShowLoadingDialog(true)

        presenter?.getCategory {
             initRecyclerCategory(it)
        }

        presenter?.getManufacture{
            initRecycler(it)
        }
        clear.setOnClickListener {
            PaperIO.clearListCategoryFilter()
            PaperIO.clearListManufactureFilter()
            replaceFragment(FragmentCustomerCategory.newInstance())
        }


        back_filter.getClick {
            replaceFragment(FragmentCustomerCategory.newInstance())
        }

        tvOk.getClick {
            replaceFragment(FragmentCustomerCategory.newInstance())
        }
    }
    private fun initRecycler(arrayList: ArrayList<CategoriesManufactureItem>) {

        manufacturer_recycler?.layoutManager = LinearLayoutManager(baseContext)

        categoryCustomerAdapter1 = FilterCustomerAdapter1(
            arrayList,
            onChecked = { categoryItem, isChecked ->

                if (isChecked) {
                    manufactureList.add(categoryItem)
                    setListManufactureFiter(manufactureList)
                } else {
                    manufactureList.remove(categoryItem)
                    setListManufactureFiter(manufactureList)
                }
            }, onAllDataLoad = {
                isShowLoadingDialog(false)
            })

        manufacturer_recycler?.adapter = categoryCustomerAdapter1
        isShowLoadingDialog(false)
    }

    private fun initRecyclerCategory(arrayList: ArrayList<CategoriesManufactureItem>) {

        categories_recycler?.layoutManager = LinearLayoutManager(baseContext)

        categoryCustomerAdapter = FilterCustomerAdapte(
            arrayList, onChecked = { categoryItem, isChecked ->

                if (isChecked) {
                    categoriesList.add(categoryItem)
                    setListCategoryFiter(categoriesList)
                } else {
                    categoriesList.remove(categoryItem)
                    setListCategoryFiter(categoriesList)
                }
            }, onAllDataLoad = {
                isShowLoadingDialog(false)
            })

        categories_recycler?.adapter = categoryCustomerAdapter
        isShowLoadingDialog(false)
    }
    override fun onBackPressed(): Boolean {
        return true
    }
}