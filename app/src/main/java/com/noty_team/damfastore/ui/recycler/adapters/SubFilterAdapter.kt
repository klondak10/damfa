package com.noty_team.damfastore.ui.recycler.adapters

import android.text.TextUtils
import android.util.Log
import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.utils.base_adapters.BaseAdapter
import com.noty_team.damfastore.utils.paper.filters.subfilter.SubFilter
import kotlinx.android.synthetic.main.item_characteristic.*


class SubFilterAdapter(
    list: ArrayList<SubFilter>,
    val onClickSelect: () -> Unit = {},
    val onAllDataLoad: () -> Unit = {}
) :
    BaseAdapter<SubFilter, SubFilterAdapter.ViewHolder>(list) {

    var isNeedUpdateBDFilter = false

    override fun getItemViewType(position: Int) = R.layout.item_characteristic

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            itemView.setOnClickListener {
                // onClick()
            }
            switch_characteristic.setOnCheckedChangeListener { _, isChecked ->
                list[position].isSelected = isChecked
                 isNeedUpdateBDFilter = true
            }


          }

        override fun bind(pos: Int) {
            val item = list[position]

            name_characteristic.text = item.subfilterName

            if (position == list.size - 1) {
                onAllDataLoad()
                //getBaseCount()
            }

            switch_characteristic.isChecked = item.isSelected
        }
    }

    fun List(): ArrayList<SubFilter> {
        return list
    }

    fun getBaseCount(): Int {

        var counter = 0

        for (i in 0 until list.size - 1) {
            if (list[i].isSelected) {
                ++counter
            }
        }

        return counter

    }

    fun filter(sequence: String?,listNew:ArrayList<SubFilter>) {


        val temp: ArrayList<SubFilter> = ArrayList()
        if (!TextUtils.isEmpty(sequence)) {
            for (i in 0 until listNew.size) {

                if(listNew[i].subfilterName.toLowerCase().contains(sequence!!.toLowerCase())) {
                    temp.add(listNew[i])
                }
            }
        }else{
                       temp.addAll(listNew)
         }
        list.clear()
        list.addAll(temp)
        notifyDataSetChanged()
        temp.clear()
    }
/*
    fun getSelectedCount(): Int {
        return baseCounter
    }*/
}