package com.noty_team.damfastore.ui.fragment.login.customer.category

import android.content.Context
import com.noty_team.damfastore.base.fragment.BasePresenter
import com.noty_team.damfastore.ui.recycler.item_adapter.CategoriesManufactureItem
import com.noty_team.damfastore.utils.paper.PaperIO
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class CustomerCategoryPresenter(context: Context) : BasePresenter(context) {

    fun getCategory(onCategorySuccess: (response: ArrayList<CategoriesManufactureItem>) -> Unit) {

        PaperIO.getSelectedCategory { listCategory ->

                if (listCategory.isNullOrEmpty())
                    onCategorySuccess(PaperIO.categories)
                else
                    onCategorySuccess(listCategory)

        }
    }

    fun getManufacture(onCategorySuccess: (response: ArrayList<CategoriesManufactureItem>) -> Unit) {

        PaperIO.getSelectedManufacture { listCategory ->
                  onCategorySuccess(listCategory)
        }
    }
}