package com.noty_team.damfastore.ui.recycler.adapters

import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.ui.recycler.item_adapter.EmptyItem
import com.noty_team.damfastore.utils.base_adapters.BaseAdapter

class SubcategoryAdapter(
    list: ArrayList<EmptyItem>,
    val onClick: () -> Unit = {}
) :
    BaseAdapter<EmptyItem, SubcategoryAdapter.ViewHolder>(list) {

    override fun getItemViewType(position: Int) = R.layout.item_subcategory

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            itemView.setOnClickListener {
                onClick()
            }
        }

        override fun bind(pos: Int) {
            val item = list[pos]

        }

    }

}