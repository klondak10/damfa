package com.noty_team.damfastore.ui.fragment.login.customer.product_detail

import android.text.Html
import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.ui.fragment.login.customer.shopping_cart.FragmentShoppingCard
import com.noty_team.damfastore.ui.recycler.item_adapter.ProductRecyclerItem
import com.noty_team.damfastore.utils.paper.PaperIO
import com.noty_team.damfastore.utils.paper.products.ProductItem
import kotlinx.android.synthetic.main.fragment_product_detail.*
import kotlinx.android.synthetic.main.fragment_product_detail.txt_count
import kotlinx.android.synthetic.main.products_content.*
import noty_team.com.urger.utils.setPhotoProductsDetail


class ProductDetailFragment : BaseFragment<ProductDetailPresenter>() {

    companion object {
        fun newInstance() = ProductDetailFragment()
        var isItemSelected = false
    }

    var callbackSelected: (item: ProductRecyclerItem) -> Unit = {}

    var productID = ""

    lateinit var productItem: ProductItem

    override fun layout() = R.layout.fragment_product_detail

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.toggleKeyboard(false)

        if (isFirstInit) {
            intiView()
            initSelectedButton(false)
            initAction()
        } else {
            initSelectedButton(false)
        }


    }

    private fun intiView() {
        if (productID.isNotEmpty())
            PaperIO.getProductById(productID) {
                productItem = it

                if (!it.image.isNullOrEmpty()) {
                    initImageProduct(it.image)
                } else {
                    initImageProduct(it.productsImage)
                }

                product_detail_name.text = it.productsAppName

                val detail = it.productsDescription/**/
                val dfg = Html.fromHtml(detail).toString()

                val f = dfg.replace("<br>", "\n")

                product_detail_description.setText(f)

                isShowLoadingDialog(false)

            }
    }


    override fun onResume() {
        super.onResume()
        initAction()
    }

    private fun initImageProduct(
        productsImage: String?
    ) {
        if (!productsImage.isNullOrEmpty()) {

            setPhotoProductsDetail(product_detail_image, productsImage)
        } else {
            product_detail_image.setImageResource(R.mipmap.test_logo2)
        }
    }

    private fun initAction() {

        if (PaperIO.recycle.size > 0) {
            txt_count.visibility = View.VISIBLE
            txt_count.text = PaperIO.recycle.size.toString()
        }else{
            txt_count.visibility = View.GONE
        }

        back_product_detail.getClick {
            baseActivity.onBackPressed()
        }

        shopping_card_product_detail.getClick {
            logicISelected()
            addFragment(FragmentShoppingCard.newInstance())
        }

        product_detail_select.getClick(600) {
            initSelectedButton(true)
        }
    }

    override fun onDetach() {
        logicISelected()
        isItemSelected = false
        super.onDetach()
    }

    private fun logicISelected() {

        if (isItemSelected) {

            saveToRecycle()

        } else {
            deleteFromRecycle()
        }
    }


    private fun initSelectedButton(isSelectToRecycle: Boolean) {
        if (isSelectToRecycle)
            isItemSelected = !isItemSelected

        if (isItemSelected) {
            product_detail_select.setImageResource(R.mipmap.ic_select)
            if (PaperIO.recycle.size > 0) {
                txt_count.visibility = View.VISIBLE
                txt_count.text = PaperIO.recycle.size.toString()
            }

        } else {
            product_detail_select.setImageResource(R.mipmap.ic_unselect)

            if ((PaperIO.recycle.size-1)== 0) {
                txt_count.visibility = View.GONE

            }else{
                txt_count.visibility = View.VISIBLE
                txt_count.text = (PaperIO.recycle.size-1).toString()
            }
        }
    }

    private fun saveToRecycle() {

        val listEnded = HashSet<ProductRecyclerItem>(PaperIO.recycle)


        listEnded.add(
            ProductRecyclerItem(
                id = productItem.productsId ?: "",
                productName = productItem.productsAppName ?: "",
                productImage = productItem.productsImage ?: "",
                productImageStore = productItem.image,
                isSelected = true

            )
        )

        callbackSelected(
            ProductRecyclerItem(
                id = productItem.productsId ?: "",
                productName = productItem.productsAppName ?: "",
                productImage = productItem.productsImage ?: "",
                productImageStore = productItem.image,
                isSelected = true

            )
        )

        PaperIO.recycle = ArrayList(listEnded.toList())
    }

    private fun deleteFromRecycle() {
        val listEnded = ArrayList(PaperIO.recycle)

        listEnded.remove(
            ProductRecyclerItem(
                id = productItem.productsId ?: "",
                productName = productItem.productsAppName ?: "",
                productImage = productItem.productsImage ?: "",
                productImageStore = productItem.image,
                isSelected = true

            )
        )

        callbackSelected(
            ProductRecyclerItem(
                id = productItem.productsId ?: "",
                productName = productItem.productsAppName ?: "",
                productImage = productItem.productsImage ?: "",
                productImageStore = productItem.image,
                isSelected = false

            )
        )

        PaperIO.recycle = ArrayList(listEnded.toList())
    }
}