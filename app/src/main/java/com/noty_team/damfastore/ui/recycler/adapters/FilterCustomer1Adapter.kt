package com.noty_team.damfastore.ui.recycler.adapters

import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.ui.recycler.item_adapter.CategoriesManufactureItem
import com.noty_team.damfastore.utils.base_adapters.BaseAdapter
import com.noty_team.damfastore.utils.paper.PaperIO
import kotlinx.android.synthetic.main.item_customer_category_filter.*

class FilterCustomerAdapter1(
    list: ArrayList<CategoriesManufactureItem>,
    val onChecked: (categoryItem: CategoriesManufactureItem, isChecked: Boolean) -> Unit,
    val onAllDataLoad: () -> Unit = {}
) :
    BaseAdapter<CategoriesManufactureItem, FilterCustomerAdapter1.ViewHolder>(list) {

    override fun getItemViewType(position: Int) = R.layout.item_customer_category_filter

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {

            checkBox.setOnClickListener {
                onChecked(list[position], checkBox.isChecked)
            }

        }

        override fun bind(pos: Int) {

            val item = list[position]

            name_item.text = item.manufactureName
            for (i in PaperIO.manufactureFilter) {
                if (item.manufactureName == i.manufactureName) {
                    checkBox.isChecked = true
                }
            }
            if (position == list.size - 1) {
                onAllDataLoad()
            }
        }
    }


    fun List(): ArrayList<CategoriesManufactureItem> {
        return list
    }
}