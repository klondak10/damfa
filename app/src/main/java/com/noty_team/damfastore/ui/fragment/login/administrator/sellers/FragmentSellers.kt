package com.noty_team.damfastore.ui.fragment.login.administrator.sellers

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.design.widget.BottomSheetBehavior
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.ViewGroup
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.ui.activity.choose_role.ChooseRoleActivity
import com.noty_team.damfastore.ui.fragment.login.administrator.sellers.add_seller.FragmentAddSeller
import com.noty_team.damfastore.ui.fragment.login.administrator.sellers.edit_seller.FragmentEditSeller
import com.noty_team.damfastore.ui.recycler.adapters.SellersAdapter
import com.noty_team.damfastore.ui.recycler.adapters.SellersAdapterPagination
import com.noty_team.damfastore.ui.recycler.item_adapter.SellerItem
import com.noty_team.damfastore.utils.Constants
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_sellers.*
import noty_team.com.urger.utils.waitUntilViewDrawn


class FragmentSellers : BaseFragment<SellersPresenter>() {

    companion object {
        fun newInstance() = FragmentSellers()
    }

    private lateinit var mBottomSheetBehavior: BottomSheetBehavior<*>

    var onAddSeller: Observable<Unit>? = null

    var onEditSeller: Observable<Unit>? = null

    private lateinit var sellersAdapterPagination: SellersAdapter

    var isCallbackAddSeller = false

    var isCallbackEditSeller = false

    private lateinit var listSellers: ArrayList<SellerItem>

    override fun layout() = R.layout.fragment_sellers

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.toggleKeyboard(false)
        mBottomSheetBehavior = BottomSheetBehavior.from(bottom_edit_seller)

        presenter = SellersPresenter(baseContext)

        if (isFirstInit) {
            getSellers()
        }

        initAction()
    }

    private fun initAction() {
        add_seller.getClick {
            addFragment(FragmentAddSeller.newInstance().apply {
                callbackAddSeller = this@FragmentSellers.callbackAddSeller
            })
        }

        smoke_layout.getClick {
            clearSmoke(smoke_layout)
        }

        log_out_sell.getClick {
            ChooseRoleActivity.start(baseActivity)
        }

    }

    private fun initRecycler(list: ArrayList<SellerItem>) {

        /*waitUntilViewDrawn(recycler_sellers) {*/
        recycler_sellers.layoutManager = LinearLayoutManager(baseActivity)

        listSellers = list

        sellersAdapterPagination = SellersAdapter(listSellers,
            // recycler_sellers,
            /*  onLoadData = {
                  getSellers(it)
              },*/
            onClick = {
                clearSmoke(smoke_layout)
            },
            onClickDot = { seller ->

                log_out_sell.getClick {
                    //ChooseRoleActivity.start(baseActivity)
                    clearSmoke(smoke_layout)
                }

                var sellerStatus = seller.customersSellerStatus ?: false

                if (sellerStatus) {
                    image_status_seller.setImageDrawable(resources.getDrawable(R.mipmap.ic_deactivate))
                    text_status_seller.text = resources.getString(R.string.deactivate)

                } else {
                    image_status_seller.setImageDrawable(resources.getDrawable(R.mipmap.ic_activated))
                    text_status_seller.text = resources.getString(R.string.activate)

                }

                mBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                applySmoke(smoke_layout, 0.5f)
                initBottomSheetClick(sellerStatus,
                    statusAction = {
                        sellerStatus = !sellerStatus
                        sellersAdapterPagination.editItem(seller)
                        clearSmoke(smoke_layout)

                    }, edit = {
                        clearSmoke(smoke_layout)
                        addFragment(FragmentEditSeller.newInstance().apply {
                            sellerItem = seller
                            callbackEditSeller = this@FragmentSellers.callbackEditSeller
                        })

                    }, delete = {

                        val sellerId = !seller.customersSellerId.isNullOrEmpty()
                        clearSmoke(smoke_layout)
                        if (sellerId) {
                            presenter?.deleteSeller(seller.customersSellerId ?: "-1") {

                                if (it) {
                                    sellersAdapterPagination.remove(seller)
                                    showShortToast("deleted")
                                } else {
                                    showShortToast("deleted failed")
                                }

                            }
                        } else {
                            var b = 5
                        }

                    })
            })

        recycler_sellers.adapter = sellersAdapterPagination
        /* }*/

    }

    var callbackAddSeller: (item: SellerItem) -> Unit = {
        isCallbackAddSeller = true
        onAddSeller = Observable.fromCallable { addNewSeller(it) }
    }


    var callbackEditSeller: (item: SellerItem) -> Unit = {
        isCallbackEditSeller = true
        onEditSeller = Observable.fromCallable { editSeller(it) }

    }

    private fun addNewSeller(item: SellerItem) {
        sellersAdapterPagination.add(item)
    }

    private fun editSeller(item: SellerItem) {
        sellersAdapterPagination.editItem(item)
    }


    override fun onResume() {
        super.onResume()
        if (isCallbackAddSeller) {
            isCallbackAddSeller = false
            onAddSeller?.subscribe()
        }

        if (isCallbackEditSeller) {
            isCallbackEditSeller = false
            onEditSeller?.subscribe()
        }
    }


    private fun getSellers() {
        presenter?.getSellersPagination(0) { response ->

            initRecycler(response)

            // sellersAdapterPagination.onNewDataReceived(response, Constants.SellerLimit)
        }
    }


    private fun initBottomSheetClick(
        status: Boolean,
        statusAction: () -> Unit,
        edit: () -> Unit,
        delete: () -> Unit
    ) {
        btm_active_deactivated.getClick {
            if (!status) {
                showShortToast("activated")
            } else {
                showShortToast("deactivated")
            }
            clearSmoke(smoke_layout)
            statusAction()

        }

        btm_edit.getClick {

            edit()
            // showShortToast("edit")
        }

        btm_delete.getClick {

            delete()

        }

    }

    fun applySmoke(parent: ViewGroup, dimAmount: Float) {
        val overlay = parent.overlay
        overlay.clear()

        val dim = ColorDrawable(Color.BLACK)
        dim.setBounds(0, 0, parent.width, parent.height)
        dim.alpha = (255 * dimAmount).toInt()

        overlay.add(dim)
    }

    fun clearSmoke(parent: ViewGroup) {
        val overlay = parent.overlay
        overlay.clear()
        mBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

        log_out_sell.getClick {
            ChooseRoleActivity.start(baseActivity)
        }
    }
}