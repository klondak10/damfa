package com.noty_team.damfastore.ui.fragment.login.seller

import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.api.request.login_seller.LoginSellerRequest
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.ui.fragment.login.seller.product_management.FragmentProductManagement
import com.noty_team.damfastore.utils.Constants
import com.noty_team.damfastore.utils.paper.PaperIO
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_login_seller.*
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket

class FragmentLoginSeller : BaseFragment<LoginSellerPresenter>() {

    companion object {
        fun newInstance() = FragmentLoginSeller()
    }

    override fun layout() = R.layout.fragment_login_seller

    override fun initialization(view: View, isFirstInit: Boolean) {

        baseActivity.toggleKeyboard(false)
        isShowLoadingDialog(false)

        presenter = LoginSellerPresenter(baseContext)

        login_btn_seller.getClick {
            hasInternetConnection().subscribe { hasInternet ->
                if (hasInternet) {
                    presenter?.loginSeller(
                        LoginSellerRequest(
                            seller_login_email.text.toString(),
                            seller_login_password.text.toString()
                        )
                    ) {
                        if (it) {
                            PaperIO.USER_LOGIN = seller_login_email.text.toString()
                            PaperIO.USER_PASSWORD = seller_login_password.text.toString()
                            replaceFragment(FragmentProductManagement.newInstance())
                        } else {
                            showShortToast(getString(R.string.login_or_password_incorrect))
                        }
                    }
                } else {

                    if (!PaperIO.USER_LOGIN.isNullOrEmpty() && !PaperIO.USER_PASSWORD.isNullOrEmpty()) {

                        if (PaperIO.USER_LOGIN == seller_login_email.text.toString() &&
                            PaperIO.USER_PASSWORD == seller_login_password.text.toString()){
                            replaceFragment(FragmentProductManagement.newInstance())
                        }else{
                            showShortToast(getString(R.string.login_or_password_incorrect))
                        }


                    } else {
                        showShortToast(resources.getString(R.string.you_should_check_your_internet_connection_and_try_again))
                    }
                }
            }
        }
    }

    private fun hasInternetConnection(): Single<Boolean> {
        return Single.fromCallable {
            try {
                // Connect to Google DNS to check for connection
                val timeoutMs = 1500
                val socket = Socket()
                val socketAddress = InetSocketAddress("8.8.8.8", 53)

                socket.connect(socketAddress, timeoutMs)
                socket.close()

                true
            } catch (e: IOException) {
                false
            }
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}