package com.noty_team.damfastore.ui.recycler.adapters

import android.support.v7.widget.RecyclerView
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.api.response.sellers_pagination.MessageItem
import com.noty_team.damfastore.ui.recycler.item_adapter.SellerItem
import kotlinx.android.synthetic.main.item_seller.*
import noty_team.com.urger.utils.adapters.BaseAdapterPagination

class SellersAdapterPagination(
    list: ArrayList<SellerItem>,
    recyclerView: RecyclerView,
    var onLoadData: (offset: Int) -> Unit = {},
    var onClick: (data: SellerItem) -> Unit = {},
    val onClickDot: (sellerItem: SellerItem) -> Unit = {}
    //var onLongClick: (documentItem: SellerItem) -> Unit = {}
) : BaseAdapterPagination<SellerItem>(list, recyclerView) {

    override val itemLayoutResourceId = R.layout.item_seller


    override fun onInit(pos: Int, viewHolder: MainViewHolder) {
        viewHolder.apply {
            try {
                itemView.setOnClickListener {
                    onClick(list[position])
                }

                itemView.setOnLongClickListener {

                    // onLongClick(list[position])

                    return@setOnLongClickListener true
                }

                three_dot.setOnClickListener {
                    onClickDot(list[position])
                }

            } catch (e: Exception) {
            }
        }
    }

    override fun onBind(pos: Int, viewHolder: MainViewHolder) {
        viewHolder.apply {

            val item = list[pos]

            val status = item.customersSellerStatus ?: false

            if (status)
                activated_indicator.background =
                    activated_indicator.context.getDrawable(R.drawable.indicator_green)
            else
                activated_indicator.background =
                    activated_indicator.context.getDrawable(R.drawable.indicator_red)

            name_surname.text = item.customersSellerName
            seller_email.text = item.customersSellerEmail
            seller_phone.text = item.customersSellerPhone
            seller_password.text = "*********"
            seller_company.text = item.customersSellerCompany


        }
    }

    fun editItem(data: SellerItem) {
        list.forEachIndexed { index, item ->
            if (item.customersSellerId == data.customersSellerId) {
                list[index] = data
                notifyItemChanged(index)
                return@forEachIndexed
            }
        }
    }

    override fun onDataLoaded(listSize: Int) {
        onLoadData(listSize)
    }
}
