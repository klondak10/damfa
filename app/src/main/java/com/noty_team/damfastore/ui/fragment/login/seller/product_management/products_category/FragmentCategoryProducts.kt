package com.noty_team.damfastore.ui.fragment.login.seller.product_management.products_category

import android.os.Bundle
import android.support.v4.os.ConfigurationCompat
import android.support.v7.widget.GridLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.ui.fragment.login.seller.product_management.FragmentProductManagement
import com.noty_team.damfastore.ui.recycler.adapters.ProductAdapter
import com.noty_team.damfastore.ui.recycler.item_adapter.ProductRecyclerItem
import com.noty_team.damfastore.utils.paper.PaperIO
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_category_products.*

class FragmentCategoryProducts : BaseFragment<CategoryProductPresenter>() {

    enum class AdapterItemType {
        CATEGORY, MANUFACTURE, DEFOULT
    }

    companion object {
        fun newInstance() = FragmentCategoryProducts()
    }

    private lateinit var productAdapter: ProductAdapter

    var listProductsId = listOf<String>()

    private lateinit var _listProduct: ArrayList<ProductRecyclerItem>

    var nameCategory = ""

    var adapterItemType = AdapterItemType.DEFOULT

    var categoryManufactureID = ""


    var callbackSelectedButton: (isNeedSelected: Boolean, categoryManufactureID: String, tepe: AdapterItemType) -> Unit =
        { _, _, _ -> }

    override fun layout() = R.layout.fragment_category_products

    private var currentPosChooseDialog = 0

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.toggleKeyboard(false)

        presenter = CategoryProductPresenter(baseContext)

        name_category_product.text = nameCategory

        initProductRecycler()
        initAction()
    }

    override fun onDetach() {
        FragmentProductManagement.isBack = true
        callbackCheckBox()
        savaToDB {

        }
        super.onDetach()
    }

    private fun savaToDB(onSuccess: () -> Unit) {
        PaperIO.setListProducts(productAdapter.List()) {
            onSuccess()
        }

    }
    private fun getItems(): java.util.ArrayList<String>? {
        return (resources.getStringArray(R.array.name_sort)).toCollection(java.util.ArrayList())
    }


    var callbackChooseLanguage: (pos: Int) -> Unit = {
        if(currentPosChooseDialog != it) {
            currentPosChooseDialog = it

            if (it==0){
                productAdapter.sortList(true)
            }else{
                productAdapter.sortList(false)
            }
        }

    }

    fun callbackCheckBox() {
        if (productAdapter.getCounter() > 0)
            callbackSelectedButton(true, categoryManufactureID, adapterItemType)
        else {
            callbackSelectedButton(false, categoryManufactureID, adapterItemType)
        }
    }

    private fun initAction() {
        back_category_product.getClick {
            callbackCheckBox()

            onBackPressed()
        }
        sortProduct.getClick {

            val manager = fragmentManager
            val dialog = DialogChooseSort().apply {
                callbackChooseLanguage = this@FragmentCategoryProducts.callbackChooseLanguage
                position = this@FragmentCategoryProducts.currentPosChooseDialog
            }

            val bundle = Bundle().apply {
                putStringArrayList(dialog.DATA, getItems())     // Require ArrayList
                putInt(dialog.SELECTED, this@FragmentCategoryProducts.currentPosChooseDialog)
            }
            dialog.arguments = bundle
            dialog.show(manager!!, "Dialog")

        }
        categories_product_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s.toString().length > 2) {

                    if (this@FragmentCategoryProducts::_listProduct.isInitialized)
                        searchProducts(s.toString())


                } else if (s.toString().isEmpty()) {
                    if (this@FragmentCategoryProducts::_listProduct.isInitialized)
                       //initAdapterData(_listProduct)
                        initProductRecycler()
                }
            }
        })

    }

    private fun initProductRecycler() {
        all_product_category_recycler.layoutManager = GridLayoutManager(baseContext, 3)

        if (listProductsId.isNotEmpty()) {
            getAllFromDBAsynk()

        } else {
            initAdapterData(arrayListOf())
        }
    }

    private fun getAllFromDBAsynk() {
        isShowLoadingDialog(true)

        PaperIO.getProductsById(listProductsId) {


            val listRecycler = it.flatMap { productItem ->
                arrayListOf(
                    ProductRecyclerItem(
                        id = productItem.productsId ?: "",
                        isSelected = productItem.isSelected,
                        productImage = productItem.productsImage ?: "",
                        productName = productItem.productsAppName ?: "",
                        productImageStore = productItem.image
                    )
                )
            } as ArrayList<ProductRecyclerItem>

            PaperIO.fragmentCategoryProduct = listRecycler

            _listProduct = listRecycler

            initAdapterData(listRecycler)

            isShowLoadingDialog(false)
        }

    }

    private fun searchProducts(name: String) {
        // isShowLoadingDialog(true)

        PaperIO.searchProductByNameSync(PaperIO.fragmentCategoryProduct, name,
            onAllDataFind = {

                val t = _listProduct
                productAdapter.clear()

                _listProduct = t
                productAdapter.addAll(it)
                // isShowLoadingDialog(false)
            }, onError = {
                isShowLoadingDialog(false)
            })
    }

    private fun initAdapterData(list: ArrayList<ProductRecyclerItem>) {
        productAdapter = ProductAdapter(list)
        all_product_category_recycler.adapter = productAdapter
    }
}