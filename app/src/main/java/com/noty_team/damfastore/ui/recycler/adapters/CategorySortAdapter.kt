package com.noty_team.damfastore.ui.recycler.adapters

import android.util.Log
import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.ui.recycler.item_adapter.CategoriesManufactureItem
import com.noty_team.damfastore.utils.base_adapters.BaseAdapter
import com.noty_team.damfastore.utils.paper.PaperIO
import kotlinx.android.synthetic.main.item_category_manufacture.*
import kotlinx.android.synthetic.main.item_category_manufacture.logo_item
import kotlinx.android.synthetic.main.item_category_manufacture.name_item
import kotlinx.android.synthetic.main.item_customer_category_filter.*

class CategorySortAdapter(
    list: ArrayList<CategoriesManufactureItem>,
    val itemSetting: ItemSettings,
    val onClickSelect: (isSelected: Boolean, item: CategoriesManufactureItem) -> Unit = { _, _ -> }
) :
    BaseAdapter<CategoriesManufactureItem, CategorySortAdapter.ViewHolder>(list) {


    override fun getItemViewType(position: Int) = R.layout.item_customer_category_filter

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            checkBox.setOnClickListener {
                val item = list[position]
                onClickSelect(checkBox.isChecked, item)
            }
        }

        override fun bind(pos: Int) {
            val item = list[position]

            if (itemSetting == ItemSettings.CATEGORY){

                logo_item.setImageResource(R.mipmap.ic_category)
                for (i in PaperIO.categoriesFilter) {
                    if (item.manufactureName == i.manufactureName) {
                        checkBox.isChecked = true
//                        onClickSelect(checkBox.isChecked, item)
                    }
                }
            }
            else if (itemSetting == ItemSettings.MANUFACTURE){

                logo_item.setImageResource(R.mipmap.ic_manufacture)
                for (i in PaperIO.manufactureFilter) {
                    if (item.manufactureName == i.manufactureName) {
                        checkBox.isChecked = true
//                        onClickSelect(checkBox.isChecked, item)
                    }
                }
            }




            name_item.text = item.manufactureName
        }

        fun isSelected(isSelected: Boolean, item: CategoriesManufactureItem) {
            if (isSelected) {
                selected_button.setImageResource(R.mipmap.ic_select)
            } else {
                onClickSelect(false, item)
                selected_button.setImageResource(R.mipmap.ic_unselect)
            }
        }

    }

    fun getCounter(): Int {
        var counterSelected = 0
        list.forEach {
            if (it.isSelected)
                ++counterSelected
        }

        return counterSelected
    }

    fun editItem(data: CategoriesManufactureItem) {
        list.forEachIndexed { index, item ->
            if (item.id == data.id) {
                list[index] = data
                notifyItemChanged(index)
                return@forEachIndexed
            }
        }
    }

    fun changeSelectedUIById(categoryId: String, isSelected: Boolean) {
        for (i in list.indices) {
            if (list[i].id == categoryId) {
                list[i].isSelected = isSelected
                notifyItemChanged(i)
                break
            }
        }
    }

    fun resetSelectedButton() {
        list.forEach {
            it.isSelected = false
            notifyDataSetChanged()
        }
    }

    enum class ItemSettings {
        CATEGORY, MANUFACTURE
    }

    fun List(): ArrayList<CategoriesManufactureItem> {
        return list
    }
}