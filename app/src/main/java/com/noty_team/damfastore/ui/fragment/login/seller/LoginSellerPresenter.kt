package com.noty_team.damfastore.ui.fragment.login.seller

import android.content.Context
import android.util.Log
import com.noty_team.damfastore.base.api.request.login_seller.LoginSellerRequest
import com.noty_team.damfastore.base.fragment.BasePresenter
import com.noty_team.damfastore.utils.paper.PaperIO

class LoginSellerPresenter(context: Context) : BasePresenter(context) {

    fun loginSeller(
        loginSellerRequest: LoginSellerRequest,
        onSuccess: (isRegister: Boolean) -> Unit
    ) {

        apiAuthorization.loginSeller(loginSellerRequest, {

            if (it.success == true) {
                onSuccess(true)
                PaperIO.setSellerID(it.message?.customers_seller_id?.toInt()!!)
            } else {
                onSuccess(false)
            }

        }, { errorMessage, errorCode ->
            showShortToast("error")
        }).callRequest()


    }


}