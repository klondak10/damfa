package com.noty_team.damfastore.ui.fragment.login.customer.products

import android.content.Context
import com.noty_team.damfastore.base.fragment.BasePresenter
import com.noty_team.damfastore.ui.recycler.item_adapter.CategoriesManufactureItem
import com.noty_team.damfastore.utils.paper.PaperIO
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class CustomerProductsPresenter(context: Context): BasePresenter(context) {

    private fun getCategoryFromDB(): Single<ArrayList<CategoriesManufactureItem>> {
        return Single.just(PaperIO.categories)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}