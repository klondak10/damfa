package com.noty_team.damfastore.ui.fragment.login.administrator.sellers

import android.content.Context
import com.noty_team.damfastore.base.fragment.BasePresenter
import com.noty_team.damfastore.ui.recycler.item_adapter.SellerItem
import com.noty_team.damfastore.utils.Constants

class SellersPresenter(context: Context) : BasePresenter(context) {

    fun getSellersPagination(
        listSize: Int,
        onSellersSuccess: (sellers: ArrayList<SellerItem>) -> Unit
    ) {
        val page = listSize / Constants.SellerLimit
        apiAuthorization.getSellerPagination(Constants.SellerLimit, page, {

            onSellersSuccess(it.mapResponse())

        }, { errorMessage, errorCode ->

            //showShortToast("error")
        }).callRequest()
    }

    fun deleteSeller(
        sellerId: String,
        isDeleted: (status: Boolean) -> Unit
    ) {

        var selerI = sellerId
        apiAuthorization.deleteSeller(sellerId,
            onSuccess = {

                var b = it
                var c = 12
                if (it.success == true) {
                    isDeleted(true)
                } else {
                    isDeleted(false)
                }


            }, onError = { errorMessage, errorCode ->

                showShortToast("error")
            }).callRequest()
    }
}