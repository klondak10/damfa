package com.noty_team.damfastore.ui.activity.choose_role

import android.content.Intent
import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.activity.BaseActivity
import com.noty_team.damfastore.ui.activity.login.MainActivity
import com.noty_team.damfastore.utils.paper.PaperIO
import kotlinx.android.synthetic.main.activity_choose_role.*

class ChooseRoleActivity : BaseActivity() {

    companion object {
        fun start(baseActivity: BaseActivity) {
            baseActivity.startActivity(
                Intent(
                    baseActivity,
                    ChooseRoleActivity::class.java
                ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            )
        }
    }

    override fun layout() = R.layout.activity_choose_role

    override fun initialization() {

            administrator_btn.getClick {

                MainActivity.start(this, MainActivity.UserType.ADMINISTRATOR)
            }

            seller_btn.getClick {
                MainActivity.start(this, MainActivity.UserType.SELLER)
            }



    }
}
