package com.noty_team.damfastore.ui.fragment.login.administrator.sellers.add_seller

import android.content.Context
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.api.request.create_new_seller.CreateNewSellerRequest
import com.noty_team.damfastore.base.fragment.BasePresenter
import com.noty_team.damfastore.ui.recycler.item_adapter.SellerItem

class AddSellerPresenter(context: Context) : BasePresenter(context) {

    fun createNewSeller(
        createNewSellerRequest: CreateNewSellerRequest,
        isSuccess: (isCreated: Boolean, sellerItem: SellerItem) -> Unit
    ) {
        apiAuthorization.createNewSeller(createNewSellerRequest, {
            val isRegister = it.success ?: false

            if (isRegister) {

                val item = it.mapToSeller()/* SellerItem(
                    customersSellerName = it.message?.customersSellerName ?: "",
                    customersSellerStatus = it.message?.customersSellerStatus ?: false,
                    customersSellerPassword = it.message?.customersSellerPassword ?: "",
                    customersSellerId = it.message?.customersSellerId ?: "",
                    customersSellerCompany = it.message?.customersSellerCompany ?: "",
                    customersSellerEmail = it.message?.customersSellerEmail ?: "",
                    customersSellerPhone = it.message?.customersSellerPhone ?: ""
                )*/

                isSuccess(true, item)
            } else {
                isSuccess(false, SellerItem())
            }

        }, { errorMessage, errorCode ->
            showShortToast("Fehler: Die E-Mail Adresse existiert schon.")

        }).callRequest()
    }
}