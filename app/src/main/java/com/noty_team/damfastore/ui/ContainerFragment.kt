package com.noty_team.damfastore.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.DamfastoreApp
import noty_team.com.urger.utils.cicerone.BackButtonListener
import noty_team.com.urger.utils.cicerone.LocalCiceroneHolder
import noty_team.com.urger.utils.cicerone.RouterProvider
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator

open class ContainerFragment : Fragment(), RouterProvider, BackButtonListener {

    companion object {
        const val EXTRA_NAME = "tcf_extra_name"
    }

    private var navigator: Navigator? = null

    lateinit var ciceroneHolder: LocalCiceroneHolder

    open fun getContainerName(): String {
        return arguments?.getString(EXTRA_NAME) ?: ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ciceroneHolder = DamfastoreApp.localCiceroneHolder
    }

    fun getCicerone(): Cicerone<Router> {
        return ciceroneHolder.getCicerone(getContainerName())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_container, container, false)
    }

    override fun onResume() {
        super.onResume()
        getCicerone().navigatorHolder.setNavigator(getNavigator())
    }

    override fun onPause() {
        getCicerone().navigatorHolder.removeNavigator()
        super.onPause()
    }

    fun getNavigator(): Navigator {
        if (navigator == null) {
            navigator = SupportAppNavigator(activity, childFragmentManager, R.id.ftc_container)
        }
        return navigator as Navigator
    }

    override fun getRouter(): Router {
        return getCicerone().router
    }

    override fun onBackPressed(): Boolean {
        val fragment = childFragmentManager.findFragmentById(R.id.ftc_container)
        if (fragment != null
                && fragment is BackButtonListener
                && (fragment as BackButtonListener).onBackPressed()) {
            return true
        } else {
            (activity as RouterProvider).getRouter().exit()
            return true
        }
    }
}