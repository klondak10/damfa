package com.noty_team.damfastore.ui.recycler.item_adapter

data class FilterItem(
    var isSelected: Boolean = false,
    var filterId: String = "",
    var filterName: String = ""
)