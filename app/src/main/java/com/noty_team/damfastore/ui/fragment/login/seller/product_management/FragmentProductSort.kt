package com.noty_team.damfastore.ui.fragment.login.seller.product_management

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.ui.recycler.adapters.CategorySortAdapter
import com.noty_team.damfastore.utils.paper.PaperIO
import com.noty_team.damfastore.utils.paper.PaperIO.Companion.manufactureFilter
import com.noty_team.damfastore.utils.paper.PaperIO.Companion.setListCategoryFiter
import com.noty_team.damfastore.utils.paper.PaperIO.Companion.setListManufactureFiter
import kotlinx.android.synthetic.main.fragment_product_sort.*

class FragmentProductSort : BaseFragment<ProductManagementPresenter>() {
    override fun layout(): Int = R.layout.fragment_product_sort
    lateinit var categorySortAdapter: CategorySortAdapter
    lateinit var manufacturerAdapter: CategorySortAdapter
    var categoriesList = PaperIO.categoriesFilter
    var manufacturerList = manufactureFilter

    override fun initialization(view: View, isFirstInit: Boolean) {
        initRecyclersData()
        tvOk.getClick {
            addFragment(FragmentProductManagement.newInstance())
        }
        back_filter.getClick {
            addFragment(FragmentProductManagement.newInstance())
        }

        clear.setOnClickListener {
            PaperIO.clearListCategoryFilter()
            PaperIO.clearListManufactureFilter()
            addFragment(FragmentProductManagement.newInstance())
        }
    }

    private fun initRecyclersData() {
        categories_recycler.layoutManager = LinearLayoutManager(baseContext)

        categorySortAdapter = CategorySortAdapter(
            PaperIO.categories,
            CategorySortAdapter.ItemSettings.CATEGORY,
            onClickSelect = { isSelected, item ->
                if (isSelected) {
                    categoriesList.add(item)
                    categoriesList
                    setListCategoryFiter(categoriesList)
                } else {
                    categoriesList.remove(item)
                    setListCategoryFiter(categoriesList)
                }
            })
        categories_recycler.adapter = categorySortAdapter

        manufacturer_recycler.layoutManager = LinearLayoutManager(baseContext)

        manufacturerAdapter = CategorySortAdapter(
            PaperIO.manufactures,
            CategorySortAdapter.ItemSettings.MANUFACTURE,
            onClickSelect = { isSelected, item ->
                if (isSelected) {
                    manufacturerList.add(item)
                    setListManufactureFiter(manufacturerList)
                } else {
                    manufacturerList.remove(item)
                    setListManufactureFiter(manufacturerList)
                }
            })
        manufacturer_recycler.adapter = manufacturerAdapter
    }

    override fun onBackPressed(): Boolean {
        return true
    }
}