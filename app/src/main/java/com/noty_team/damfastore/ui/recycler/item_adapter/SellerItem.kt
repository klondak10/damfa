package com.noty_team.damfastore.ui.recycler.item_adapter

import com.noty_team.damfastore.utils.base_adapters.BaseUiPaginationItem

data class SellerItem(
    val customersSellerName: String? = null,

    val customersSellerStatus: Boolean? = null,

    val customersSellerPassword: String? = null,

    val customersSellerId: String? = null,

    val customersSellerCompany: String? = null,

    val customersSellerEmail: String? = null,

    val customersSellerPhone: String? = null
) : BaseUiPaginationItem()