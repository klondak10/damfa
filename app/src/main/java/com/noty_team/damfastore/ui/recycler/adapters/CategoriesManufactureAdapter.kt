package com.noty_team.damfastore.ui.recycler.adapters

import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.ui.recycler.item_adapter.CategoriesManufactureItem
import com.noty_team.damfastore.utils.base_adapters.BaseAdapter
import kotlinx.android.synthetic.main.item_category_manufacture.*

class CategoriesManufactureAdapter(
    list: ArrayList<CategoriesManufactureItem>,
    val itemSetting: ItemSettings,
    val onClickSelect: (isSelected: Boolean, listProducts: List<String>) -> Unit = {_,_ ->},
    val onItemClick: (categoryItem: CategoriesManufactureItem) -> Unit
) :
    BaseAdapter<CategoriesManufactureItem, CategoriesManufactureAdapter.ViewHolder>(list) {

    var counterSelected = 0

    override fun getItemViewType(position: Int) = R.layout.item_category_manufacture

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            itemView.setOnClickListener {
                onItemClick(list[position])
            }

            selected_button.setOnClickListener {
                val item = list[position]
                item.isSelected = !item.isSelected

                isSelected(item.isSelected)

                onClickSelect(item.isSelected, item.listProducts)
            }
        }

        override fun bind(pos: Int) {
            val item = list[position]

            if (itemSetting == ItemSettings.CATEGORY)
                logo_item.setImageResource(R.mipmap.ic_category)
            else if (itemSetting == ItemSettings.MANUFACTURE)
                logo_item.setImageResource(R.mipmap.ic_manufacture)

            isSelected(item.isSelected)

            if (item.isSelected) {
                ++counterSelected
            }

            name_item.text = item.manufactureName
        }

        fun isSelected(isSelected: Boolean) {
            if (isSelected) {
                selected_button.setImageResource(R.mipmap.ic_select)
            } else {
                selected_button.setImageResource(R.mipmap.ic_unselect)
            }
        }
    }

    fun getCounter(): Int {
        var counterSelected = 0
        list.forEach {
            if (it.isSelected)
                ++counterSelected
        }

        return counterSelected
    }

    fun editItem(data: CategoriesManufactureItem) {
        list.forEachIndexed { index, item ->
            if (item.id == data.id) {
                list[index] = data
                notifyItemChanged(index)
                return@forEachIndexed
            }
        }
    }

    fun changeSelectedUIById(categoryId: String , isSelected:Boolean) {
        for (i in list.indices) {
            if (list[i].id == categoryId) {
                list[i].isSelected = isSelected
                notifyItemChanged(i)
                break
            }
        }
    }

    fun resetSelectedButton() {
        list.forEach {
            it.isSelected = false
            notifyDataSetChanged()
        }
    }

    enum class ItemSettings {
        CATEGORY, MANUFACTURE
    }

    fun List(): ArrayList<CategoriesManufactureItem> {
        return list
    }
}