package com.noty_team.damfastore.ui.fragment.login.seller.product_management.products_category

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment


class DialogChooseSort : DialogFragment() {
    val DATA = "items"

    val SELECTED = "selected"

    var callbackChooseLanguage: (pos: Int) -> Unit = {}
    var position: Int = 2

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val bundle = arguments

        val dialog = AlertDialog.Builder(activity)
        dialog.setTitle("Bitte wählen")
        dialog.setPositiveButton("Abbrechen", PositiveButtonClickListener())

        val list = bundle!!.get(DATA) as List<String>

        val cs = list.toTypedArray()
        dialog.setSingleChoiceItems(cs, position, selectItemListener)

        return dialog.create()
    }

    internal inner class PositiveButtonClickListener : DialogInterface.OnClickListener {
        override fun onClick(dialog: DialogInterface, which: Int) {
            dialog.dismiss()
        }
    }

    private var selectItemListener: DialogInterface.OnClickListener =
        DialogInterface.OnClickListener { dialog, which ->
            run {
                callbackChooseLanguage(which)
                dialog?.dismiss()
            }
    }
}