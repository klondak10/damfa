package com.noty_team.damfastore.ui.recycler.adapters

import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.ui.recycler.item_adapter.ProductRecyclerItem
import com.noty_team.damfastore.utils.base_adapters.BaseAdapter
import com.noty_team.damfastore.utils.paper.PaperIO
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.item_product.*
import noty_team.com.urger.utils.setPhotoProductsTT

class ProductAdapterForCustomer(
    list: ArrayList<ProductRecyclerItem>,
    val itemClick: ItemClickAdapter,
    val onCheck: () -> Unit = {},
    val unCheck: () -> Unit = {},
    val onAllDataLoaded: () -> Unit = {}
) : BaseAdapter<ProductRecyclerItem, ProductAdapterForCustomer.ViewHolder>(list) {

    override fun getItemViewType(position: Int) = R.layout.item_product

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            itemView.setOnClickListener {
                itemClick.onItemCLick(list[position].id, list[position].isSelected)

            }

            selected_product.setOnClickListener {
                selectedLogic()
                unCheck()
            }

            unselected_product.setOnClickListener {
                selectedLogic()
                onCheck()
            }
        }

        override fun bind(pos: Int) {
            val item = list[pos]

            product_name.text = item.productName


            if (!item.productImageStore.isNullOrEmpty()) {
                setPhotoProductsTT(item_image, item.productImageStore)
            } else {
                if (item.productImage.isNotEmpty()) {

                    setPhotoProductsTT(item_image, item.productImage)

                } else item_image.setImageResource(R.mipmap.test_logo2)
            }

            isSelected(item.isSelected)

            if (position == list.size - 1) {
                onAllDataLoaded()
            }

        }

        fun isSelected(isSelected: Boolean) {
            if (isSelected) {
                selected_product.visibility = View.VISIBLE
                unselected_product.visibility = View.GONE
                //  .setImageResource(R.mipmap.ic_select)
            } else {
                selected_product.visibility = View.GONE
                unselected_product.visibility = View.VISIBLE
                //selected_product.setImageResource(R.mipmap.ic_unselect)
            }
        }

    }

    private fun ViewHolder.selectedLogic() {
        val item = list[position]
        item.isSelected = !item.isSelected
        isSelected(item.isSelected)

        if (!item.isSelected)
            PaperIO.getSingleRecycler { listRecycler ->
                Single.fromCallable { listRecycler }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : SingleObserver<ArrayList<ProductRecyclerItem>> {
                        override fun onSuccess(t: ArrayList<ProductRecyclerItem>) {
                            listRecycler.remove(
                                ProductRecyclerItem(
                                    item.id,
                                    item.productName,
                                    item.fillterName,
                                    item.desName,
                                    item.productImage,
                                    item.productImageStore,
                                    true
                                )
                            )
                            PaperIO.recycle = listRecycler
                        }

                        override fun onSubscribe(d: Disposable) {
                        }

                        override fun onError(e: Throwable) {}
                    }
                    )
            }
        else {

            val listEnded = HashSet<ProductRecyclerItem>(PaperIO.recycle)
            listEnded.add(item)
            PaperIO.recycle = ArrayList(listEnded.toList())
        }
    }

    fun getSelected(): ArrayList<ProductRecyclerItem> {

        val selectedlist = ArrayList<ProductRecyclerItem>()

        list.forEach {
            if (it.isSelected) {
                selectedlist.add(it)
            }
        }

        return selectedlist
    }

    fun resetSelectedButton() {
        list.forEach {
            for (i in PaperIO.deletedList) {
                if (it.id == i.id) {
                    it.isSelected = false
                }
            }
            notifyDataSetChanged()
        }
    }


    fun sortList(isSorted: Boolean) {
        if (isSorted) {
            list.sortBy {
                it.productName
            }
        } else {
            list.reverse()
        }
        notifyDataSetChanged()
    }

    fun insert(item: ProductRecyclerItem) {
        list.forEach {
            if (it.id == item.id) {
                it.isSelected = item.isSelected
                notifyDataSetChanged()
            }
        }
    }

    fun getDataList(): ArrayList<ProductRecyclerItem> {
        return list
    }


    fun addAllList(data: ArrayList<ProductRecyclerItem>) {
        list.addAll(data)
        notifyDataSetChanged()
    }

}