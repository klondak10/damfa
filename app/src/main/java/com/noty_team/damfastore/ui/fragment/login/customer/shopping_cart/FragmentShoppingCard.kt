package com.noty_team.damfastore.ui.fragment.login.customer.shopping_cart

import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.ui.fragment.login.customer.category.FragmentCustomerSearch
import com.noty_team.damfastore.ui.fragment.login.customer.product_detail.ProductDetailFragment
import com.noty_team.damfastore.ui.fragment.login.customer.products.FragmentCustomerProducts
import com.noty_team.damfastore.ui.recycler.adapters.ItemClickAdapter
import com.noty_team.damfastore.ui.recycler.adapters.ShoppingCardAdapter
import com.noty_team.damfastore.utils.paper.PaperIO
import kotlinx.android.synthetic.main.fragment_shopping_card.*

class FragmentShoppingCard : BaseFragment<ShoppingCardPresenter>() {
    lateinit var itemClick: ItemClickAdapter
    lateinit var adapter: ShoppingCardAdapter

    companion object {
        fun newInstance() = FragmentShoppingCard()
    }

    override fun layout() = R.layout.fragment_shopping_card

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.toggleKeyboard(false)
        validateDeleteButton()
        adapter = ShoppingCardAdapter(PaperIO.recycle)
        back_shopping_card.getClick {
            onBackPressed()
        }

        shopping_card_select_all.getClick {
            adapter.selectAll()
        }

        shopping_card_delete_selected.getClick {
            adapter.deleteSelected()


//            (shopping_card_recycler.adapter as ShoppingCardAdapter).clear()
//            PaperIO.recycle = arrayListOf()
            ProductDetailFragment.isItemSelected = false
            FragmentCustomerProducts.isClearShoppingCard = true
            FragmentCustomerSearch.isClearShoppingCard = true
            validateDeleteButton()
        }

        shopping_card_recycler.layoutManager = GridLayoutManager(baseContext, 3)

        shopping_card_recycler.adapter = adapter



    }

    private fun validateDeleteButton() {

        if (PaperIO.recycle.size > 0) {
            shopping_card_select_all.visibility = View.VISIBLE
            shopping_card_delete_selected.visibility = View.VISIBLE
        } else {
            shopping_card_select_all.visibility = View.GONE
            shopping_card_delete_selected.visibility = View.GONE
        }
    }

}