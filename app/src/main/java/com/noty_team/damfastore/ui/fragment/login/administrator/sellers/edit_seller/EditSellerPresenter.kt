package com.noty_team.damfastore.ui.fragment.login.administrator.sellers.edit_seller

import android.content.Context
import com.noty_team.damfastore.base.fragment.BasePresenter
import com.noty_team.damfastore.ui.recycler.item_adapter.SellerItem

class EditSellerPresenter(context: Context) : BasePresenter(context) {


    fun editSeller(sellerItem: SellerItem, onSuccess: (sellerItem: SellerItem) -> Unit) {

        if (!sellerItem.customersSellerId.isNullOrEmpty())
            apiAuthorization.editSeller(sellerItem.customersSellerId,
                sellerItem.customersSellerEmail ?: "",
                sellerItem.customersSellerPassword ?: "",
                sellerItem.customersSellerName ?: "",
                sellerItem.customersSellerCompany ?: "",
                sellerItem.customersSellerPhone ?: "",
                onSuccess = {

                    if (it.success == true) {
                        onSuccess(it.mapToSeller())
                    } else {
                        showShortToast("success false")
                    }

                },
                onError = { errorMessage, errorCode ->

                    showShortToast("error")

                }).callRequest()
    }
}