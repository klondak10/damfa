package com.noty_team.damfastore.ui.fragment.login.administrator.sellers.add_seller

import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.api.request.create_new_seller.CreateNewSellerRequest
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.ui.recycler.item_adapter.SellerItem
import kotlinx.android.synthetic.main.fragment_add_seller.*

class FragmentAddSeller : BaseFragment<AddSellerPresenter>() {

    companion object {
        fun newInstance() = FragmentAddSeller()
    }

    override fun layout() = R.layout.fragment_add_seller


    var callbackAddSeller: (item: SellerItem) -> Unit = {}

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.toggleKeyboard(false)
        presenter = AddSellerPresenter(baseContext)

        initAction()
    }

    private fun initAction() {
        create_seller_btn.getClick {
            presenter?.createNewSeller(CreateNewSellerRequest(
                name = name_surname_seller.text.toString(),
                company = company_name_seller.text.toString(),
                phone = phone_seller.text.toString(),
                email = email_seller.text.toString(),
                password = password_seller.text.toString()
            ), isSuccess = { isCreated, sellerItem ->
                if (isCreated) {
                    callbackAddSeller(sellerItem)
                    baseActivity.onBackPressed()

                } else {
                    showShortToast("user isn't created")
                }
            })
        }
    }
}