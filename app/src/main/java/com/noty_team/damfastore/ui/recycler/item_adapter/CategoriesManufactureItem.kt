package com.noty_team.damfastore.ui.recycler.item_adapter

data class CategoriesManufactureItem(
    var id: String,
    var manufactureName: String = "",
    var isSelected: Boolean = false,
    var listProducts: List<String> = emptyList()
)