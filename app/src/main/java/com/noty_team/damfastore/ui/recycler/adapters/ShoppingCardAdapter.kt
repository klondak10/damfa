package com.noty_team.damfastore.ui.recycler.adapters

import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.ui.recycler.item_adapter.ProductRecyclerItem
import com.noty_team.damfastore.utils.base_adapters.BaseAdapter
import com.noty_team.damfastore.utils.paper.PaperIO
import kotlinx.android.synthetic.main.item_shopping_card.*
import noty_team.com.urger.utils.setPhotoProductsTT

class ShoppingCardAdapter(
    list: ArrayList<ProductRecyclerItem>
) :
    BaseAdapter<ProductRecyclerItem, ShoppingCardAdapter.ViewHolder>(list) {
    var tmp = false
    private val listItems = hashSetOf<ProductRecyclerItem>()

    override fun getItemViewType(position: Int) = R.layout.item_shopping_card

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    fun selectAll() {
        tmp = !tmp
        notifyDataSetChanged()
    }

    fun deleteSelected() {

        list.removeAll(listItems)

        for (item in listItems) {
            item.isSelected = false
        }
        PaperIO.recycle = list
        PaperIO.deletedList = ArrayList(listItems)
//        listItems.clear()
        tmp = false
        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {

            checkBox.setOnClickListener {
                if (checkBox.isChecked) {
                    checkBox.setBackgroundResource(R.drawable.ic_select)
                    llRootItem.setBackgroundResource(R.drawable.button_white_selected)
                    selectedLogic(checkBox.isChecked)
                } else {
                    checkBox.setBackgroundResource(R.drawable.ic_unselect)
                    llRootItem.setBackgroundResource(R.drawable.button_white_normal)
                    selectedLogic(checkBox.isChecked)
                }
            }
        }

        override fun bind(pos: Int) {

            val item = list[pos]

            if (!item.productImageStore.isNullOrEmpty()) {
                setPhotoProductsTT(image_shopping_card, item.productImageStore)
            } else {
                if (item.productImage.isNotEmpty()) {

                    setPhotoProductsTT(image_shopping_card, item.productImage)

                } else image_shopping_card.setImageResource(R.mipmap.test_logo2)
            }

            //setPhoto(image_shopping_card, item.productImage)
            name_shopping_card.text = item.productName
            if (tmp) {
                checkBox.isChecked = true
                checkBox.setBackgroundResource(R.drawable.ic_select)
                llRootItem.setBackgroundResource(R.drawable.button_white_selected)
                selectedLogic(checkBox.isChecked)
            } else {
                checkBox.isChecked = false
                checkBox.setBackgroundResource(R.drawable.ic_unselect)
                llRootItem.setBackgroundResource(R.drawable.button_white_normal)
                selectedLogic(checkBox.isChecked)
            }
        }
    }

    private fun ViewHolder.selectedLogic(isSelected: Boolean) {
        val item = list[position]

        if (isSelected) {
            listItems.add(item)
        } else {
            listItems.remove(item)
        }

//        item.isSelected = !item.isSelected
//
//
//        if (!item.isSelected)
//            PaperIO.getSingleRecycler { listRecycler ->
//                Single.fromCallable { listRecycler }
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(object : SingleObserver<ArrayList<ProductRecyclerItem>> {
//                        override fun onSuccess(t: ArrayList<ProductRecyclerItem>) {
//                            listRecycler.remove(
//                                ProductRecyclerItem(
//                                    item.id,
//                                    item.productName,
//                                    item.productImage,
//                                    item.productImageStore,
//                                    true
//                                )
//                            )
//                            PaperIO.recycle = listRecycler
//                        }
//
//                        override fun onSubscribe(d: Disposable) {
//                        }
//
//                        override fun onError(e: Throwable) {}
//                    }
//                    )
//            } else {
//
//            val listEnded = HashSet<ProductRecyclerItem>(PaperIO.recycle)
//            listEnded.add(item)
//            PaperIO.recycle = ArrayList(listEnded.toList())
//        }
    }

}

interface Callback {
    fun onClick()
}
