package com.noty_team.damfastore.ui.fragment.login.customer.products

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.SearchView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.ui.fragment.login.customer.category.FragmentCustomerCategory

import com.noty_team.damfastore.ui.fragment.login.customer.product_detail.ProductDetailFragment
import com.noty_team.damfastore.ui.fragment.login.customer.product_detail.ProductDetailFragment.Companion.isItemSelected
import com.noty_team.damfastore.ui.fragment.login.customer.shopping_cart.FragmentShoppingCard
import com.noty_team.damfastore.ui.fragment.login.seller.product_management.product_filters.subfilter.FragmentSubFilterProduct
import com.noty_team.damfastore.ui.fragment.login.seller.product_management.products_category.DialogChooseSort
import com.noty_team.damfastore.ui.recycler.adapters.ItemClickAdapter
import com.noty_team.damfastore.ui.recycler.adapters.ProductAdapterForCustomer
import com.noty_team.damfastore.ui.recycler.item_adapter.ProductRecyclerItem
import com.noty_team.damfastore.utils.paper.PaperIO
import com.noty_team.damfastore.utils.paper.products.ProductItem
import io.reactivex.Observable
import kotlinx.android.synthetic.main.drawer_header.*
import kotlinx.android.synthetic.main.fragment_customer_category.*
import kotlinx.android.synthetic.main.products_content.*
import kotlinx.android.synthetic.main.products_content.product_content_search
import kotlinx.android.synthetic.main.products_content.txt_count

class FragmentCustomerProducts : BaseFragment<CustomerProductsPresenter>(),
    NavigationView.OnNavigationItemSelectedListener {

    companion object {
        fun newInstance() = FragmentCustomerProducts()
        var isClearShoppingCard = false
    }

    private var _listProductRecycler = ArrayList<ProductRecyclerItem>()

    var filterNames = "Geschmack"

    var isNeedOpenDetailFragment = false

    var preCounter = 0

    var changedProduct: Observable<Unit>? = null

    var nameCategory = ""

    var categoryID = ""
    var search: Boolean = true
    private var productSize = 0

    var isCallbackDeleteIsSelected = false

    var productIdList = listOf<String>()

    lateinit var itemClick: ItemClickAdapter

    private lateinit var productAdapter: ProductAdapterForCustomer

    override fun layout() = R.layout.fragment_products

    private var currentPosChooseDialog = 0

    private lateinit var sort: TextView

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.toggleKeyboard(false)
        sort = view.findViewById(R.id.sort)

        if (isFirstInit) {
            initAdapterInterfaceClick()
            initSearchListener()
            getAllFromDBAsynk()
            initView()
            initAction()
        } else {
            if (isClearShoppingCard) {
                if (this::productAdapter.isInitialized)
                    productAdapter.resetSelectedButton()
                isClearShoppingCard = false
            }
        }


        if (PaperIO.recycle.size > 0) {
            txt_count.visibility = View.VISIBLE
            txt_count.text = PaperIO.recycle.size.toString()
        }else{
            txt_count.visibility = View.GONE
        }

    }

    private fun initAdapterInterfaceClick() {
        itemClick = object : ItemClickAdapter {
            override fun onItemCLick(id: String, isSelected: Boolean) {

                saveToRecycle()

                addFragment(ProductDetailFragment.newInstance().apply {
                    productID = id
                    isItemSelected = isSelected
                    callbackSelected = this@FragmentCustomerProducts.callbackAddSeller
                })
            }
        }
    }

    var callbackAddSeller: (item: ProductRecyclerItem) -> Unit = {
        isCallbackDeleteIsSelected = true
        changedProduct =
            Observable.fromCallable { editItem(it) }
    }

    private fun editItem(item: ProductRecyclerItem) {
        productAdapter.insert(item)
    }


    private fun initView() {
        if (nameCategory == "") name_category_customer.text = "Products"
        else name_category_customer.text = nameCategory
    }

    override fun onResume() {
        super.onResume()
        baseActivity.toggleKeyboard(false)

        if (PaperIO.getFilterCounter() != preCounter) {
            getAllFromDBAsynk()
        }

        if (isCallbackDeleteIsSelected) {
            isCallbackDeleteIsSelected = false
            changedProduct?.subscribe()
        }
    }

    private fun initSearchListener() {

        product_content_search.setOnClickListener({ product_content_search.setIconified(false) })

        product_content_search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                if (search){
                    if (newText.length>2) {

                        PaperIO.searchProductByNameSync(_listProductRecycler, newText,
                            onAllDataFind = { listSearchProducts ->
                                PaperIO.getSingleRecycler { recyclerList ->
                                    for (i in recyclerList.indices) {
                                        for (j in listSearchProducts.indices)
                                            if (recyclerList[i].id == listSearchProducts[j].id) {
                                                listSearchProducts[j].isSelected = true
                                            }
                                    }

                                    productAdapter.clear()
                                    productAdapter.addAll(listSearchProducts)
                                }
                            }, onError = {
                                Log.d("mylogs", "1")
                                isShowLoadingDialog(false)
                            }
                        )

                    }
                    else if (newText.length==0)  {
                        search = false
                        if (!search)
                        productAdapter.clear()
                        getAllFromDBAsynk()


                    }
                }



                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {


                return false
            }
        })




    }

    private fun initAction() {

        products_shopping_card.setOnClickListener {

            saveToRecycle()

            addFragment(FragmentShoppingCard.newInstance())

        }


        sort.setOnClickListener {

            val manager = fragmentManager
            val dialog = DialogChooseSort().apply {
                callbackChooseLanguage = this@FragmentCustomerProducts.callbackChooseLanguage
                position = this@FragmentCustomerProducts.currentPosChooseDialog
            }


            val bundle = Bundle().apply {
                putStringArrayList(dialog.DATA, getItems())     // Require ArrayList
                putInt(dialog.SELECTED, this@FragmentCustomerProducts.currentPosChooseDialog)
            }

            dialog.arguments = bundle
            dialog.show(manager!!, "Dialog")

        }

        products_filter.setOnClickListener {
            saveToRecycle()

            filters_names.text = filterNames

            delete_all_filters?.setOnClickListener {
                isShowLoadingDialog(true)
                PaperIO.clearAllFilters {
                    baseActivity.findViewById<DrawerLayout>(R.id.drawer)
                        .closeDrawer(GravityCompat.END)
                    getAllFromDBAsynk()
                }
            }

            baseActivity.findViewById<DrawerLayout>(R.id.drawer)
                .openDrawer(GravityCompat.END)  /*drawer.closeDrawer()*/

            ok_drawer_btn?.setOnClickListener {
                baseActivity.findViewById<DrawerLayout>(R.id.drawer)
                    .closeDrawer(GravityCompat.END)
            }

            drawer_filter_characteristic?.setOnClickListener {
                baseActivity.findViewById<DrawerLayout>(R.id.drawer)
                    .closeDrawer(GravityCompat.END)
                android.os.Handler()
                    .postDelayed({ addFragment(FragmentSubFilterProduct.newInstance()) }, 400)

            }
        }

        back_customer_products.setOnClickListener {
            PaperIO.resetAllFilters()
            baseActivity.onBackPressed()
        }
    }

    var callbackChooseLanguage: (pos: Int) -> Unit = {
        if(currentPosChooseDialog != it) {
            currentPosChooseDialog = it

            if (it==0){
                productAdapter.sortList(true)

            }else{
                productAdapter.sortList(false)
            }
        }

    }

    private fun getItems(): java.util.ArrayList<String>? {
        return (resources.getStringArray(R.array.name_sort)).toCollection(java.util.ArrayList())
    }


    override fun onBackPressed(): Boolean {
        saveToRecycle()
        return true
    }

    override fun onDetach() {
        PaperIO.resetAllFilters()
        saveToRecycle()
        super.onDetach()
    }

    override fun onDestroy() {
        baseActivity.supportFragmentManager.popBackStack()
        super.onDestroy()
    }

    private fun saveToRecycle() {
        if (this::productAdapter.isInitialized) {
            val selectedList = productAdapter.getSelected()

            val listEnded = HashSet<ProductRecyclerItem>(PaperIO.recycle)

            listEnded.addAll(selectedList.toHashSet())

            PaperIO.recycle = ArrayList(listEnded.toList())
        }
    }

    private fun initProductAdapter(list: ArrayList<ProductRecyclerItem>) {

        customer_product_recycler.layoutManager = GridLayoutManager(baseContext, 3)
        productAdapter = ProductAdapterForCustomer(list,
            itemClick = itemClick,
            onAllDataLoaded = {
                isShowLoadingDialog(false)
            },
            onCheck = {
                if (PaperIO.recycle.size > 0) {
                    txt_count.visibility = View.VISIBLE
                    txt_count.text = PaperIO.recycle.size.toString()
                }
            },
            unCheck = {
                if ((PaperIO.recycle.size-1)== 0) {
                    txt_count.visibility = View.GONE

                }else{
                    txt_count.visibility = View.VISIBLE
                    txt_count.text = (PaperIO.recycle.size-1).toString()
                }
            }
        )
        customer_product_recycler.adapter = productAdapter
        android.os.Handler().postDelayed({
            isShowLoadingDialog(false)
        }, 1000)
    }


    private fun getAllFromDBAsynk() {

        isShowLoadingDialog(true)

        if (PaperIO.getFilterCounter() > 0) {
            preCounter = PaperIO.getFilterCounter()
            PaperIO.getProductByIdIfSelected(productIdList) { listProductItems ->
                PaperIO.getAllSelectedFilterNames {

                    filterNames = if (it.isNotEmpty())
                        it
                    else
                        resources.getString(R.string.all)

                    PaperIO.getRecycleToSelectedProducts(listProductItems) {
                        PaperIO.getProductsByIdsCategoryByFilters(it) {
                            _listProductRecycler = convertToRecyclerItem(it)
                            if (it.size > 0) {
                                isShowTextWhenNoProducts(false)
                            } else {
                                isShowTextWhenNoProducts(true)
                            }
                            initProductAdapter(convertToRecyclerItem(it))
                        }
                    }
                }
            }
        } else {
            //if no filters
            preCounter = 0
            PaperIO.getProductsByIdCustomer(productIdList) { listProductItems ->
                PaperIO.getRecycleToSelectedProducts(listProductItems) {
                    isShowTextWhenNoProducts(false)
                    _listProductRecycler = convertToRecyclerItem(it)
                    initProductAdapter(convertToRecyclerItem(it))

                }
            }
        }
    }


    private fun isShowTextWhenNoProducts(isShow: Boolean) {
        if (isShow) text_when_no_products.visibility = View.VISIBLE
        else text_when_no_products.visibility = View.GONE
    }

    private fun convertToRecyclerItem(it: java.util.ArrayList<ProductItem>): ArrayList<ProductRecyclerItem> {

        val listRecycler = it.flatMap { productItem ->
            arrayListOf(
                ProductRecyclerItem(
                    id = productItem.productsId ?: "",
                    isSelected = productItem.isSelected,
                    productImage = productItem.productsImage ?: "",
                    productName = productItem.productsAppName ?: "",
                    fillterName = productItem.filter_name ?: arrayListOf(),
                    desName = productItem.productsDescription ?: "",
                    productImageStore = productItem.image
                )
            )
        } as ArrayList<ProductRecyclerItem>

        productSize = listRecycler.size

        return listRecycler
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.ok_drawer_btn -> {
            }
        }

        baseActivity.findViewById<DrawerLayout>(R.id.drawer).closeDrawer(GravityCompat.END)
        return true
    }
}