package com.noty_team.damfastore.ui.fragment.login.customer.category

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.ui.fragment.login.customer.product_detail.ProductDetailFragment
import com.noty_team.damfastore.ui.fragment.login.customer.shopping_cart.FragmentShoppingCard
import com.noty_team.damfastore.ui.fragment.login.seller.product_management.products_category.DialogChooseSort
import com.noty_team.damfastore.ui.recycler.adapters.ItemClickAdapter
import com.noty_team.damfastore.ui.recycler.adapters.ProductSearchAdapterForCustomer
import com.noty_team.damfastore.ui.recycler.item_adapter.ProductRecyclerItem
import com.noty_team.damfastore.utils.paper.PaperIO
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_customer_search.*
import kotlinx.android.synthetic.main.fragment_customer_search.back_customer_products
import kotlinx.android.synthetic.main.fragment_customer_search.sort
import kotlinx.android.synthetic.main.fragment_customer_search.txt_count
import kotlinx.android.synthetic.main.products_content.*

class FragmentCustomerSearch : BaseFragment<CustomerCategoryPresenter>() {
    companion object {
        var isClearShoppingCard = false
    }

    override fun layout(): Int = R.layout.fragment_customer_search
    private var isSortedList: Boolean = false
    private lateinit var productAdapter: ProductSearchAdapterForCustomer
    private lateinit var itemClick: ItemClickAdapter
    private var isCallbackDeleteIsSelected = false
    private var changedProduct: Observable<Unit>? = null
    var listProducts = ArrayList<ProductRecyclerItem>()
    private var currentPosChooseDialog = 0
    override fun initialization(view: View, isFirstInit: Boolean) {

        if (isFirstInit) {
            initAdapterInterfaceClick()
            initProductAdapter(listProducts)
        } else {
            if (isClearShoppingCard) {
                if (this::productAdapter.isInitialized)
                    productAdapter.resetSelectedButton()
                isClearShoppingCard = false
            }
        }
        back_customer_products.setOnClickListener {
            PaperIO.resetAllFilters()
            baseActivity.onBackPressed()
        }

        products_shopping.setOnClickListener {
            saveToRecycle()
            addFragment(FragmentShoppingCard.newInstance())
        }

        sort.setOnClickListener {
            val manager = fragmentManager
            val dialog = DialogChooseSort().apply {
                callbackChooseLanguage = this@FragmentCustomerSearch.callbackChooseLanguage
                position = this@FragmentCustomerSearch.currentPosChooseDialog
            }


            val bundle = Bundle().apply {
                putStringArrayList(dialog.DATA, getItems())     // Require ArrayList
                putInt(dialog.SELECTED, this@FragmentCustomerSearch.currentPosChooseDialog)
            }

            dialog.arguments = bundle
            dialog.show(manager!!, "Dialog")
        }

        if (PaperIO.recycle.size > 0) {
            txt_count.visibility = View.VISIBLE
            txt_count.text = PaperIO.recycle.size.toString()
        }else{
            txt_count.visibility = View.GONE
        }

    }


    var callbackChooseLanguage: (pos: Int) -> Unit = {
        if(currentPosChooseDialog != it) {
            currentPosChooseDialog = it

            if (it==0){
                productAdapter.sortList(true)

            }else{
                productAdapter.sortList(false)
            }
        }

    }

    private fun getItems(): java.util.ArrayList<String>? {
        return (resources.getStringArray(R.array.name_sort)).toCollection(java.util.ArrayList())
    }

    override fun onResume() {
        super.onResume()
        baseActivity.toggleKeyboard(false)

        if (isCallbackDeleteIsSelected) {
            isCallbackDeleteIsSelected = false
            changedProduct?.subscribe()
        }
    }

    private fun initProductAdapter(list: ArrayList<ProductRecyclerItem>) {
        isShowLoadingDialog(true)
        customer_search_product_recycler.layoutManager = GridLayoutManager(context, 3)

        productAdapter = ProductSearchAdapterForCustomer(list,
            itemClick = itemClick,

            onCheck = {
                if (PaperIO.recycle.size > 0) {
                    txt_count.visibility = View.VISIBLE
                    txt_count.text = PaperIO.recycle.size.toString()
                }
            },
            unCheck = {
                if ((PaperIO.recycle.size-1)== 0) {
                    txt_count.visibility = View.GONE

                }else{
                    txt_count.visibility = View.VISIBLE
                    txt_count.text = (PaperIO.recycle.size-1).toString()
                }
            },
            onAllDataLoaded = {
            }
        )
        customer_search_product_recycler.adapter = productAdapter


        android.os.Handler().postDelayed({ isShowLoadingDialog(false) }, 1000)
    }

    private fun initAdapterInterfaceClick() {
        itemClick = object : ItemClickAdapter {
            override fun onItemCLick(id: String, isSelected: Boolean) {

                saveToRecycle()

                addFragment(ProductDetailFragment.newInstance().apply {
                    productID = id
                    ProductDetailFragment.isItemSelected = isSelected
                    callbackSelected = this@FragmentCustomerSearch.callbackAddSeller
                })
            }
        }
    }

    var callbackAddSeller: (item: ProductRecyclerItem) -> Unit = {
        isCallbackDeleteIsSelected = true
        changedProduct =
            Observable.fromCallable { editItem(it) }
    }

    private fun editItem(item: ProductRecyclerItem) {
        productAdapter.insert(item)
    }

    private fun saveToRecycle() {
        if (this::productAdapter.isInitialized) {
            val selectedList = productAdapter.getSelected()

            val listEnded = HashSet<ProductRecyclerItem>(PaperIO.recycle)

            listEnded.addAll(selectedList.toHashSet())

            PaperIO.recycle = ArrayList(listEnded.toList())
        }
    }
}