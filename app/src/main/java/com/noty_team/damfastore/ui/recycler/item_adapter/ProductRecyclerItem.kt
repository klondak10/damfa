package com.noty_team.damfastore.ui.recycler.item_adapter

data class ProductRecyclerItem(
    val id: String,
    val productName: String = "",
    val fillterName: List<String> = arrayListOf(),
    val desName: String = "",
    val productImage: String = "",
    val productImageStore:String = "",
    var isSelected: Boolean = false
){
    override fun toString(): String {
        return "ProductRecyclerItem(id='$id', productName='$productName', fillterName='$fillterName', desName='$desName', productImage='$productImage', productImageStore='$productImageStore', isSelected=$isSelected)"
    }
}