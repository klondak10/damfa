package com.noty_team.damfastore.ui.fragment.login.customer.category

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.ui.fragment.login.customer.products.FragmentCustomerProducts
import com.noty_team.damfastore.ui.fragment.login.customer.shopping_cart.FragmentShoppingCard
import com.noty_team.damfastore.ui.fragment.login.seller.FragmentLoginSeller
import com.noty_team.damfastore.ui.recycler.adapters.CategoryCustomerAdapter
import com.noty_team.damfastore.ui.recycler.item_adapter.CategoriesManufactureItem
import com.noty_team.damfastore.ui.recycler.item_adapter.ProductRecyclerItem
import com.noty_team.damfastore.utils.paper.PaperIO
import com.noty_team.damfastore.utils.paper.PaperIO.Companion.categoriesFilter
import com.noty_team.damfastore.utils.paper.PaperIO.Companion.manufactureFilter
import com.noty_team.damfastore.utils.paper.products.ProductItem
import kotlinx.android.synthetic.main.fragment_customer_category.*
import kotlinx.android.synthetic.main.products_content.txt_count


class FragmentCustomerCategory : BaseFragment<CustomerCategoryPresenter>() {

    companion object {
        fun newInstance() = FragmentCustomerCategory()
    }

    private var listProductRecycler = ArrayList<ProductRecyclerItem>()
    private var listProductRecycler1 = ArrayList<ProductRecyclerItem>()
    private var listCategories = ArrayList<String>()
    private var listCategories1 = ArrayList<String>()
    private lateinit var categoryCustomerAdapter: CategoryCustomerAdapter

    override fun layout() = R.layout.fragment_customer_category

    override fun initialization(view: View, isFirstInit: Boolean) {

        if (PaperIO.recycle.size > 0) {
            txt_count.visibility = View.VISIBLE
            txt_count.text = PaperIO.recycle.size.toString()
        }else{
            txt_count.visibility = View.GONE
        }


        baseActivity.toggleKeyboard(false)
        presenter = CustomerCategoryPresenter(baseContext)


        if (isFirstInit) {
            isShowLoadingDialog(true)
            presenter?.getCategory {
                if (categoriesFilter.isNotEmpty()) {
                    initRecycler(categoriesFilter)
                    for (category in categoriesFilter) {
                        listCategories.addAll(category.listProducts)
                    }
                } else {
                    initRecycler(it)
                    for (category in it) {
                        listCategories.addAll(category.listProducts)
                    }

                }
                initListForSearch(listCategories)
            }

            presenter?.getManufacture {
                if (manufactureFilter.isNotEmpty()) {
                    initRecycler1(manufactureFilter)
                    for (category in manufactureFilter) {
                        listCategories1.addAll(category.listProducts)
                    }
                } else {
                    initRecycler1(it)
                    for (category in it) {
                        listCategories1.addAll(category.listProducts)
                    }

                }
                initListForSearchManufacture(listCategories1)
            }
            initAction()
        }
    }

    private fun initListForSearchManufacture(listCategories: ArrayList<String>) {
        PaperIO.getProductByIdIfSelected(listCategories) { listProductItems ->

            PaperIO.getAllSelectedFilterNames {

                PaperIO.getRecycleToSelectedProducts(listProductItems) {
                    listProductRecycler1 = convertToRecyclerItem(it)
                }
            }
        }
    }

    private fun initListForSearch(listCategories: ArrayList<String>) {
        PaperIO.getProductByIdIfSelected(listCategories) { listProductItems ->

            PaperIO.getAllSelectedFilterNames {

                PaperIO.getRecycleToSelectedProducts(listProductItems) {
                    listProductRecycler = convertToRecyclerItem(it)
                }
            }
        }
    }

    private fun convertToRecyclerItem(it: ArrayList<ProductItem>): ArrayList<ProductRecyclerItem> {

        val listRecycler = it.flatMap { productItem ->
            arrayListOf(
                ProductRecyclerItem(
                    id = productItem.productsId ?: "",
                    isSelected = productItem.isSelected,
                    productImage = productItem.productsImage ?: "",
                    productName = productItem.productsAppName ?: "",
                    productImageStore = productItem.image
                )
            )
        } as ArrayList<ProductRecyclerItem>

        return listRecycler
    }

    override fun onResume() {
        super.onResume()
        initAction()
    }

    private fun initAction() {
        login_seller_btn.getClick {
            addFragment(FragmentLoginSeller.newInstance())
        }

        filter.getClick {
            replaceFragment(FragmentCustomerCategoryFilter())
        }

        shopping_card_customer_category.getClick {
            addFragment(FragmentShoppingCard.newInstance())
        }

        product_content_search.setOnClickListener({ product_content_search.setIconified(false) })

        product_content_search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {

                if (query.length>2) {
                    PaperIO.searchProductByNameSync(
                        listProductRecycler,
                        query,
                        onAllDataFind = { listSearchProducts ->
                            addFragment(FragmentCustomerSearch().apply {
                                listProducts = listSearchProducts
                            })
                        }, onError = {
                            isShowLoadingDialog(false)
                        }
                    )
                }

                return false
            }
        })


    }
    private fun initRecycler1(arrayList: ArrayList<CategoriesManufactureItem>) {

        manufacturer_recycler_cus.layoutManager = LinearLayoutManager(baseContext)
        categoryCustomerAdapter = CategoryCustomerAdapter(
            ArrayList(arrayList.sortedWith(compareBy({ it.manufactureName}))),
            onItemClick = {
                categoryCustomerAdapter.clear()

                addFragment(FragmentCustomerProducts.newInstance().apply {
                    categoryID = it.id
                    productIdList = it.listProducts
                    nameCategory = it.manufactureName
                    isNeedOpenDetailFragment = true
                })
            }, onAllDataLoad = {
                isShowLoadingDialog(false)
            })

        manufacturer_recycler_cus?.adapter = categoryCustomerAdapter
        isShowLoadingDialog(false)
    }

    private fun initRecycler(arrayList: ArrayList<CategoriesManufactureItem>) {

        customer_category_recycler?.layoutManager = LinearLayoutManager(baseContext)

        categoryCustomerAdapter = CategoryCustomerAdapter(
            ArrayList(arrayList.sortedWith(compareBy({ it.manufactureName}))),
            onItemClick = {
                categoryCustomerAdapter.clear()

                addFragment(FragmentCustomerProducts.newInstance().apply {
                    categoryID = it.id
                    productIdList = it.listProducts
                    nameCategory = it.manufactureName
                    isNeedOpenDetailFragment = true
                })
            }, onAllDataLoad = {
                isShowLoadingDialog(false)
            })

        customer_category_recycler?.adapter = categoryCustomerAdapter

        isShowLoadingDialog(false)
    }

    override fun onDestroyView() {
        subscriptions.clear()
        super.onDestroyView()
    }
}