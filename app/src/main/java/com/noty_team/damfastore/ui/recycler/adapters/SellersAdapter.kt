package com.noty_team.damfastore.ui.recycler.adapters

import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.ui.recycler.item_adapter.SellerItem
import com.noty_team.damfastore.utils.base_adapters.BaseAdapter
import kotlinx.android.synthetic.main.item_seller.*


class SellersAdapter(
    list: ArrayList<SellerItem>,
    var onClick: (data: SellerItem) -> Unit = {},
    val onClickDot: (sellerItem: SellerItem) -> Unit = {}
) :
    BaseAdapter<SellerItem, SellersAdapter.ViewHolder>(list) {

    override fun getItemViewType(position: Int) = R.layout.item_seller
    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            itemView.setOnClickListener {
                onClick(list[position])
            }

            three_dot.setOnClickListener {
                onClickDot(list[position])
            }
        }

        override fun bind(pos: Int) {
        /*    val item = list[pos]

            if (item.status)
                activated_indicator.background =
                    activated_indicator.context.getDrawable(R.drawable.indicator_green)
            else
                activated_indicator.background =
                    activated_indicator.context.getDrawable(R.drawable.indicator_red)

            name_surname.text = item.name_surname
            seller_company.text = item.company
            seller_email.text = item.email
            seller_password.text = item.password
            seller_phone.text = item.phone*/


            val item = list[pos]

            val status = item.customersSellerStatus ?: false

            if (status)
                activated_indicator.background =
                    activated_indicator.context.getDrawable(R.drawable.indicator_green)
            else
                activated_indicator.background =
                    activated_indicator.context.getDrawable(R.drawable.indicator_red)

            name_surname.text = item.customersSellerName
            seller_email.text = item.customersSellerEmail
            seller_phone.text = item.customersSellerPhone
            seller_password.text = "*********"
            seller_company.text = item.customersSellerCompany
        }

    }

    fun editItem(data: SellerItem) {
        list.forEachIndexed { index, item ->
            if (item.customersSellerId == data.customersSellerId) {
                list[index] = data
                notifyItemChanged(index)
                return@forEachIndexed
            }
        }
    }
}
