package com.noty_team.damfastore.ui.activity.login

import android.content.Intent
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.activity.BaseActivity
import com.noty_team.damfastore.ui.fragment.login.administrator.FragmentLoginAdministrator
import com.noty_team.damfastore.ui.fragment.login.customer.category.FragmentCustomerCategory
import com.noty_team.damfastore.ui.fragment.login.customer.category.FragmentCustomerCategoryFilter
import com.noty_team.damfastore.ui.fragment.login.seller.FragmentLoginSeller
import com.noty_team.damfastore.ui.fragment.login.seller.product_management.FragmentProductManagement
import com.noty_team.damfastore.ui.fragment.login.seller.product_management.FragmentProductSort
import com.noty_team.damfastore.utils.paper.PaperIO

class MainActivity : BaseActivity() {

    companion object {
        private const val SCREEN_TYPE_KEY = "SCREEN_TYPE_KEY"

        fun start(baseActivity: BaseActivity, userType: UserType) {

            baseActivity.startActivity(
                Intent(
                    baseActivity,
                    MainActivity::class.java
                )/*.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)*/
                    .putExtra(SCREEN_TYPE_KEY, userType)
            )
        }


    }

    enum class UserType {
        ADMINISTRATOR,
        SELLER
    }

    override fun layout() = R.layout.activity_main

    override fun initialization() {

        when {
            intent.getSerializableExtra(SCREEN_TYPE_KEY) == UserType.ADMINISTRATOR -> {
                replaceFragment(
                    FragmentLoginAdministrator.newInstance()
                )
            }

            else -> {

                PaperIO.getCounterSelected {
                    if (it > 0) {
                        replaceFragment(FragmentCustomerCategory.newInstance())
                    } else {
                        replaceFragment(FragmentLoginSeller.newInstance())
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        val fm = supportFragmentManager

        val currentFragment = fm.findFragmentById(R.id.main_fragment_container)


        if (currentFragment is FragmentProductManagement) {
            PaperIO.getCounterSelected {
                if (it > 0) {
                    replaceFragment(FragmentCustomerCategory.newInstance())
                } else {
                    replaceFragment(FragmentLoginSeller.newInstance())
                }
            }
        } else if (currentFragment is FragmentProductSort) {
            PaperIO.clearListCategoryFilter()
            PaperIO.clearListManufactureFilter()
            replaceFragment(FragmentProductManagement.newInstance())
        } else if (currentFragment is FragmentCustomerCategoryFilter) {
            PaperIO.clearListManufactureFilter()
            PaperIO.clearListCategoryFilter()
            replaceFragment(FragmentCustomerCategory.newInstance())
        } else {
            super.onBackPressed()
        }
        /*val d =  v?.childFragmentManager*/

        // val backEntry = fm?.getBackStackEntryAt(fm?.backStackEntryCount)
        //  val fragmentName = backEntry?.name
        /*if ()
            PaperIO.getCounterSelected {
                if (it > 0) {
                    replaceFragment(FragmentCustomerCategory.newInstance())
                } else {
                    replaceFragment(FragmentLoginSeller.newInstance())
                }
            }*/

        var b = 0


    }
}