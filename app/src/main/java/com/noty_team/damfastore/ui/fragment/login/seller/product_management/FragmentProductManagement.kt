package com.noty_team.damfastore.ui.fragment.login.seller.product_management

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Environment
import android.os.Handler
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.base.api.response.MessageForCategory
import com.noty_team.damfastore.base.fragment.BaseFragment
import com.noty_team.damfastore.ui.fragment.login.customer.category.FragmentCustomerCategory
import com.noty_team.damfastore.ui.fragment.login.seller.FragmentLoginSeller
import com.noty_team.damfastore.ui.fragment.login.seller.product_management.product_filters.subfilter.FragmentSubFilterProduct
import com.noty_team.damfastore.ui.fragment.login.seller.product_management.products_category.FragmentCategoryProducts
import com.noty_team.damfastore.ui.recycler.adapters.CategoriesManufactureAdapter
import com.noty_team.damfastore.ui.recycler.adapters.ProductAdapter
import com.noty_team.damfastore.ui.recycler.item_adapter.CategoriesManufactureItem
import com.noty_team.damfastore.ui.recycler.item_adapter.ProductRecyclerItem
import com.noty_team.damfastore.utils.paper.PaperIO
import com.noty_team.damfastore.utils.paper.PaperIO.Companion.categories
import com.noty_team.damfastore.utils.paper.PaperIO.Companion.categoriesFilter
import com.noty_team.damfastore.utils.paper.PaperIO.Companion.manufactureFilter
import com.noty_team.damfastore.utils.paper.PaperIO.Companion.manufactures
import com.noty_team.damfastore.utils.paper.products.ProductItem
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_product_manegement.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


class FragmentProductManagement : BaseFragment<ProductManagementPresenter>() {

    companion object {
        fun newInstance() = FragmentProductManagement()
        var isNeedUpdate = false
        var isBack = false
    }
    var search: Boolean = true

    var prevCounter = 0
    val response = arrayListOf<ProductItem>()
    var categoriesSelectedList = listOf<MessageForCategory>()
    var categoriesList = categoriesFilter
    var manufacturerList = manufactureFilter
    var onAddCheckcategoryManufacture: Observable<Unit>? = null

    var isCallbackAddCheckcategoryManufacture = false

    private lateinit var categoriesAdapter: CategoriesManufactureAdapter

    private lateinit var manufacturerAdapter: CategoriesManufactureAdapter

    private lateinit var productAdapter: ProductAdapter
    private var product_recycler: RecyclerView? = null
    var dalayAdapter = 500L

    private var downloadID: Long = 0


    override fun layout() = R.layout.fragment_product_manegement

//    var productIdListAll = mutableListOf<String>()

    private val onDownloadComplete = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            //Fetching the download id received with the broadcast
            val id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
            //Checking if the received broadcast is for our enqueued download by matching download id
            if (id == downloadID) {

                Log.d("myLogs", "initRecyclersData ")
            }
        }
    }

    private fun beginDownload(imageName: String, addressLink: String): String {

        if (addressLink.isNotEmpty()) {

            val nameDir =
                if (imageName.isNotEmpty()) {
                    imageName
                } else {
                    UUID.randomUUID().toString()
                }

            val file =
                File(baseActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES), nameDir)

            val request = DownloadManager.Request(Uri.parse(addressLink))
                .setTitle(nameDir)
                .setDescription("Downloading")
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                .setDestinationUri(Uri.fromFile(file))// Uri of the destination file
                .setAllowedOverMetered(true)
                .setAllowedOverRoaming(true)

            val downloadManager =
                baseActivity.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

            downloadID = downloadManager.enqueue(request)

            return file.absolutePath
        } else return ""
    }


    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.toggleKeyboard(false)

        baseActivity.registerReceiver(
            onDownloadComplete,
            IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        )
        product_recycler = view.findViewById(R.id.product_recycler)
        presenter = ProductManagementPresenter(baseContext)

        if (isFirstInit) {
            if (categories.size > 0 &&
                manufactures.size > 0 &&
                PaperIO.products.size > 0
            ) {
                presenter?.getCategoriesIdsBySeller(
                    onSuccess = {
                        categoriesSelectedList = it.message!!
                        initRecyclersData()
                    }, onError = {
                        initRecyclersData()
                    })

            } else {
                getBaseData()
            }
            initSearchListener()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        PaperIO.clearListCategoryFilter()
        PaperIO.clearListManufactureFilter()
        // baseActivity.unregisterReceiver(onDownloadComplete)
    }

    private fun getBaseData() {

        isShowLoadingDialog(true)
        presenter?.getBaseData(onAllRequestsLoaded = {
            /*   baseActivity.startService(
                   Intent(
                       baseActivity,
                       DownloadService::class.java
                   ).putExtra(Constants.SERVICE_IDENT, Constants.SERVICE_IDENT)
               )

               initRecyclersData()
   */
            // startDownload()


            presenter?.loadAllImages(PaperIO.products, onItemComplete = { productItem ->

                Log.d("myLogs", "productsAppName " +productItem.productsAppName)

                    val imageParse = beginDownload(

                        productItem.productsAppId ?: "",
                        productItem.productsImage ?: ""
                    )
                    productItem.image = imageParse

                    response.add(productItem)
            }, onComplete = {

                PaperIO.products = response

            })

        }, onCategorySuccess = {
            categories = it
        }, onManufactureSuccess = {
            manufactures = it
        }, onProductSuccess = {

            PaperIO.products = it
            initRecyclersData()

        }, onFilterSuccess = {
            PaperIO.filters = it
        }, onFilterSubfilterSuccess = {
            PaperIO.subfiltersHashList = it.mapToSubfilter()
            PaperIO.subfiltersHashListClean = it.mapToSubfilter()

            PaperIO.filterBDItem = it.mapToFilterList()
            PaperIO.filterBDItemClean = it.mapToFilterList()

            PaperIO.filterSubfilter = it
            PaperIO.filterSubfilterClean = it

            PaperIO.filterCounter = it.mapToCounter()
            PaperIO.filterCounterClean = PaperIO.filterCounter
        },
            onError = {
                isShowLoadingDialog(false)
            }
        )

    }

    var callbackSelectedButton: (
        isNeedSelected: Boolean,
        categoryManufactureID: String,
        type: FragmentCategoryProducts.AdapterItemType
    ) -> Unit =
        { isNeedSelected, categoryManufactureID, type ->
            isCallbackAddCheckcategoryManufacture = true
            onAddCheckcategoryManufacture =
                Observable.fromCallable {
                    addCheckboxCategoryManufacture(
                        isNeedSelected,
                        categoryManufactureID,
                        type
                    )
                }
        }

    private fun addCheckboxCategoryManufacture(
        isNeedSelected: Boolean,
        categoryManufactureID: String,
        type: FragmentCategoryProducts.AdapterItemType
    ) {
        if (type == FragmentCategoryProducts.AdapterItemType.CATEGORY)
            categoriesAdapter.changeSelectedUIById(categoryManufactureID, isNeedSelected)
        else if (type == FragmentCategoryProducts.AdapterItemType.MANUFACTURE)
            manufacturerAdapter.changeSelectedUIById(categoryManufactureID, isNeedSelected)
    }

    private fun initRecyclersData() {

        isShowLoadingDialog(false)

        if (PaperIO.getFilterCounter() > 0) {
            isShowTextcategoryManufacture(false)
            prevCounter = PaperIO.getFilterCounter()
            PaperIO.getProductsByFilters {
                clearAdapters()
                if (!it.isNullOrEmpty()) {
                    isShowTextNoProductByFilter(false)
                    initProductAdapter(mapRecyclerProduct(it))
                } else {
                    isShowTextNoProductByFilter(true)

                }
            }
        } else {
            initWithoutFilters()
        }
    }

    private fun isShowTextNoProductByFilter(isShowText: Boolean) {
        if (isShowText) text_when_no_product_by_filter.visibility = View.VISIBLE
        else text_when_no_product_by_filter.visibility = View.GONE
    }

    private fun clearAdapters() {
        if (this::productAdapter.isInitialized)
            productAdapter.clear()

        if (this::categoriesAdapter.isInitialized)
            categoriesAdapter.clear()

        if (this::manufacturerAdapter.isInitialized)
            manufacturerAdapter.clear()
    }

    private fun initWithoutFilters() {
        isShowTextcategoryManufacture(true)
        isShowLoadingDialog(true)
        isShowTextNoProductByFilter(false)


        prevCounter = 0
        presenter?.getDataFromDB(
            onCategorySuccess = {
                if (categoriesSelectedList.isNotEmpty()) {

                    for (item in it) {
                        for (selectedCategory in categoriesSelectedList) {
                            if (selectedCategory.categoriesAppCategoryId == item.id && selectedCategory.categoriesAppStatus == "1") {
                                item.isSelected = true
                                break
                            } else {
                                item.isSelected = false
                            }

                        }
                    }
                }
                if (categoriesList.isNotEmpty()) {
                    initCategoriesAdapter(categoriesList)

                } else {

                    initCategoriesAdapter(it)
                }
            }, onProductSuccess = {
                if (!it.isNullOrEmpty()) {

                    initProductAdapter(mapRecyclerProduct(it))
                }
            },
            onManufactureSuccess = {
                if (categoriesSelectedList.isNotEmpty()) {

                    for (item in it) {
                        for (selectedCategory in categoriesSelectedList) {
                            if (selectedCategory.categoriesAppCategoryId == item.id && selectedCategory.categoriesAppStatus == "1") {
                                item.isSelected = true
                                break
                            } else {
                                item.isSelected = false
                            }

                        }
                    }
                }
                if (manufacturerList.isEmpty()) {
                    initManufacturerAdapter(it)
                } else {
                    initManufacturerAdapter(manufacturerList)
                }
            })
    }

    private fun initAction() {
        log_out_seller.getClick {
            PaperIO.resetAllFilters()
            categoriesFilter.clear()
            manufactureFilter.clear()
            save {
                PaperIO.getCounterSelected {
                    if (it > 0) {
                        val hashListCategories: HashMap<String, Any> = HashMap()
                        var indexCategory: Int = 0

                        for ((index, hashListItem) in categories.withIndex()) {
                            hashListCategories["categories_ids[${index}][category_id]"] =
                                hashListItem.id.toInt()
                            hashListCategories["categories_ids[${index}][status]"] =
                                hashListItem.isSelected
                            indexCategory = index
                        }

                        for (hashListItem in manufactures) {
                            ++indexCategory
                            hashListCategories["categories_ids[${indexCategory}][category_id]"] =
                                hashListItem.id.toInt()
                            hashListCategories["categories_ids[${indexCategory}][status]"] =
                                hashListItem.isSelected
                        }

                        presenter?.сreateCategoriesIdsBySeller(hashListCategories,
                            onSuccess = {
                                replaceFragment(FragmentCustomerCategory.newInstance())
                            }, onError = {
                                replaceFragment(FragmentCustomerCategory.newInstance())
                            })
                    } else {
                        replaceFragment(FragmentLoginSeller.newInstance())
                    }
                }
            }
        }
        tvSortSellerProduct.getClick {
            replaceFragment(FragmentProductSort())
        }
        characteristic_btn.getClick {
            save {
                addFragment(FragmentSubFilterProduct.newInstance())
            }
        }
    }

    override fun onBackPressed(): Boolean {
        return true
    }

    private fun save(onSuccess: () -> Unit) {

        if (!this::categoriesAdapter.isInitialized || categoriesAdapter.List().size == 0) {
            saveProduct {
                onSuccess()
            }
        } else {
            saveCategory {
                if (it) {
                    saveManufacture {
                        onSuccess()
                    }
                } else {
                    onSuccess()
                }
            }
        }
    }

    fun saveCategory(onSuccess: (isSave: Boolean) -> Unit) {
        if (this::categoriesAdapter.isInitialized) {
            PaperIO.setListCategory(categoriesAdapter.List()) {
                onSuccess(true)
            }
        } else {
            onSuccess(false)
        }

    }

    fun saveManufacture(onSuccess: (isSave: Boolean) -> Unit) {
        if (this::manufacturerAdapter.isInitialized) {
            PaperIO.setListManufacture(manufacturerAdapter.List()) {
                onSuccess(true)
            }
        } else {
            onSuccess(false)
        }
    }

    fun saveProduct(onSuccess: (isSave: Boolean) -> Unit) {

        if (this::productAdapter.isInitialized)
            PaperIO.setListProducts(productAdapter.List()) {
                onSuccess(true)
            }
        else {
            onSuccess(false)
        }
    }

    private fun initSearchListener() {

        product_content_search_main.setOnClickListener({ product_content_search_main.setIconified(false) })

        product_content_search_main.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {

                    if (newText.length > 2) {

                        if (PaperIO.getFilterCounter() > 0) {

                            if (this@FragmentProductManagement::categoriesAdapter.isInitialized &&
                                this@FragmentProductManagement::manufacturerAdapter.isInitialized
                            ) {
                                categoriesAdapter.clear()
                                manufacturerAdapter.clear()

                            }

                            productAdapter.clear()

                            initRecyclersData()

                        } else {
                            searchLogic(newText)

                        }


                    } else if (newText.length == 0) {

                        product_content_search_main.setIconified(true)
                        product_content_search_main.clearFocus()
                        initRecyclersData()

                    }


                    return false
                }

                override fun onQueryTextSubmit(query: String): Boolean {
                    product_content_search_main.clearFocus()

                    return false
                }

        })

        /*product_manager_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {


                if (count > 2) {
                    if (isNeedUpdate) {

                        if (PaperIO.getFilterCounter() > 0) {

                            if (this@FragmentProductManagement::categoriesAdapter.isInitialized &&
                                this@FragmentProductManagement::manufacturerAdapter.isInitialized
                            ) {
                                categoriesAdapter.clear()
                                manufacturerAdapter.clear()

                            }

                            productAdapter.clear()

                            initRecyclersData()

                        } else {
                            searchLogic(s)

                        }
                        isNeedUpdate = false
                    } else {

                        if (count == before) {
                            isShowLoadingDialog(false)
                            isBack = false
                        } else {
                            searchLogic(s)
                        }
                    }

                } else if (count == 0) {
                    if (isNeedUpdate) {
                        if (PaperIO.getFilterCounter() > 0) {

                            if (this@FragmentProductManagement::categoriesAdapter.isInitialized &&
                                this@FragmentProductManagement::manufacturerAdapter.isInitialized
                            ) {
                                categoriesAdapter.clear()
                                manufacturerAdapter.clear()
                            }

                            productAdapter.clear()
                            initRecyclersData()
                        }
                        isNeedUpdate = false
                    } else {

                        if (count == before) {
                            isShowLoadingDialog(false)
                            isBack = false
                            if (PaperIO.getFilterCounter() > 0) {

                            } else {
                                presenter?.getFiftyProductZip {
                                    if (!it.isNullOrEmpty())
                                        initProductAdapter(mapRecyclerProduct(it))
                                }
                            }
                        } else {
                            initRecyclersData()

                            Handler().postDelayed({ isShowLoadingDialog(false) }, dalayAdapter)
                        }

                    }
                }
            }
        })*/
    }

    private fun searchLogic(s: CharSequence?) {

        PaperIO.getProductByNameSync(s.toString()) {
            productAdapter.clear()
            productAdapter.addAll(it)
        }


        if (this::categoriesAdapter.isInitialized && this::manufacturerAdapter.isInitialized) {
            PaperIO.getCategoryByNameSync(s.toString()) { listSearchCategory ->
                categoriesAdapter.clear()
                categoriesAdapter.addAll(listSearchCategory)
            }

            PaperIO.getManufactureByNameSync(s.toString()) { listSearchManufacture ->
                manufacturerAdapter.clear()
                manufacturerAdapter.addAll(listSearchManufacture)
            }

        }

        Handler().postDelayed({ isShowLoadingDialog(false) }, dalayAdapter)
    }


    private fun initProductAdapter(listProducts: ArrayList<ProductRecyclerItem>) {

        product_recycler?.layoutManager = GridLayoutManager(context, 3)

        productAdapter = ProductAdapter(
            listProducts,
            onClickSelect = {

            }, onAllDataLoaded = {
                Handler().postDelayed({ isShowLoadingDialog(false) }, dalayAdapter)
            })

        product_recycler?.adapter = productAdapter

        Handler().postDelayed({ isShowLoadingDialog(false) }, dalayAdapter)
    }

    private fun initManufacturerAdapter(listManufacture: ArrayList<CategoriesManufactureItem>) {

        manufacturer_recycler.layoutManager = LinearLayoutManager(baseContext)

        manufacturerAdapter = CategoriesManufactureAdapter(listManufacture,
            CategoriesManufactureAdapter.ItemSettings.MANUFACTURE,
            onClickSelect = { isSelected, listProducts ->
                isShowLoadingDialog(true)
                if (isSelected) {
                    PaperIO.setSelectedUnselectedByListProduct(true, listProducts) {
                        isShowLoadingDialog(false)
                    }
                } else {
                    PaperIO.setSelectedUnselectedByListProduct(false, listProducts) {
                        isShowLoadingDialog(false)
                    }
                }


            },
            onItemClick = {
                addFragment(FragmentCategoryProducts.newInstance().apply {
                    callbackSelectedButton = this@FragmentProductManagement.callbackSelectedButton
                    categoryManufactureID = it.id
                    adapterItemType = FragmentCategoryProducts.AdapterItemType.MANUFACTURE
                    listProductsId = it.listProducts
                    nameCategory = it.manufactureName
                })
            })
        manufacturer_recycler.adapter = manufacturerAdapter
    }

    private fun initCategoriesAdapter(listCategory: ArrayList<CategoriesManufactureItem>) {

        categories_recycler?.layoutManager = LinearLayoutManager(baseContext)

        categoriesAdapter = CategoriesManufactureAdapter(
            listCategory,
            CategoriesManufactureAdapter.ItemSettings.CATEGORY,
            onClickSelect = { isSelected, listProducts ->


                isShowLoadingDialog(true)
                if (isSelected) {

                    PaperIO.setSelectedUnselectedByListProduct(true, listProducts) {
                        saveCategory {
                            isShowLoadingDialog(false)
                        }
                    }

                } else {
                    PaperIO.setSelectedUnselectedByListProduct(false, listProducts) {
                        saveCategory {
                            isShowLoadingDialog(false)
                        }
                    }
                }
                for (i in categories) {
                    Log.d("myLogs", i.id + " -- " + i.isSelected)
                }
            },
            onItemClick = {
                addFragment(FragmentCategoryProducts.newInstance().apply {
                    callbackSelectedButton = this@FragmentProductManagement.callbackSelectedButton
                    categoryManufactureID = it.id
                    adapterItemType = FragmentCategoryProducts.AdapterItemType.CATEGORY
                    listProductsId = it.listProducts
                    nameCategory = it.manufactureName
                })

            })
        categories_recycler.adapter = categoriesAdapter
    }

    private fun isShowTextcategoryManufacture(isShow: Boolean) {
        if (isShow) {
            category_title_text.visibility = View.VISIBLE
            manufacture_title_text.visibility = View.VISIBLE
        } else {
            category_title_text.visibility = View.GONE
            manufacture_title_text.visibility = View.GONE
        }
    }

    private fun mapRecyclerProduct(list: ArrayList<ProductItem>): ArrayList<ProductRecyclerItem> {

        val listRecycler = ArrayList<ProductRecyclerItem>()

        for (i in 0 until list.size) {
            listRecycler.add(
                ProductRecyclerItem(
                    id = list[i].productsId ?: "",
                    isSelected = list[i].isSelected,
                    productImage = list[i].productsImage ?: "",
                    productImageStore = list[i].image,
                    productName = list[i].productsAppName ?: ""
                )
            )
        }
        return listRecycler
    }

    override fun onResume() {
        baseActivity.toggleKeyboard(false)

        initAction()

        if (isCallbackAddCheckcategoryManufacture) {
            isCallbackAddCheckcategoryManufacture = false
            onAddCheckcategoryManufacture?.subscribe()
        }

        //if filter update
        if (PaperIO.getFilterCounter() != prevCounter) {
            initRecyclersData()
        }
        super.onResume()
    }


    override fun onDetach() {
        PaperIO.resetAllFilters()
        save {}
        super.onDetach()
    }
}