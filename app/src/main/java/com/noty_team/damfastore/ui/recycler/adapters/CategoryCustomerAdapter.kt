package com.noty_team.damfastore.ui.recycler.adapters

import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.ui.recycler.item_adapter.CategoriesItem
import com.noty_team.damfastore.ui.recycler.item_adapter.CategoriesManufactureItem
import com.noty_team.damfastore.utils.base_adapters.BaseAdapter
import kotlinx.android.synthetic.main.item_customer_category.*

class CategoryCustomerAdapter(
    list: ArrayList<CategoriesManufactureItem>,
    val onItemClick: (categoryItem: CategoriesManufactureItem) -> Unit,
    val onAllDataLoad: () -> Unit = {}
) :
    BaseAdapter<CategoriesManufactureItem, CategoryCustomerAdapter.ViewHolder>(list) {

    override fun getItemViewType(position: Int) = R.layout.item_customer_category

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            itemView.setOnClickListener {
                onItemClick(list[position])
            }
        }

        override fun bind(pos: Int) {
            val item = list[pos]
            name_customer_product.text = item.manufactureName

            if (position == list.size - 1)
                onAllDataLoad()
        }

    }
}