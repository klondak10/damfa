package com.noty_team.damfastore.ui.recycler.adapters

import android.view.View
import com.noty_team.damfastore.R
import com.noty_team.damfastore.ui.recycler.item_adapter.CategoriesManufactureItem
import com.noty_team.damfastore.ui.recycler.item_adapter.ProductRecyclerItem
import com.noty_team.damfastore.utils.base_adapters.BaseAdapter
import com.noty_team.damfastore.utils.paper.products.ProductItem
import kotlinx.android.synthetic.main.item_product.*
import noty_team.com.urger.utils.setPhoto
import noty_team.com.urger.utils.setPhotoProducts

class ProductAdapter(
    list: ArrayList<ProductRecyclerItem>,
    val onClick: (item: ProductRecyclerItem) -> Unit = {},
    val onClickSelect: (item: ProductRecyclerItem) -> Unit = {},
    val onAllDataLoaded: () -> Unit = {}
) :
    BaseAdapter<ProductRecyclerItem, ProductAdapter.ViewHolder>(list) {

    var counterSelected = 0

    override fun getItemViewType(position: Int) = R.layout.item_product

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            itemView.setOnClickListener {
                onClick(list[position])
            }

            selected_product.setOnClickListener {
                val item = list[position]
                item.isSelected = !item.isSelected

                isSelected(item.isSelected)

                onClickSelect(item)
            }
        }

        override fun bind(pos: Int) {
            val item = list[pos]

            product_name.text = item.productName


            if (!item.productImageStore.isNullOrEmpty()) {
                setPhotoProducts(item_image, item.productImageStore)
            } else {
                if (!item.productImage.isNullOrEmpty())
                    setPhotoProducts(item_image, item.productImage)
                else item_image.setImageResource(R.mipmap.test_logo2)
            }

            isSelected(item.isSelected)

            if (item.isSelected)
                ++counterSelected

            if (position == list.size - 1) {
                onAllDataLoaded()
            }


        }

        private fun isSelected(isSelected: Boolean) {
            selected_product.visibility = View.VISIBLE
            unselected_product.visibility = View.GONE
            if (isSelected) {
                selected_product.setImageResource(R.mipmap.ic_select)
            } else {
                selected_product.setImageResource(R.mipmap.ic_unselect)
            }
        }

    }

    fun getSelected(): ArrayList<ProductRecyclerItem> {

        val selectedlist = ArrayList<ProductRecyclerItem>()

        list.forEach {
            if (it.isSelected) {
                selectedlist.add(it)
            }
        }

        return selectedlist
    }

    fun resetSelectedButton() {
        list.forEach {
            it.isSelected = false
            notifyDataSetChanged()
        }
    }


    fun sortList(isSorted: Boolean) {
        if (isSorted) {
            list.sortBy {
                it.productName
            }
        } else {
            list.reverse()
        }
        notifyDataSetChanged()
    }

    fun List(): ArrayList<ProductRecyclerItem> {
        return list
    }

    fun getCounter(): Int {
        var counterSelected = 0
        list.forEach {
            if (it.isSelected)
                ++counterSelected
        }

        return counterSelected
    }
}